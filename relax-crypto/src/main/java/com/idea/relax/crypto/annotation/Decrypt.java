package com.idea.relax.crypto.annotation;

import com.idea.relax.crypto.config.CryptoType;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @author: 沉香
 * @date: 2023/3/17
 * @description:
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Decrypt {

	/**
	 * 解密的私钥，如果不配置则使用{@link com.idea.relax.crypto.config.RelaxCryptoProperties}中配置的私钥
	 * Alias for {@link Decrypt#privateKey()}.
	 *
	 * @return
	 */
	@AliasFor(value = "privateKey")
	String value() default "";

	/**
	 * 解密的私钥，如果不配置则使用{@link com.idea.relax.crypto.config.RelaxCryptoProperties}中配置的私钥
	 * Alias for {@link Decrypt#value()}.
	 *
	 * @return
	 */
	@AliasFor(value = "value")
	String privateKey() default "";

	/**
	 * 解密的方式
	 *
	 * @return
	 */
	CryptoType cryptoType() default CryptoType.DES;


}
