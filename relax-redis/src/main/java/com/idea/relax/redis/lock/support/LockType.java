package com.idea.relax.redis.lock.support;

/**
 * @className: LockType
 * @description: 锁的类型
 * @author: salad
 * @date: 2022/9/2
 **/
public enum LockType {

    /**
     * 可重入锁
     */
    REENTRANT,

    /**
     * 公平锁
     */
    FAIR;

}
