package com.idea.relax.exception.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.Set;

/**
 * @author: 沉香
 * @date: 2023/2/27
 * @description: 异常工具包相关的属性配置
 */
@Data
@ConfigurationProperties(ExceptionKitProperties.PREFIX)
public class ExceptionKitProperties {

	/**
	 * 前缀
	 */
	public static final String PREFIX = "relax.exception-kit";

	/**
	 * 是否开启异常枚举类包扫描
	 */
	private boolean enabledEnumScan = true;

	/**
	 * 异常枚举类包扫描路径，默认主启动类下的所有包
	 */
	private Set<String> enumScanPackages;

	/**
	 * 是否展示内置的异常码（阿里巴巴规范中的异常码）
	 */
	private boolean useDefaultErrorCode = false;

	/**
	 * 是否开启异常码国际化
	 */
	private boolean enabledI18n = false;

	/**
	 * 是否在控制台打印异常码文档地址
	 */
	private boolean printDocUri = true;

	/**
	 * 文档标题名称，默认：应用名+"异常码"
	 */
	private String docTitle = "";

	/**
	 * 是否告警
	 */
	private boolean isAlarm = true;

	/**
	 * 允许告警的配置，支持的属性请参考 {@link com.idea.relax.exception.config.ExceptionKitProperties.AlarmType}
	 */
	private Map<AlarmType, Set<String>> alarmLevels;


	public enum AlarmType {

		/**
		 * 异常的类名
		 */
		EXCEPTION_NAME,

		/**
		 * 异常的所在包名
		 */
		EXCEPTION_PACKAGE,

		/**
		 * 异常等级，getLevel方法的值
		 */
		ERROR_LEVEL,

		/**
		 * 异常码
		 */
		ERROR_CODE,

		/**
		 * 异常码的类名
		 */
		ERROR_CODE_NAME,

		/**
		 * 异常码的所在包名
		 */
		ERROR_CODE_PACKAGE

	}


}
