package com.toplion.project.tools.domain;

import com.toplion.project.tools.service.ConsumerStringOutStream;
import io.fabric8.kubernetes.client.KubernetesClient;
import lombok.Data;

import java.util.function.Consumer;

/**
 * @author azhao
 */
@Data
public class PodLogWatchThread implements Runnable {
    private KubernetesClient kubernetesClient;
    private String namespace;
    private String podName;
    private Consumer<String> consumer;

    public PodLogWatchThread() {

    }

    @Override
    public void run() {
        try {
            kubernetesClient.pods().inNamespace(namespace).withName(podName).watchLog(new ConsumerStringOutStream(consumer));
            synchronized (PodLogWatchThread.class) {
                PodLogWatchThread.class.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
