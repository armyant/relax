package com.idea.relax.tool.wechat.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: 沉香
 * @date: 2023/2/27
 * @description:
 */
@Data
@ConfigurationProperties(WechatPayProperties.PREFIX)
public class WechatPayProperties {

    /**
     * 前缀
     */
    public static final String PREFIX = "relax.wechat-pay";

    /**
     * appId
     */
    private String appId;

    /**
     * 商户ID
     */
    private String mchId;

    /**
     * 商户证书序列号
     */
    private String mchSerialNo;

    /**
     * apiV3密钥
     */
    private String v3Secret;

    /**
     * 商户API私钥
     */
    private String privateKey;

    /**
     * 商户API私钥存放路径
     */
    private String privateKeyPath;

    /**
     * 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。 公网域名必须为https，如果是走专线接入，使用专线NAT IP或者私有回调域名可使用http
     * 示例值：https://www.weixin.qq.com/wxpay/pay.php
     */
    public String notifyUrl;

}
