package com.idea.relax.crypto.config;

/**
 * @author: 沉香
 * @date: 2023/3/18
 * @description:
 */
public enum CryptoType implements ICryptoType{

	AES,

	DES;

	@Override
	public String get() {
		return AES.name();
	}

}
