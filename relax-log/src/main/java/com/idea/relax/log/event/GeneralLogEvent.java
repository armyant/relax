package com.idea.relax.log.event;

import com.idea.relax.log.publisher.ILogPublisher;
import org.springframework.context.ApplicationEvent;

/**
 * @className: GeneralLogEvent
 * @description: 通用日志的事件,当记录通用日志时，会发布该事件
 * @author: salad
 * @date: 2022/10/1
 **/
public class GeneralLogEvent extends ApplicationEvent {

    public GeneralLogEvent(ILogPublisher.Wrapper source) {
        super(source);
    }

}
