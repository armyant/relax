package com.idea.relax.boot.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author: 沉香
 * @date: 2023/4/1
 * @description:
 */
public class BigNumberModule extends SimpleModule {

	public static final BigNumberModule INSTANCE = new BigNumberModule();

	public BigNumberModule() {
		super(BigNumberModule.class.getName());
		this.addSerializer(Long.class, ToStringSerializer.instance);
		this.addSerializer(Long.TYPE, ToStringSerializer.instance);
		this.addSerializer(BigInteger.class, ToStringSerializer.instance);
		this.addSerializer(BigDecimal.class, ToStringSerializer.instance);
	}

}
