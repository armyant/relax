package com.idea.relax.cache.aspect;

import com.idea.relax.cache.annotation.CacheEvict;
import com.idea.relax.cache.annotation.Cacheable;
import com.idea.relax.cache.cache.Cache;
import com.idea.relax.cache.props.RelaxCacheProperties;
import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import com.idea.relax.cache.suppert.parser.CacheExpressionParser;
import com.idea.relax.cache.util.ValueUtil;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

/**
 * @className: CacheEvictAspect
 * @description:
 * @author: salad
 * @date: 2022/10/13
 **/
@Aspect
@AllArgsConstructor
public class CacheEvictAspect {

    private final Cache cache;

    private final CacheExpressionParser cacheExpressionParser;

    private final RelaxCacheProperties properties;

    private final IFunctionHandler<IFunction> handler;

    @Around("@within(com.idea.relax.cache.annotation.CacheEvict) " +
            "|| @annotation(com.idea.relax.cache.annotation.CacheEvict)")
    public Object cacheEvictAround(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getSignature();
        String methodName = signature.getName();
        MethodSignature methodSignature = (MethodSignature) signature;
        CacheEvict cacheEvict = AnnotationUtils.findAnnotation(methodSignature.getMethod(), CacheEvict.class);
        if (null == cacheEvict){
            cacheEvict = AnnotationUtils.findAnnotation(methodSignature.getClass(),CacheEvict.class);
        }
        Object[] args = point.getArgs();
        Assert.notNull(cacheEvict,"");
        String name = cacheEvict.cacheNames();
        String key = cacheEvict.key();
        String condition = cacheEvict.condition();
        String keyPrefix = properties.getKeyPrefix();
        Boolean isEvict = Optional.ofNullable(cacheExpressionParser
                .parseExpressionForCondition(condition, point, handler)).orElse(Boolean.FALSE);
        if (!isEvict){
            return point.proceed();
        }
        if (ValueUtil.isEmpty(key)) {
            key = methodName.concat(":").concat(Arrays.toString(args));
        }else {
            key = cacheExpressionParser.parseExpressionForKey(key, point, handler);
        }
        if (ValueUtil.isNotEmpty(keyPrefix)){
            key = keyPrefix.concat(key);
        }
        String cacheKey = name.concat(":").concat(key);
        if (cacheEvict.beforeInvocation()){
            cache.evict(cacheKey);
        }
        Object result = null;
        boolean status;
        try{
            result = point.proceed();
            status = true;
        }catch (Throwable e){
            status = false;
        }
        if (status && !cacheEvict.beforeInvocation()){
            cache.evict(cacheKey);
        }
      return result;
    }



}
