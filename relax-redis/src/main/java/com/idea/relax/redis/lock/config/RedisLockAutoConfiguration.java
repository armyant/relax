package com.idea.relax.redis.lock.config;

import com.idea.relax.redis.lock.aspect.RedisLockAspect;
import com.idea.relax.redis.lock.client.RedisLockClient;
import com.idea.relax.redis.lock.client.RedisLockClientImpl;
import com.idea.relax.redis.support.spel.IExpressionEvaluator;
import com.idea.relax.redis.support.spel.RelaxExpressionEvaluator;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;


/**
 * @className: RedisLockAutoConfiguration
 * @description: 分布式锁自动配置类
 * @author: salad
 * @date: 2022/9/2
 **/
@Order
@Configuration(
	proxyBeanMethods = false
)
@ConditionalOnClass({RedissonClient.class})
@EnableConfigurationProperties({RelaxRedisLockProperties.class})
@ConditionalOnProperty(
	value = {"redis.lock.enabled"},
	havingValue = "true"
)
public class RedisLockAutoConfiguration {


	@Bean("redisLockClient")
	@ConditionalOnMissingBean(name = "redisLockClient")
	public RedisLockClient redisLockClient(RedissonClient redissonClient) {
		return new RedisLockClientImpl(redissonClient);
	}

	@Bean
	@ConditionalOnMissingBean(name = "redisLockAspect")
	public RedisLockAspect redisLockAspect(RelaxRedisLockProperties relaxRedisLockProperties,
										   IExpressionEvaluator expressionEvaluator,
										   RedisLockClient redisLockClient) {
		return new RedisLockAspect(relaxRedisLockProperties, expressionEvaluator, redisLockClient);
	}


}
