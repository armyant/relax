package com.idea.relax.exception.core.registry;


/**
 * @author: 沉香
 * @date: 2023/5/25
 * @description: 自定义异常码的收集器
 */
public interface IErrorCodeCollector {

	/**
	 * 收集异常码
	 * @param errorCodeProvider
	 */
	 void collect(ErrorCodeProvider errorCodeProvider);

}
