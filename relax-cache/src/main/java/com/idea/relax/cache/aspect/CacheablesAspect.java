package com.idea.relax.cache.aspect;


import com.idea.relax.cache.annotation.Cacheable;
import com.idea.relax.cache.cache.Cache;
import com.idea.relax.cache.props.RelaxCacheProperties;
import com.idea.relax.cache.suppert.parser.CacheExpressionParser;
import com.idea.relax.cache.suppert.lock.CacheLockClient;
import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import com.idea.relax.cache.util.ValueUtil;
import com.idea.relax.redis.lock.support.LockType;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @className: CacheablesAspect
 * @description: 注解切面类
 * @author: salad
 * @date: 2022/6/1
 **/
@Aspect
@AllArgsConstructor
public class CacheablesAspect {

    private final CacheLockClient cacheLockClient;

    private final Cache cache;

    private final CacheExpressionParser cacheExpressionParser;

    private final RelaxCacheProperties properties;

    private final IFunctionHandler<IFunction> handler;

    @Around("@within(com.idea.relax.cache.annotation.Cacheable) " +
            "|| @annotation(com.idea.relax.cache.annotation.Cacheable)")
    public Object cacheablesAround(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Cacheable cacheable = AnnotationUtils.findAnnotation(methodSignature.getMethod(), Cacheable.class);
        if (null == cacheable){
            cacheable = AnnotationUtils.findAnnotation(methodSignature.getClass(), Cacheable.class);
        }
        String methodName = signature.getName();
        Object[] args = point.getArgs();
        Assert.notNull(cacheable,"");
        String name = cacheable.cacheNames();
        String key = cacheable.key();
        long ttl = cacheable.ttl();
        String condition = cacheable.condition();
        LockType lockType = cacheable.lockType();
        String keyPrefix = properties.getKeyPrefix();
        Boolean isCache = Optional.ofNullable(cacheExpressionParser
                .parseExpressionForCondition(condition, point, handler)).orElse(Boolean.FALSE);
        if (ValueUtil.isNotEmpty(key)){
            key = methodName.concat(":").concat(Arrays.toString(args));
        }else {
            key = cacheExpressionParser.parseExpressionForKey(key, point, handler);
        }

        if (ValueUtil.isNotEmpty(keyPrefix)){
            key = keyPrefix.concat(key);
        }

        String cacheKey = name.concat(":").concat(key);
        Object value = cache.get(cacheKey);
        //缓存未命中
        if (isCache && ValueUtil.isEmpty(value)){
            if (properties.isEnableLock()){
                boolean isLock = cacheLockClient.tryLock(cacheKey, lockType,
                        30, 90, TimeUnit.SECONDS);
                try {
                   if (isLock){
                       //谁抢到锁谁就去执行目标方法，其它线程等待
                       value = cache.get(cacheKey);
                       if (ValueUtil.isNotEmpty(value)){
                           return value;
                       }
                       Object result = point.proceed();
                       putValueToCache(result,cacheKey,ttl);
                   }
               }finally {
                   cacheLockClient.unLock(cacheKey,lockType);
               }
            }else {
                Object result = point.proceed();
                putValueToCache(result,cacheKey,ttl);
                return result;
            }
        }
        return value;
    }

    private void putValueToCache(Object result,String cacheKey,Long ttl){
        //isCacheNullValue：是否缓存空数据，防止缓存穿透
        if (ValueUtil.isEmpty(result) && properties.isCacheNullValue()){
            cache.put(cacheKey,result,30, TimeUnit.SECONDS);
        }else {
            if (ValueUtil.isNotEmpty(result) && ttl <= -1L){
                cache.put(cacheKey,result);
            }else {
                cache.put(cacheKey,result,ttl, TimeUnit.SECONDS);
            }
        }
    }

}
