package com.idea.relax.sensitive.factory;


import com.idea.relax.sensitive.processor.ISensitiveProcessor;

import java.util.function.Consumer;

/**
 * @author: 沉香
 * @date: 2023/7/10
 * @description: 脱敏算法工厂
 */
public interface ISensitiveFactory {

	/**
	 * 获取脱敏算法类
	 * @param name
	 * @param clazz
	 * @return
	 */
	ISensitiveProcessor<Object> get(String name, Class<?> clazz);

	/**
	 * 新增脱敏算法
	 * @param processors
	 */
	void add(ISensitiveProcessor<Object>... processors);

}
