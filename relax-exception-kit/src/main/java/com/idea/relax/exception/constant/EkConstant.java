package com.idea.relax.exception.constant;

/**
 * @author: 沉香
 * @date: 2023/6/6
 * @description: 常量
 */
public interface EkConstant {

	/**
	 * 列表格式的文档地址
	 */
	String VIEW_URL = "/ek-wel";

	/**
	 * json格式的文档地址
	 */
	String VIEW_TREE_URL = "/tree";

	/**
	 * 属性
	 */
	String ENUM_SCAN_PACKAGE_KEY = "relax.exception-kit.enum-scan-packages";

	/**
	 * 属性
	 */
	String DOC_TITLE_KEY = "relax.exception-kit.doc-title";


}
