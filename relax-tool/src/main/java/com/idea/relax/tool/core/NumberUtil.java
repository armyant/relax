package com.idea.relax.tool.core;

import org.springframework.util.NumberUtils;

/**
 * @className: NumberUtil
 * @description: 数字转换工具类
 * @author: salad
 * @date: 2022/2/17
 **/
public class NumberUtil extends NumberUtils {

    /**
     * 转Int类型,假如转换失败返回0
     */
    public static int toInt(final Object value) {
        return toInt(value, 0);
    }

    /**
     * 转Int类型,假如转换失败返回defaultValue
     */
    public static int toInt(final Object value, final int defaultValue) {
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Integer.parseInt(String.valueOf(value));
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        }
    }

    /**
     * 转long类型,假如转换失败返回0
     */
    public static long toLong(final Object value) {
        return toLong(value, 0L);
    }

    /**
     * 转long类型,假如转换失败返回defaultValue
     */
    public static long toLong(final Object value, final long defaultValue) {
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Long.parseLong(String.valueOf(value));
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        }
    }

    /**
     * 转Double类型,假如转换失败返回0
     */
    public static Double toDouble(Object value) {
        return toDouble(value, 0D);
    }

    /**
     * 转Double类型,假如转换失败返回defaultValue
     */
    public static Double toDouble(Object value, Double defaultValue) {
        return value != null ? Double.valueOf(String.valueOf(value).trim()) : defaultValue;
    }

    /**
     * 转Float类型,假如转换失败返回0
     */
    public static Float toFloat(Object value) {
        return toFloat(value, 0F);
    }

    /**
     * 转Float类型,假如转换失败返回defaultValue
     */
    public static Float toFloat(Object value, Float defaultValue) {
        return value != null ? Float.valueOf(String.valueOf(value).trim()) : defaultValue;
    }

    /**
     * 判断String是否是整数
     *
     * @param s String
     * @return 是否为整数
     */
    public static boolean isInteger(String s) {
        if (StringUtil.isNotBlank(s)) {
            return s.matches("^-?\\d+$");
        } else {
            return false;
        }
    }

    /**
     * 判断字符串是否是Long类型
     *
     * @param s String
     * @return 是否为{@link Long}类型
     * @since 4.0.0
     */
    public static boolean isLong(String s) {
        try {
            Long.parseLong(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 判断字符串是否是浮点数
     *
     * @param s String
     * @return 是否为{@link Double}类型
     */
    public static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
            if (s.contains(".")) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 判断字符串是否是浮点数
     *
     * @param s String
     * @return 是否为{@link Float}类型
     */
    public static boolean isFloat(String s) {
        try {
            Float.parseFloat(s);
            if (s.contains(".")) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

	/**
	 * 强转string,并去掉多余空格
	 *
	 * @param str 字符串
	 * @return String
	 */
	public static String toStr(Object str) {
		return toStr(str, "");
	}

	/**
	 * 强转string,并去掉多余空格
	 *
	 * @param str          字符串
	 * @param defaultValue 默认值
	 * @return String
	 */
	public static String toStr(Object str, String defaultValue) {
		if (null == str) {
			return defaultValue;
		}
		return String.valueOf(str);
	}
}
