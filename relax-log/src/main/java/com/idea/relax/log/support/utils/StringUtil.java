package com.idea.relax.log.support.utils;

import org.springframework.lang.Nullable;

import java.util.*;

/**
 * @className: StringUtils
 * @description:
 * @author: salad
 * @date: 2022/2/25
 **/
public class StringUtil extends org.springframework.util.StringUtils {

    public static boolean isBlank(final CharSequence cs) {
        return !hasText(cs);
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return hasText(cs);
    }

    public static boolean eq(final CharSequence v1, final CharSequence v2) {
        return eq(v1, v2, false);
    }

    public static boolean eqIgnoreCase(final CharSequence v1, final CharSequence v2) {
        return eq(v1, v2, true);
    }

    public static boolean eq(CharSequence str1, CharSequence str2, boolean ignoreCase) {
        if (null == str1) {
            return str2 == null;
        } else if (null == str2) {
            return false;
        } else {
            return ignoreCase ? str1.toString().equalsIgnoreCase(str2.toString()) : str1.equals(str2);
        }
    }

    /**
     * 驼峰命名转下划线分割命名
     */
    public static String humpToUnderline(@Nullable String para) {
        para = firstCharToLower(para);
        StringBuilder sb = new StringBuilder(para);
        int temp = 0;

        for (int i = 0; i < para.length(); ++i) {
            if (Character.isUpperCase(para.charAt(i))) {
                sb.insert(i + temp, "_");
                ++temp;
            }
        }
        return sb.toString().toLowerCase();
    }


    public static String firstCharToLower(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'A' && firstChar <= 'Z') {
            char[] arr = str.toCharArray();
            arr[0] = (char) (arr[0] + 32);
            return new String(arr);
        } else {
            return str;
        }
    }

    public static String removeSuffix(CharSequence str, CharSequence suffix) {
        if (!Func.isEmpty(str) && !Func.isEmpty(suffix)) {
            String str2 = str.toString();
            return str2.endsWith(suffix.toString()) ? subPre(str2, str2.length() - suffix.length()) : str2;
        } else {
            return "";
        }
    }

    public static String subPre(CharSequence string, int toIndex) {
        return sub(string, 0, toIndex);
    }

    public static String sub(CharSequence str, int fromIndex, int toIndex) {
        if (str == null || "".contentEquals(str)) {
            return "";
        } else {
            int len = str.length();
            if (fromIndex < 0) {
                fromIndex += len;
                if (fromIndex < 0) {
                    fromIndex = 0;
                }
            } else if (fromIndex > len) {
                fromIndex = len;
            }

            if (toIndex < 0) {
                toIndex += len;
                if (toIndex < 0) {
                    toIndex = len;
                }
            } else if (toIndex > len) {
                toIndex = len;
            }

            if (toIndex < fromIndex) {
                int tmp = fromIndex;
                fromIndex = toIndex;
                toIndex = tmp;
            }

            return fromIndex == toIndex ? "" : str.toString().substring(fromIndex, toIndex);
        }
    }


    public static StringBuilder appendBuilder(StringBuilder sb, CharSequence... strs) {
        for (CharSequence str : strs) {
            sb.append(str);
        }
        return sb;
    }

    public static String join(Collection<?> coll, String delimiter,
                              String prefix, String suffix,
                              String emptyValue, boolean skipEmpty) {
        if (Func.isEmpty(coll)) {
            return emptyValue;
        }

        Iterator<?> iterator = coll.iterator();
        StringJoiner joiner = new StringJoiner(delimiter, prefix, suffix);

        if (null != emptyValue){
            joiner.setEmptyValue(emptyValue);
        }

        while (iterator.hasNext()) {
            Object next = iterator.next();
            if (skipEmpty) {
                if (null != next && isNotBlank(String.valueOf(next))) {
                    joiner.add(String.valueOf(next));
                }
            } else {
                joiner.add(String.valueOf(next));
            }
        }
        return joiner.toString();
    }

    public static String join(Collection<?> coll, String delimiter, boolean isSkipEmpty) {
        return join(coll, delimiter, "", "", "", isSkipEmpty);
    }

    public static String join(Collection<?> coll, String delimiter) {
        return join(coll, delimiter, false);
    }

    public static String join(Collection<?> coll) {
        return join(coll, ",");
    }

    public static <T> String join(T[] array) {
        return join(Arrays.asList(array), ",");
    }

    public static String join(CharSequence ...array) {
        return join(Arrays.asList(array), ",");
    }

    public static String join(String delimiter,CharSequence ...array) {
        return join(Arrays.asList(array), delimiter);
    }

    /**
     * 同 log 格式的 format 规则
     * <p>
     * use: format("my name is {}, and i like {}!", "L.cm", "Java")
     *
     * @param message   需要转换的字符串
     * @param arguments 需要替换的变量
     * @return 转换后的字符串
     */
    public static String format(@Nullable String message, @Nullable Object... arguments) {
        // message 为 null 返回空字符串
        if (message == null) {
            return StringPool.EMPTY;
        }
        // 参数为 null 或者为空
        if (arguments == null || arguments.length == 0) {
            return message;
        }
        StringBuilder sb = new StringBuilder((int) (message.length() * 1.5));
        int cursor = 0;
        int index = 0;
        int argsLength = arguments.length;
        for (int start, end; (start = message.indexOf(StringPool.LEFT_BRACE, cursor)) != -1 && (end = message.indexOf(StringPool.RIGHT_BRACE, start)) != -1 && index < argsLength; ) {
            sb.append(message, cursor, start);
            sb.append(arguments[index]);
            cursor = end + 1;
            index++;
        }
        sb.append(message.substring(cursor));
        return sb.toString();
    }

}
