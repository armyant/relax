package com.idea.relax.tool.core.enums;


/**
 * @author: 沉香
 * @date: 2023/5/8
 * @description:
 */
public interface IEnumValueBase<T> {


	T getValue();


}
