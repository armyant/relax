package com.idea.relax.tool.core;

import java.security.SecureRandom;
import java.util.Random;

/**
 * 单例对象
 *
 * @author salad
 */
public class SingletonFactory {

	/**
	 * OBJECT
	 */
	public final static Object OBJECT = new Object();

	/**
	 * RANDOM
	 */
	public final static Random RANDOM = new Random();

	/**
	 * SECURE_RANDOM
	 */
	public final static SecureRandom SECURE_RANDOM = new SecureRandom();


}
