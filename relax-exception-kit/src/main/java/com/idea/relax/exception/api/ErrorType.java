package com.idea.relax.exception.api;

import lombok.AllArgsConstructor;

/**
 * @author: 沉香
 * @date: 2023/2/21
 * @description: 内置的异常类型枚举类
 */
@AllArgsConstructor
public enum ErrorType implements IErrorType {

	/**
	 * 业务异常
	 */
	BUSINESS("B"),

	/**
	 * 系统异常
	 */
	SYSTEM("S");


	/**
	 * 异常类型
	 */
	private final String type;


	@Override
	public String getType() {
		return type;
	}

}
