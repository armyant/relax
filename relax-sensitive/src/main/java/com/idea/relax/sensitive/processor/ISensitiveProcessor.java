package com.idea.relax.sensitive.processor;

/**
 * @author: 沉香
 * @date: 2023/7/6
 * @description: 脱敏处理器，子类可以实现不同算法来进行脱敏
 */
public interface ISensitiveProcessor<T> {

	/**
	 * 对数据进行脱敏处理
	 *
	 * @param value
	 * @return
	 */
	T process(T value);

	/**
	 * 名称
	 *
	 * @return
	 */
	String name();

	/**
	 * 该处理器对什么类型生效
	 *
	 * @return
	 */
	Class<T> getTargetType();

}
