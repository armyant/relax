package com.idea.relax.datascope;

import org.apache.ibatis.mapping.BoundSql;

/**
 * @className: DataScopeConditionProcessor
 * @description: TODO 类描述
 * @author: salad
 * @date: 2022/11/13
 **/
public interface DataScopeConditionProcessor {

    String getCondition(DataScopeEnum type, String column, BoundSql boundSql, String targetSql);

}
