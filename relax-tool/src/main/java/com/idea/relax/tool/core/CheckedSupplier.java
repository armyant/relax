//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.idea.relax.tool.core;

import org.springframework.lang.Nullable;

@FunctionalInterface
public interface CheckedSupplier<T> {
    @Nullable
    T get() throws Throwable;
}
