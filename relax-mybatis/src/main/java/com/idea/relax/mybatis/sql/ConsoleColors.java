package com.idea.relax.mybatis.sql;

/**
 * @author salad
 */
public interface ConsoleColors {

	/**
	 * Text Reset
	 */
    String RESET = "\033[0m";

	/**
	 * BLACK
	 */
    String BLACK = "\033[0;30m";

	/**
	 * RED
	 */
    String RED = "\033[0;31m";

	/**
	 * GREEN
	 */
    String GREEN = "\033[0;32m";

	/**
	 * YELLOW
	 */
    String YELLOW = "\033[0;33m";

	/**
	 * BLUE
	 */
    String BLUE = "\033[0;34m";

	/**
	 * WHITE
	 */
    String WHITE = "\033[0;37m";

}
