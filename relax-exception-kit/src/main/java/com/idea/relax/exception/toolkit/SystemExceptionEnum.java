package com.idea.relax.exception.toolkit;

import com.idea.relax.exception.api.*;
import lombok.AllArgsConstructor;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 系统异常工具类
 */
@AllArgsConstructor
public enum SystemExceptionEnum {

	/**
	 * 高等级异常
	 */
	H(ErrorLevel.HIGH),

	/**
	 * 中等级异常
	 */
	M(ErrorLevel.MIDDLE),

	/**
	 * 低等级异常
	 */
	L(ErrorLevel.LOW);

	/**
	 *
	 */
	private final IErrorLevel errorLevel;

	/**
	 * 创建系统异常
	 *
	 * @param errorCode 异常码
	 * @param args      异常message的参数
	 * @return BusinessException
	 */
	public BaseException error(IErrorCode errorCode, Object... args) {
		return SystemExceptionHelper.error(errorLevel, errorCode, args);
	}

	/**
	 * 断言表达式为true的情况下创建系统异常
	 *
	 * @param expression 断言的表达式
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 */
	public void isTrueThrow(boolean expression, IErrorCode errorCode, Object... args) {
		SystemExceptionHelper.isTrueThrow(expression, errorLevel, errorCode, args);
	}

	/**
	 * 断言表达式为false的情况下创建系统异常
	 *
	 * @param expression 断言的表达式
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 */
	public void isFalseThrow(boolean expression, IErrorCode errorCode, Object... args) {
		SystemExceptionHelper.isFalseThrow(expression, errorLevel, errorCode, args);
	}

	/**
	 * 断言值为Empty的情况下创建系统异常
	 *
	 * @param value     断言的值
	 * @param errorCode 异常码
	 * @param args      异常message的参数
	 * @param <T>
	 */
	public <T> void isEmptyThrow(T value, IErrorCode errorCode, Object... args) {
		SystemExceptionHelper.isEmptyThrow(value, errorLevel, errorCode, args);
	}

	/**
	 * 断言值不为Empty的情况下创建系统异常
	 *
	 * @param value     断言的值
	 * @param errorCode 异常码
	 * @param args      异常message的参数
	 * @param <T>
	 */
	public <T> void isNotEmptyThrow(T value, IErrorLevel errorLevel, IErrorCode errorCode, Object... args) {
		SystemExceptionHelper.isNotEmptyThrow(value, errorLevel, errorCode, args);
	}


}
