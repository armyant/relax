package com.idea.relax.log;

/**
 * @className: RelaxLog
 * @description:
 * @author: salad
 * @date: 2023/1/25
 **/
public class RelaxLog {

    public static final String AUTHOR = "Salad";

    public static final String VERSION = "1.0.0-RELEASE";

	public static final String GITEE = "https://gitee.com/liuhao3169/relax.git";

}
