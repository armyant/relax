package com.idea.relax.exception.api;

/**
 * @author: 沉香
 * @date: 2023/5/24
 * @description: 异常码CodeSegment
 */
public interface ICodeSegment {

	/**
	 * 构成异常码的code片段
	 *
	 * @return 异常码片段
	 */
	String getCode();

	/**
	 * 该code片段的描述
	 *
	 * @return 描述
	 */
	String getDesc();

	/**
	 * 获取父级CodeSegment
	 *
	 * @return 父级CodeSegment
	 */
	default ICodeSegment leftCodeSegment() {
		return null;
	}

}
