package com.idea.relax.datascope.aron;

/**
 * @className: IDataAuthValueProvider
 * @description: TODO 类描述
 * @author: salad
 * @date: 2023/5/3
 **/
public interface IDataAuthValueProvider {

	String codeSupport();

	Object getValue(String code, String key);

}
