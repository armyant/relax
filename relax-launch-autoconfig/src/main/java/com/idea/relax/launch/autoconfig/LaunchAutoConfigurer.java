package com.idea.relax.launch.autoconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;


/**
 * @author: 沉香
 * @date: 2023/4/12
 * @description: 目的：简化yml配置，约定大于配置
 */
public interface LaunchAutoConfigurer extends Ordered {


	/**
	 * 自动配置
	 *
	 * @param configurableEnvironment
	 * @param application
	 */
	void autoconfig(ConfigurableEnvironment configurableEnvironment, SpringApplication application);

}
