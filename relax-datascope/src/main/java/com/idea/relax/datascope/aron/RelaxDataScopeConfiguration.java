package com.idea.relax.datascope.aron;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @className: RelaxDataScopeConfiguration
 * @description:
 * @author: salad
 * @date: 2022/11/8
 **/
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(RelaxDataScopeProperties.class)
public class RelaxDataScopeConfiguration {

	@Bean
	@ConditionalOnMissingBean
	public IDataScopeHandler dataScopeHandler(ObjectProvider<IDataAuthValueProvider> providers) {
		List<IDataAuthValueProvider> providerList = providers.stream().collect(Collectors.toList());
		return new DataScopeHandler(providerList);
	}

}
