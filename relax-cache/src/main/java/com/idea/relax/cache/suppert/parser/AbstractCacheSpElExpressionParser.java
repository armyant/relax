package com.idea.relax.cache.suppert.parser;

import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import com.idea.relax.cache.util.ValueUtil;
import com.idea.relax.tool.core.ClassUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.MethodParameter;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @className: AbstractCacheSpElExpressionParser
 * @description:
 * @author: salad
 * @date: 2022/10/9
 **/
public abstract class AbstractCacheSpElExpressionParser implements CacheExpressionParser{


    @Override
    public EvaluationContext getEvaluationContext(ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler){
        MethodSignature ms = (MethodSignature) joinPoint.getSignature();
        Signature signature = joinPoint.getSignature();
        Object target = joinPoint.getTarget();
        Class<?> targetClass = joinPoint.getTarget().getClass();
        String className = joinPoint.getTarget().getClass().getName();
        Method method = ms.getMethod();
        String methodName = signature.getName();
        Object[] args = joinPoint.getArgs();

        EvaluationContextRootObject rootObject = new EvaluationContextRootObject();
        rootObject.setClassName(className);
        rootObject.setArgs(args);
        rootObject.setMethodName(methodName);
        rootObject.setMethod(method);
        rootObject.setTarget(target);
        rootObject.setTargetClass(targetClass);

        StandardEvaluationContext context = new StandardEvaluationContext(rootObject);
        // 给上下文赋值
        for (int i = 0; i < args.length; i++) {
            // 读取方法参数
            MethodParameter methodParam = ClassUtil.getMethodParameter(method, i);
            context.setVariable(methodParam.getParameterName(), args[i]);
        }
        List<IFunction> funcList = handler.getFuncList();
        if (ValueUtil.isNotEmpty(funcList)){
            funcList.forEach(func -> {
                String name = func.getClass().getName();
                context.setVariable(name,func);
            });
        }
        return context;
    }



}
