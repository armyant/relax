package com.idea.relax.log.api.callback;

import com.idea.relax.log.support.CallbackMessage;

/**
 * @author: 沉香
 * @date: 2022/12/27
 * @description: 失败回调类，操作日志、通用日志、错误日志的存储如果失败，会回调onFailure方法
 */
@FunctionalInterface
public interface FailureCallback {

    /**
     * 操作日志、通用日志、错误日志的存储如果失败，会调用该方法
     *
     * @param callbackMessage 回调的相关信息
     */
    void onFailure(CallbackMessage callbackMessage);

}
