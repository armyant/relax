package com.idea.relax.tool.wechat.model;


import com.wechat.pay.java.service.payments.model.Transaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: 沉香
 * @date: 2023/3/7
 * @description:
 */
@Getter
@Setter
@AllArgsConstructor
public class OrderResponseDTO {


    private Transaction transaction;

}
