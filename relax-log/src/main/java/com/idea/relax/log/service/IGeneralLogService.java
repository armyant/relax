package com.idea.relax.log.service;


import com.idea.relax.log.model.GeneralLogModel;

/**
 * @className: IGeneralLogService
 * @description: 持久化通用日志的接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface IGeneralLogService{

    /**
     * 持久化通用日志
     * @param generalLogModel 通用日志Model对象
     */
    void saveGeneralLog(GeneralLogModel generalLogModel);

}
