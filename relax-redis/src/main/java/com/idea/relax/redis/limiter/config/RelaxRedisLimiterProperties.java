package com.idea.relax.redis.limiter.config;

import com.idea.relax.redis.limiter.constant.RedisLimiterConstant;
import com.idea.relax.redis.limiter.reject.LimiterRejectStrategy;
import com.idea.relax.redis.config.RelaxRedisProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @className: RelaxRedisLimiterProperties
 * @description:
 * @author: salad
 * @date: 2022/9/2
 **/
@Getter
@Setter
@ConfigurationProperties(RelaxRedisLimiterProperties.PREFIX)
public class RelaxRedisLimiterProperties {

    /**
     *
     */
    public static final String PREFIX = RelaxRedisProperties.PREFIX + ".limiter";

    /**
     * redis 限流 key 前缀
     */
    private String limiterPrefix = "limiter:";

    /**
     * 隔离模式
     */
    private boolean isolationMode = true;

    /**
     * 隔离模式表达式
     * 多个值会采用“:”分割，最大支持三个
     */
    private List<String> isolationExpression = RedisLimiterConstant.ISOLATION_EXPRESSION;

	/**
	 * 限流的拒绝策略
	 */
	private LimiterRejectStrategy limiterRejectStrategy = LimiterRejectStrategy.THROW_ERR;




}
