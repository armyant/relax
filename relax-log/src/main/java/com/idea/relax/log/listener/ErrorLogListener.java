package com.idea.relax.log.listener;


import com.idea.relax.log.event.ErrorLogEvent;
import com.idea.relax.log.props.RelaxLogProperties;
import com.idea.relax.log.publisher.ILogPublisher;
import com.idea.relax.log.service.IRelaxLogService;
import com.idea.relax.log.service.IErrorLogService;
import com.idea.relax.log.thread.RelaxLoggingThreadPoolManager;
import com.idea.relax.log.api.callback.RelaxFailureCallbackHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;

import java.util.Map;


/**
 * @className: ErrorLogListener
 * @description: 错误日志事件的监听
 * @author: salad
 * @date: 2022/10/7
 **/
@Slf4j
@AllArgsConstructor
public class ErrorLogListener {

    private final IErrorLogService errorLogService;

    private final IRelaxLogService relaxLogService;

    private final RelaxLogProperties props;

    private final RelaxLoggingThreadPoolManager threadPoolManager;

    private final RelaxFailureCallbackHandler failureCallbackHandler;

    @SuppressWarnings("unchecked")
    @EventListener(ErrorLogEvent.class)
    public void listener(ErrorLogEvent event) {
        ILogPublisher.Wrapper<Map<String, Object>> wrapper =
                (ILogPublisher.Wrapper<Map<String, Object>>) event.getSource();
        threadPoolManager.getThreadPool().execute(
                new ErrorLogWorker(wrapper, errorLogService,
                        relaxLogService, props, failureCallbackHandler
                ));
    }

}