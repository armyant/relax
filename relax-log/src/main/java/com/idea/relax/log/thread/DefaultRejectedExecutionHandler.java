package com.idea.relax.log.thread;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author: 沉香
 * @date: 2023/1/12
 * @description: 默认的线程池拒绝策略,将任务丢给提交任务的线程执行，即可以执行任务又可以延缓新任务的提交
 */
public class DefaultRejectedExecutionHandler implements RejectedExecutionHandler {

    /**
     * 实现自己的任务拒绝处理策略
     * CallRunsPolicy: 将任务丢给提交任务的线程执行，即可以执行任务又可以延缓新任务的提交
     */
    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        if (!executor.isShutdown()) {
            r.run();
        }
    }

}
