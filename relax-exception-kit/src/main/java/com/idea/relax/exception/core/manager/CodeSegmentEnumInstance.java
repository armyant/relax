package com.idea.relax.exception.core.manager;

import com.idea.relax.exception.api.ICodeSegment;

import java.util.Objects;

/**
 * @author: 沉香
 * @date: 2023/5/24
 * @description: 存储异常码片段枚举类信息
 */
public class CodeSegmentEnumInstance implements ICodeSegment {

	/**
	 * 异常码片段
	 */
	private String code;

	/**
	 * 描述
	 */
	private String desc;

	/**
	 * 父级异常码片段实例
	 */
	private CodeSegmentEnumInstance leftCodeSegment;

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public ICodeSegment leftCodeSegment() {
		return leftCodeSegment;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setLeftCodeSegment(CodeSegmentEnumInstance leftCodeSegment) {
		this.leftCodeSegment = leftCodeSegment;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CodeSegmentEnumInstance that = (CodeSegmentEnumInstance) o;
		return Objects.equals(code, that.code) &&
			Objects.equals(desc, that.desc) &&
			Objects.equals(leftCodeSegment, that.leftCodeSegment);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, desc, leftCodeSegment);
	}
}
