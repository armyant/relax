package com.idea.relax.datascope.aron;

import java.util.Arrays;
import java.util.List;

/**
 * @className: DefaultDataAuthValueProvider
 * @description: TODO 类描述
 * @author: salad
 * @date: 2023/5/3
 **/
public class DefaultDataAuthValueProvider implements IDataAuthValueProvider {

	@Override
	public String codeSupport() {
		//对哪个code生效
		return "user:list";
	}

	@Override
	public List<Long> getValue(String code, String key) {
		//根据code和key返回数据，可以查询数据库等操作
		return Arrays.asList(1L, 2L, 3L);
	}

}

