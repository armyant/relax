package com.idea.relax.log.support.utils;

import org.springframework.lang.Nullable;
import org.springframework.util.NumberUtils;

/**
 * @className: NumberUtil
 * @description: 数字转换工具类
 * @author: salad
 * @date: 2022/2/17
 **/
public class NumberUtil extends NumberUtils {

    /**
     * 转Int类型,假如转换失败返回0
     */
    public static int toInt(final Object value) {
        return toInt(value, 0);
    }

    /**
     * 转Int类型,假如转换失败返回defaultValue
     */
    public static int toInt(final Object value, final int defaultValue) {
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Integer.parseInt(String.valueOf(value));
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        }
    }

    /**
     * 转long类型,假如转换失败返回0
     */
    public static long toLong(final Object value) {
        return toLong(value, 0L);
    }

    /**
     * 转long类型,假如转换失败返回defaultValue
     */
    public static long toLong(final Object value, final long defaultValue) {
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Long.parseLong(String.valueOf(value));
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        }
    }

    /**
     * 转Double类型,假如转换失败返回0
     */
    public static Double toDouble(Object value) {
        return toDouble(value, 0D);
    }

    /**
     * 转Double类型,假如转换失败返回defaultValue
     */
    public static Double toDouble(Object value, Double defaultValue) {
        return value != null ? Double.valueOf(String.valueOf(value).trim()) : defaultValue;
    }

    /**
     * 转Float类型,假如转换失败返回0
     */
    public static Float toFloat(Object value) {
        return toFloat(value, 0F);
    }

    /**
     * 转Float类型,假如转换失败返回defaultValue
     */
    public static Float toFloat(Object value, Float defaultValue) {
        return value != null ? Float.valueOf(String.valueOf(value).trim()) : defaultValue;
    }

    /**
     * 对象转为 Boolean
     *
     * @param object Object
     * @return 结果
     */
    @Nullable
    public static Boolean toBoolean(@Nullable Object object) {
        return toBoolean(object, null);
    }

    /**
     * 对象转为 Boolean，支持 1、0，y、yes、n、no，on、off，true、false
     *
     * @param object       Object
     * @param defaultValue 默认值
     * @return 结果
     */
    @Nullable
    public static Boolean toBoolean(@Nullable Object object, @Nullable Boolean defaultValue) {
        if (object instanceof Boolean) {
            return (Boolean) object;
        } else if (object instanceof CharSequence) {
            String value = ((CharSequence) object).toString();
            if (StringPool.TRUE.equalsIgnoreCase(value) ||
                    StringPool.Y.equalsIgnoreCase(value) ||
                    StringPool.YES.equalsIgnoreCase(value) ||
                    StringPool.ON.equalsIgnoreCase(value) ||
                    StringPool.ONE.equalsIgnoreCase(value)) {
                return true;
            } else if (StringPool.FALSE.equalsIgnoreCase(value) ||
                    StringPool.N.equalsIgnoreCase(value) ||
                    StringPool.NO.equalsIgnoreCase(value) ||
                    StringPool.OFF.equalsIgnoreCase(value) ||
                    StringPool.ZERO.equalsIgnoreCase(value)) {
                return false;
            }
        }
        return defaultValue;
    }

    /**
     * 判断String是否是整数
     *
     * @param s String
     * @return 是否为整数
     */
    public static boolean isInteger(String s) {
        if (StringUtil.isNotBlank(s)) {
            return s.matches("^-?\\d+$");
        } else {
            return false;
        }
    }

    /**
     * 判断字符串是否是Long类型
     *
     * @param s String
     * @return 是否为{@link Long}类型
     * @since 4.0.0
     */
    public static boolean isLong(String s) {
        try {
            Long.parseLong(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 判断字符串是否是浮点数
     *
     * @param s String
     * @return 是否为{@link Double}类型
     */
    public static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
            return s.contains(".");
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 判断字符串是否是浮点数
     *
     * @param s String
     * @return 是否为{@link Float}类型
     */
    public static boolean isFloat(String s) {
        try {
            Float.parseFloat(s);
            return s.contains(".");
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isBaseType(Object val) {
        if (val instanceof Integer) {
            return true;
        }
        if (val instanceof Long) {
            return true;
        }
        if (val instanceof Boolean) {
            return true;
        }
        if (val instanceof Byte) {
            return true;
        }
        if (val instanceof Double) {
            return true;
        }
        if (val instanceof Character) {
            return true;
        }
        if (val instanceof Float) {
            return true;
        }
        return val instanceof Short;
    }


}
