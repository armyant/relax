package com.idea.relax.log.event;

import com.idea.relax.log.publisher.ILogPublisher;
import org.springframework.context.ApplicationEvent;

/**
 * @className: ApiLogEvent
 * @description: 操作日志的事件,当记录操作日志时，会发布该事件
 * @author: salad
 * @date: 2022/10/1
 **/
public class ApiLogEvent extends ApplicationEvent {

    public ApiLogEvent(ILogPublisher.Wrapper source) {
        super(source);
    }

}
