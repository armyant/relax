package com.idea.relax.boot.jackson;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@Getter
@Setter
@ConfigurationProperties(RelaxJacksonProperties.PREFIX)
public class RelaxJacksonProperties {


	public static final String PREFIX = "relax.boot.jackson";

	/**
	 * 是否对空值进行特殊处理序列化处理
	 */
	private Boolean useNullSerializers = Boolean.TRUE;

	/**
	 * 全局的datetime格式
	 */
 	private String globalDateTimeFormat = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 全局的date格式
	 */
 	private String globalDateFormat = "yyyy-MM-dd";

	/**
	 * 全局的time格式
	 */
 	private String globalTimeFormat = "HH:mm:ss";

}
