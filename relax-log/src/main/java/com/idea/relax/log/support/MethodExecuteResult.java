package com.idea.relax.log.support;

import com.idea.relax.log.support.utils.StringPool;
import lombok.Getter;

/**
 * @className: MethodExecuteResult
 * @description: 方法执行结果状态上下文对象
 * @author: salad
 * @date: 2022/12/11
 **/
@Getter
public class MethodExecuteResult {

    /**
     * 方法执行后的状态，error | success
     */
    private ResponseStatus responseStatus;

    /**
     * 执行出错时抛出的异常，只有方法执行失败才会记录方法的异常
     */
    private Throwable ex;

    /**
     * 方法的返回值，只有方法执行成功才会记录方法的返回值
     */
    private Object result;

    /**
     * 方法执行的耗时，无论方法执行成功还是报错都会统计耗时，单位：ms
     */
    private long time;


    public void setSuccess() {
        this.responseStatus = ResponseStatus.SUCCESS;
        this.result = StringPool.EMPTY;
    }

    public void setSuccess(Object result) {
        this.responseStatus = ResponseStatus.SUCCESS;
        this.result = result;
    }

    public void setSuccess(Object result, long time) {
        this.responseStatus = ResponseStatus.SUCCESS;
        this.result = result;
        this.time = time;
    }

    public void setError(Throwable ex, long time) {
        this.responseStatus = ResponseStatus.ERROR;
        this.ex = ex;
        this.time = time;
    }

    public void setError(Throwable ex) {
        this.responseStatus = ResponseStatus.ERROR;
        this.ex = ex;
    }

}
