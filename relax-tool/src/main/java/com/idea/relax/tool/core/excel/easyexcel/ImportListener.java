package com.idea.relax.tool.core.excel.easyexcel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @className: ImportListener
 * @description:
 * @author: salad
 * @date: 2022/2/18
 **/
public class ImportListener<T> extends AnalysisEventListener<T> {
    private int batchCount = 3000;
    private List<T> list = new ArrayList();
    private Importer<T> importer;

    public ImportListener(Importer<T> importer) {
        this.importer = importer;
    }

    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
        this.list.add(data);
        if (this.list.size() >= this.batchCount) {
            this.importer.save(this.list);
            this.list.clear();
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        this.importer.save(this.list);
        this.list.clear();
    }

    public int getBatchCount() {
        return this.batchCount;
    }

    public List<T> getList() {
        return this.list;
    }

    public void setBatchCount(final int batchCount) {
        this.batchCount = batchCount;
    }

    public void setList(final List<T> list) {
        this.list = list;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ImportListener;
    }

}


