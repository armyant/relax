package com.idea.relax.exception.viewer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

/**
 * @className: NodeModel
 * @description: 获取异常码接口的响应实体
 * @author: salad
 * @date: 2023/5/25
 **/
@Data
@NoArgsConstructor
public class NodeModel {

	/**
	 * 编码
	 */
	@JsonProperty("编码")
	private String code;

	/**
	 * 描述
	 */
	@JsonProperty("描述")
	private String desc;

	/**
	 * 子级异常码CodeSegment
	 */
	@JsonProperty("子级域")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Set<NodeModel> nodeModels;

	/**
	 * 子级异常码
	 */
	@JsonProperty("异常码")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Set<ErrorCodeModel> errorCodeModels;


	public NodeModel(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public void setNodeModels(Set<NodeModel> nodeModels) {
		this.nodeModels = nodeModels;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		NodeModel nodeModel = (NodeModel) o;
		return Objects.equals(code, nodeModel.code) && Objects.equals(desc, nodeModel.desc) && Objects.equals(nodeModels, nodeModel.nodeModels);
	}

	@Override
	public int hashCode() {
		return Objects.hash(code, desc, nodeModels);
	}
}
