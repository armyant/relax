package com.idea.relax.exception.viewer;

import com.idea.relax.exception.api.StringFormatter;

import java.util.List;

/**
 * @author: 沉香
 * @date: 2023/6/12
 * @description: 异常码文档HTML页面
 */
public class WelcomeHtml {


	private static final String WEL_HTML_TEMPLATE = "<!DOCTYPE html>\n" +
		"<head>\n" +
		"    <title>{}</title>\n" +
		"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
		"</head>\n" +
		"<body>\n" +
		"<main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 pt-3 px-4\">\n" +
		"    <div class=\"plan_box\">\n" +
		"        <span style=\"font-weight: 900;font-size: 20px;margin-left: 45%;\">{}</span>\n" +
		"        <table id=\"table\" >\n" +
		"            <thead>\n" +
		"            <tr>\n" +
		"                <th>异常码</th>\n" +
		"                <th>异常信息</th>\n" +
		"                <th>用户提示</th>\n" +
		"                <th>枚举类名</th>\n" +
		"                <th>枚举名称</th>\n" +
		"            </tr>\n" +
		"            </thead>\n" +
		"            <tbody>\n" +
		"            	{}" +
		"            </tbody>\n" +
		"        </table>\n" +
		"    </div>\n" +
		"</main>\n" +
		"</body>\n" +
		"<style>\n" +
		"    /*!*表格样式*!*/\n" +
		"    table {\n" +
		"        width: 60%;\n" +
		"        /*background: #ccc;*/\n" +
		"        margin: 10px auto;\n" +
		"        border-collapse: collapse;\n" +
		"        /*border-collapse:collapse合并内外边距\n" +
		"        (去除表格单元格默认的2个像素内外边距*/\n" +
		"    }\n" +
		"\n" +
		"    tr:hover {\n" +
		"        background: #e3e3e3;\n" +
		"    }\n" +
		"\n" +
		"    thead {\n" +
		"        height: 40px;\n" +
		"    }\n" +
		"\n" +
		"    table thead th {\n" +
		"        background-color: rgb(78, 144, 222);\n" +
		"        color: #fff;\n" +
		"        border-bottom-width: 0;\n" +
		"        font-weight: 1000;\n" +
		"    }\n" +
		"\n" +
		"    /* Column Style */\n" +
		"    table td {\n" +
		"        color: #000;\n" +
		"        border: 1px solid #b0b0b0;\n" +
		"    }\n" +
		"\n" +
		"    /* Heading and Column Style */\n" +
		"    table tr, table th {\n" +
		"        border-width: 1px;\n" +
		"        border-style: solid;\n" +
		"        border-color: rgb(177, 190, 204);\n" +
		"    }\n" +
		"\n" +
		"    /* Padding and font style */\n" +
		"    table th {\n" +
		"        padding: 5px 10px;\n" +
		"        font-size: 12px;\n" +
		"        font-family: Verdana;\n" +
		"        font-weight: bold;\n" +
		"    }\n" +
		"\n" +
		"    table td {\n" +
		"        padding: 5px 10px;\n" +
		"        font-family: Verdana;\n" +
		"    }\n" +
		"\n" +
		"</style>\n" +
		"</html>\n";

	/**
	 * 构建HTML页面
	 *
	 * @param docTitle 文档标题
	 * @param models   文档的页面数据
	 * @return HTML页面
	 */
	public static String buildWelHtml(String docTitle, List<ErrorCodeModel> models) {
		StringBuilder trs = new StringBuilder();
		for (ErrorCodeModel model : models) {
			String code = model.getCode() == null ? "" : model.getCode();
			String message = model.getMessage() == null ? "" : model.getMessage();
			String userTip = model.getUserTip() == null ? "" : model.getUserTip();
			String className = model.getClassName() == null ? "" : model.getClassName();
			String enumName = model.getEnumName() == null ? "" : model.getEnumName();
			trs.append("<tr>\n");
			trs.append("<td>").append(code).append("</td>\n");
			trs.append("<td>").append(message).append("</td>\n");
			trs.append("<td>").append(userTip).append("</td>\n");
			trs.append("<td>").append(className).append("</td>\n");
			trs.append("<td>").append(enumName).append("</td>\n");
			trs.append("</tr>\n");
		}
		return StringFormatter.format(WEL_HTML_TEMPLATE, docTitle, docTitle, trs);
	}

}
