package com.idea.relax.log.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @className: GeneralLogModel
 * @description: 通用日志Model对象
 * @author: salad
 * @date: 2022/6/1
 **/

@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GeneralLogModel extends LogCommonModel {

    protected static final long serialVersionUID = 1L;

    /**
     * 日志等级
     */
    private String logLevel;

    /**
     * 日志的标识
     */
    private String logId;

    /**
     * 日志信息
     */
    private String logData;


}
