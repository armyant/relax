package com.idea.relax.cache.annotation;

import java.lang.annotation.*;

/**
 * @className: Caching
 * @description:
 * @author: salad
 * @date: 2022/10/20
 **/
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Caching {

	Cacheable[] cacheable() default {};

	CachePut[] put() default {};

	CacheEvict[] evict() default {};

}