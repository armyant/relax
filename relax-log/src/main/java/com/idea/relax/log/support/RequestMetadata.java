package com.idea.relax.log.support;


import lombok.Data;

import java.util.Date;

/**
 * @author: 沉香
 * @date: 2023/1/6
 * @description: 请求相关的数据
 */
@Data
public class RequestMetadata {

    /**
     * 应用访问者的IP地址
     */
    private String visitorIp;

    /**
     * 请求的URI
     */
    private String requestUri;

    /**
     * 用户请求时的userAgent
     */
    private String userAgent;

    /**
     * 请求的方法类型，比如：GET | POST 等等
     */
    private String requestMethod;

    /**
     * 请求的参数
     */
    private String requestParams;

    /**
     * 请求时间
     */
    private Date requestTime;


}
