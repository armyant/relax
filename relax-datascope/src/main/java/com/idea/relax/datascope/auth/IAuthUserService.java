package com.idea.relax.datascope.auth;


import java.util.List;

/**
 * @className: IAuthUserService
 * @description:
 * @author: salad
 * @date: 2022/11/8
 **/
public interface IAuthUserService {

	boolean userOnline();

	Number getUserId();

	List<Number> getUserDept();

}

