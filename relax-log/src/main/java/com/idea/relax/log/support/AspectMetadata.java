package com.idea.relax.log.support;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * @author: 沉香
 * @date: 2023/1/6
 * @description: 切面相关的数据
 */
@Data
public class AspectMetadata {

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * Method对象
     */
    private Method method;

    /**
     * 方法的参数
     */
    private Object[] args;

    /**
     * 目标类的Class对象
     */
    private Class<?> targetClass;

    /**
     * 目标类的实例
     */
    private Object target;

}
