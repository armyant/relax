package com.idea.relax.log.props;

import ch.qos.logback.classic.Level;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: 沉香
 * @date: 2022/12/22
 * @description: 日志级别的枚举类
 */
@Getter
@AllArgsConstructor
public enum LogLevel {

    /**
     * 关闭
     */
    OFF(Level.OFF),
    /**
     * ERROR日志级别
     */
    ERROR(Level.ERROR),
    /**
     * WARN日志级别
     */
    WARN(Level.WARN),
    /**
     * INFO日志级别
     */
    INFO(Level.INFO),
    /**
     * DEBUG日志级别
     */
    DEBUG(Level.DEBUG),
    /**
     * TRACE日志级别
     */
    TRACE(Level.TRACE),
    /**
     * 全部日志级别
     */
    ALL(Level.ALL);


    private final Level level;


}
