package com.idea.relax.cache.suppert.parser;

import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

/**
 * @className: CacheSpElExpressionParser
 * @description:
 * @author: salad
 * @date: 2022/10/17
 **/
public class CacheSpElExpressionParser extends AbstractCacheSpElExpressionParser{

    @Override
    public Object parseExpression(String expression, ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler){
        EvaluationContext evaluationContext = getEvaluationContext(joinPoint, handler);
        Expression expressionObject = EXPRESSION_PARSER.parseExpression(expression);
        return expressionObject.getValue(evaluationContext);
    }

    @Override
    public String parseExpressionForKey(String expression, ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler) {
        EvaluationContext evaluationContext = getEvaluationContext(joinPoint, handler);
        Expression expressionObject = EXPRESSION_PARSER.parseExpression(expression);
        return expressionObject.getValue(evaluationContext,String.class);
    }

    @Override
    public Boolean parseExpressionForCondition(String expression, ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler) {
        EvaluationContext evaluationContext = getEvaluationContext(joinPoint, handler);
        Expression expressionObject = EXPRESSION_PARSER.parseExpression(expression);
        return expressionObject.getValue(evaluationContext,Boolean.TYPE);
    }


}
