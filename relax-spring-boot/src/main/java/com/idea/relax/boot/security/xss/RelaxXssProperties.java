package com.idea.relax.boot.security.xss;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;


/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@Getter
@Setter
@ConfigurationProperties(RelaxXssProperties.PREFIX)
public class RelaxXssProperties {

	public static final String PREFIX = "relax.boot.xss";

	private boolean enabled = true;

	private String[] urlPatterns = {"/*"};

	private List<String> skipUrls = new ArrayList<>();


}
