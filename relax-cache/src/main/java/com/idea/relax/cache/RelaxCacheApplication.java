package com.idea.relax.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelaxCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(RelaxCacheApplication.class, args);
    }

}
