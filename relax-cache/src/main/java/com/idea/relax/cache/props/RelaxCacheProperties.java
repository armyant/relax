package com.idea.relax.cache.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className: RelaxCacheProperties
 * @description:
 * @author: salad
 * @date: 2022/10/6
 **/
@Data
@ConfigurationProperties(prefix = "relax.cache")
public class RelaxCacheProperties {

    private final String keyPrefix = "";

    private final boolean cacheNullValue = false;

    private final boolean enableLock = true;

}
