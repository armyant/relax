# 🎉什么是Relax？

Relax的一款基于SpringBoot开发的企业级功能类库，开发者可按需引入自己需要的模块。

**工程结构**

```plain
relax
  ├── relax-cache -- 缓存模块
  ├── relax-crypto -- 报文加解密模块
  ├── relax-datascope -- 数据权限模块
  ├── relax-exception-kit -- 异常码工具包
  ├── relax-launch-autoconfig -- 自动配置模块
  ├── relax-log -- 日志模块
  ├── relax-mybatis -- mybatis plus工具包
  ├── relax-redis -- redis工具包
  ├── relax-spring-boot -- 围绕springboot相关的开发常用类库
  ├── relax-tool -- 常用工具类
```

# 🎄快速上手

Maven坐标基本与项目名本身相同，如relax-redis模块的坐标如下：

**具体的文档请看其对应的模块目录**

```java
<dependency>
    <groupId>com.idea</groupId>
    <artifactId>relax-redis</artifactId>
    <version>${version}</version>
</dependency>
```
# 🎁功能说明

目前已经Release的项目：

🍿relax-exception-kit

>异常码工具包，一款可以帮你统一系统异常码的宝贝，具有自动生成异常码文档功能，减少维护成本

🌭relax-log

>日志工具包，具有请求链路日志、操作日志等功能

🍳relax-mybatis

>对mybatis和mybatis-plus功能增强包

🍖relax-redis

>基于redis的增加功能包，提供注解式的redis分布式锁和redis限流等扩展功能

🍤relax-sensitive

>注解式的数据脱敏工具包，提供各种脱敏算法（可拓展）

🍦relax-spring-boot

>对于spring-boot项目的开箱自动配置与功能增强，让你新建项目时无需关心各种组件的配置问题
