package com.idea.relax.exception.core.registry;

import com.idea.relax.exception.constant.EkConstant;
import com.idea.relax.launch.autoconfig.ServerUtil;

/**
 * @author: 沉香
 * @date: 2023/6/8
 * @description:
 */
class ConsoleBannerUtil {


	/**
	 * 打印异常码文档
	 */
	protected static void printBanner() {
		System.out.println("\033[0;32m\n----------------------------------------------------------\n\t" +
			"Exception-Kit异常码:\n\t" +
			"本地文档地址（列表）: \thttp://localhost:" + ServerUtil.getServerMetadata().getPort() + EkConstant.VIEW_URL + "\n\t" +
			"外部文档地址（列表）: \thttp://" + ServerUtil.getServerMetadata().getServerAddr() + EkConstant.VIEW_URL + "\n\t" +
			"本地文档地址（JSON）: \thttp://localhost:" + ServerUtil.getServerMetadata().getPort() + EkConstant.VIEW_URL + "/tree\n\t" +
			"外部文档地址（JSON）: \thttp://" + ServerUtil.getServerMetadata().getServerAddr() + EkConstant.VIEW_URL + "/tree\n" +
			"----------------------------------------------------------\033[0m");
	}

}
