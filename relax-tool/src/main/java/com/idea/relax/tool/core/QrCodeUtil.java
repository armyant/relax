package com.idea.relax.tool.core;

import com.github.hui.quick.plugin.base.GraphicUtil;
import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeGenWrapper;
import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeOptions;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.function.Consumer;

/**
 * @className: QrCodeUtil
 * @description: 二维码工具类
 * @author: salad
 * @date: 2022/3/22
 **/
public class QrCodeUtil {

    /**
     * 简单的二维码生成
     *
     * @param fullPath  生成的全路径
     * @param content   二维码内容
     * @param imageName 文件名,包括文件名后缀
     * @param w         宽度
     * @param h         高度
     */
    public static QRCodeGenerated generate(String fullPath, String content,
                                           String imageName, int w, int h) {
        try {
            QrCodeGenWrapper.Builder builder = QrCodeGenWrapper
                    .of(content).setW(w).setH(h)
                    //设置二维码容错度;
                    .setErrorCorrection(ErrorCorrectionLevel.H);
            if (!fullPath.endsWith("/")) {
                fullPath += "/";
            }
            File pathFile = new File(fullPath);
            if (!pathFile.exists()) {
                pathFile.mkdir();
            }
            fullPath += imageName;
            if (builder.asFile(fullPath))
                return QRCodeGenerated.success(imageName, fullPath);
            return QRCodeGenerated.fail(imageName, null);
        } catch (Exception e) {
            e.printStackTrace();
            return QRCodeGenerated.fail(imageName, null);
        }
    }

    /**
     * 二维码生成，支持中心图片logo和底部文字
     *
     * @param fullPath    生成的全路径
     * @param content     二维码内容
     * @param bottomText  底部文字
     * @param imageName   文件名,包括文件名后缀
     * @param w           宽度
     * @param h           高度
     * @param logoAbsPath logo的存放路径，包括文件名
     */
    public static QRCodeGenerated generate(String fullPath, String content, String bottomText,
                                           String imageName, int w, int h, String logoAbsPath) {
        try {
            QrCodeGenWrapper.Builder builder = QrCodeGenWrapper
                    .of(content).setW(w).setH(h)
                    .setErrorCorrection(ErrorCorrectionLevel.H); //设置二维码容错度;
            if (StringUtil.isNotBlank(logoAbsPath)) {
                builder.setLogo(logoAbsPath)
                        .setLogoStyle(QrCodeOptions.LogoStyle.CIRCLE) //logo默认圆形
                        .setLogoBgColor(0xfffefefe)
                        .setLogoBorderBgColor(0xfffefefe)
                        .setLogoBorder(true);
            }
            addBottomText(bottomText, w, h, builder);
            if (!fullPath.endsWith("/")) {
                fullPath += "/";
            }
            File pathFile = new File(fullPath);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }
            fullPath += imageName;
            if (builder.asFile(fullPath))
                return QRCodeGenerated.success(imageName, fullPath);
            return QRCodeGenerated.fail(imageName, null);
        } catch (Exception e) {
            e.printStackTrace();
            return QRCodeGenerated.fail(imageName, null);
        }
    }

    /**
     * 二维码生成，支持中心图片logo和底部文字
     *
     * @param content     二维码内容
     * @param bottomText  底部文字
     * @param w           宽度
     * @param h           高度
     * @param logoAbsPath logo的存放路径，包括文件名
     */
    public static QrCodeGenWrapper.Builder generate(String content, String bottomText,
                                                    int w, int h, String logoAbsPath) {
        try {
            QrCodeGenWrapper.Builder builder = QrCodeGenWrapper
                    .of(content).setW(w).setH(h)
                    .setErrorCorrection(ErrorCorrectionLevel.H); //设置二维码容错度;
            if (StringUtil.isNotBlank(logoAbsPath)) {
                builder.setLogo(logoAbsPath)
                        .setLogoStyle(QrCodeOptions.LogoStyle.CIRCLE) //logo默认圆形
                        .setLogoBgColor(0xfffefefe)
                        .setLogoBorderBgColor(0xfffefefe)
                        .setLogoBorder(true);
            }
            addBottomText(bottomText, w, h, builder);
            return builder;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void addBottomText(String bottomText, int w, int h, QrCodeGenWrapper.Builder builder) {
        if (StringUtil.isNotBlank(bottomText)) {
            //  创建一个文字的前置图
            BufferedImage ftImg = GraphicUtil.createImg(w, h + 60, null);
            Graphics2D g2d = GraphicUtil.getG2d(ftImg);
            g2d.setColor(Color.WHITE);
            g2d.fillRect(0, 500, 500, 500);
            g2d.setColor(Color.BLACK);
            g2d.setFont(new Font(null, Font.BOLD, 40));
            int x = (w - (bottomText.length() * 25)) / 2;
            int y = h - (h - w - 11) / 2;
            g2d.drawString(bottomText, x, y);
            builder.setFtImg(ftImg)
                    .setFtFillColor(Color.WHITE);
        }
    }

    /**
     * 二维码生成，支持中心图片logo和底部文字
     *
     * @param fullPath     生成的全路径
     * @param content      二维码内容
     * @param bottomText   底部文字
     * @param imageName    文件名,包括文件名后缀
     * @param w            宽度
     * @param h            高度
     * @param logoFullPath logo的存放路径，包括文件名
     * @param consumer     自定义设置额外的builder对象属性
     */
    public static QRCodeGenerated generate(String fullPath, String content, String bottomText,
                                           String imageName, int w, int h, String logoFullPath,
                                           Consumer<QrCodeGenWrapper.Builder> consumer) {
        try {
            QrCodeGenWrapper.Builder builder = QrCodeGenWrapper
                    .of(content).setW(w).setH(h)
                    .setErrorCorrection(ErrorCorrectionLevel.H);
            if (StringUtil.isNotBlank(logoFullPath)) {
                builder.setLogo(logoFullPath);
                consumer.accept(builder);
            }
            addBottomText(bottomText, w, h, builder);
            if (!fullPath.endsWith("/")) {
                fullPath += "/";
            }
            File pathFile = new File(fullPath);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }
            fullPath += imageName;
            if (builder.asFile(fullPath))
                return QRCodeGenerated.success(imageName, fullPath);
            return QRCodeGenerated.fail(imageName, null);
        } catch (Exception e) {
            e.printStackTrace();
            return QRCodeGenerated.fail(imageName, null);
        }
    }

    public static class QRCodeGenerated {
        private boolean successful;
        private String name;
        private String fullPath;

        public QRCodeGenerated(boolean successful, String name, String fullPath) {
            this.successful = successful;
            this.name = name;
            this.fullPath = fullPath;
        }

        public boolean isSuccessful() {
            return successful;
        }

        public void setSuccessful(boolean successful) {
            this.successful = successful;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFullPath() {
            return fullPath;
        }

        public void setFullPath(String fullPath) {
            this.fullPath = fullPath;
        }

        public static QRCodeGenerated fail(String name, String fullPath) {
            return new QRCodeGenerated(false, name, fullPath);
        }

        public static QRCodeGenerated success(String name, String fullPath) {
            return new QRCodeGenerated(true, name, fullPath);
        }
    }
}
