package com.idea.relax.exception.core.manager;

import com.idea.relax.exception.api.ICodeSegment;
import com.idea.relax.exception.api.IErrorCode;

import java.util.Objects;

/**
 * @author: 沉香
 * @date: 2023/5/26
 * @description: 存储异常码枚举类信息
 */
public class ErrorCodeEnumInstance implements IErrorCode {

	/**
	 * 枚举值名称
	 */
	private String enumName;

	/**
	 * 枚举类
	 */
	private String className;

	/**
	 * HTTP状态码
	 */
	private Integer code;

	/**
	 * 最子级的异常码
	 */
	private String rightErrorCode;

	/**
	 * 异常消息
	 */
	private String message;

	/**
	 * 用户提示
	 */
	private String userTip;

	/**
	 * 父级异常码
	 */
	private ICodeSegment leftCodeSegment;


	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getEnumName() {
		return enumName;
	}

	public void setEnumName(String enumName) {
		this.enumName = enumName;
	}

	@Override
	public String getErrorCode() {
		return rightErrorCode;
	}

	public void setErrorCode(String errorCode) {
		this.rightErrorCode = errorCode;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public void setUserTip(String userTip) {
		this.userTip = userTip;
	}

	@Override
	public String getUserTip() {
		return userTip;
	}

	@Override
	public ICodeSegment leftCodeSegment() {
		return leftCodeSegment;
	}

	public void setLeftCodeSegment(ICodeSegment leftCodeSegment) {
		this.leftCodeSegment = leftCodeSegment;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o){
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ErrorCodeEnumInstance that = (ErrorCodeEnumInstance) o;
		return Objects.equals(enumName, that.enumName) && Objects.equals(code, that.code) && Objects.equals(rightErrorCode, that.rightErrorCode) && Objects.equals(message, that.message) && Objects.equals(userTip, that.userTip) && Objects.equals(leftCodeSegment, that.leftCodeSegment);
	}

	@Override
	public int hashCode() {
		return Objects.hash(enumName, code, rightErrorCode, message, userTip, leftCodeSegment);
	}
}
