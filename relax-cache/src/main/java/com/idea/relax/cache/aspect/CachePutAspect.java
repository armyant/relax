package com.idea.relax.cache.aspect;

import com.idea.relax.cache.annotation.CacheEvict;
import com.idea.relax.cache.annotation.CachePut;
import com.idea.relax.cache.cache.Cache;
import com.idea.relax.cache.props.RelaxCacheProperties;
import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import com.idea.relax.cache.suppert.parser.CacheExpressionParser;
import com.idea.relax.cache.util.ValueUtil;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

/**
 * @className: CachePutAspect
 * @description:
 * @author: salad
 * @date: 2022/10/13
 **/
@Aspect
@AllArgsConstructor
public class CachePutAspect {

    private final Cache cache;

    private final CacheExpressionParser cacheExpressionParser;

    private final RelaxCacheProperties properties;

    private final IFunctionHandler<IFunction> handler;

    @Around("@within(com.idea.relax.cache.annotation.CachePut) " +
            "|| @annotation(com.idea.relax.cache.annotation.CachePut)")
    public Object cachePutAround(ProceedingJoinPoint point) throws Throwable {
        Signature signature = point.getSignature();
        String methodName = signature.getName();
        MethodSignature methodSignature = (MethodSignature) signature;
        CachePut cachePut = AnnotationUtils.findAnnotation(methodSignature.getMethod(), CachePut.class);
        if (null == cachePut){
            cachePut = AnnotationUtils.findAnnotation(methodSignature.getClass(),CachePut.class);
        }
        Object[] args = point.getArgs();
        Assert.notNull(cachePut,"");
        String name = cachePut.cacheNames();
        String key = cachePut.key();
        String condition = cachePut.condition();
        String keyPrefix = properties.getKeyPrefix();
        Boolean isPut = Optional.ofNullable(cacheExpressionParser
                .parseExpressionForCondition(condition, point, handler)).orElse(Boolean.FALSE);
        if (!isPut){
            return point.proceed();
        }
        if (ValueUtil.isEmpty(key)) {
            key = methodName.concat(":").concat(Arrays.toString(args));
        }else {
            key = cacheExpressionParser.parseExpressionForKey(key, point, handler);
        }
        if (ValueUtil.isNotEmpty(keyPrefix)){
            key = keyPrefix.concat(key);
        }
        String cacheKey = name.concat(":").concat(key);
        Object result = point.proceed();
        cache.put(cacheKey,result);
        return result;
    }



}
