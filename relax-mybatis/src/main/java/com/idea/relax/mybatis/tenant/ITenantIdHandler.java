package com.idea.relax.mybatis.tenant;

import net.sf.jsqlparser.expression.Expression;

/**
 * @className: ITenantIdHandler
 * @description: TODO 类描述
 * @author: salad
 * @date: 2023/4/30
 **/
public interface ITenantIdHandler {

	Expression getTenantId();

}
