package com.idea.relax.redis.limiter.reject;

import java.util.concurrent.TimeUnit;

/**
 * @author: 沉香
 * @date: 2023/2/14
 * @description: 被限流后的拒绝策略
 */
@FunctionalInterface
public interface RedisLimiterRejectStrategy {

	/**
	 * 拒绝策略
	 * @param key
	 * @param count
	 * @param period
	 * @param timeUnit
	 * @return
	 */
    Object reject(String key, long count, long period, TimeUnit timeUnit);

}
