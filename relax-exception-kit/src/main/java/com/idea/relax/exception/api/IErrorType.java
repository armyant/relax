package com.idea.relax.exception.api;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 异常类型基类
 */
public interface IErrorType {

	/**
	 * 获取异常类型
	 *
	 * @return 异常类型
	 */
	String getType();

}
