package com.idea.relax.tool.wechat.model;

import lombok.Data;

/**
 * @author: 沉香
 * @date: 2023/3/7
 * @description:
 */
@Data
public class WechatPayCallbackVO {

    /**
     * 回调通知id
     */
    private String id;

    /**
     * 通知创建的时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE
     */
    private String createTime;

    /**
     * 通知的类型，支付成功通知的类型为TRANSACTION.SUCCESS
     */
    private String eventType;

    /**
     * 通知的资源数据类型，支付成功通知为encrypt-resource
     */
    private String resourceType;

    /**
     * 回调摘要
     * 示例值：支付成功
     */
    private String summary;

    /**
     * 通知资源数据
     * json格式
     */
    private WechatPayResourceVO resource;

}
