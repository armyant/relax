package com.idea.relax.cache.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @className: CacheEvict
 * @description:
 * @author: salad
 * @date: 2022/10/11
 **/
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheEvict {


    @AliasFor("cacheNames")
    String value();


    @AliasFor("value")
    String cacheNames();


    String key() default "";


    String condition() default "";


    boolean allEntries() default false;

    /**
     * 是否在调用方法之前就清空缓存
     */
    boolean beforeInvocation() default false;


}
