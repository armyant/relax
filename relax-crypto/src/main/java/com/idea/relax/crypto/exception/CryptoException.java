package com.idea.relax.crypto.exception;

/**
 * @author: 沉香
 * @date: 2023/2/28
 * @description:
 */
public class CryptoException extends RuntimeException {

	public CryptoException(final String message) {
		super(message);
	}

}
