package com.idea.relax.tool.core.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * <p>通用Builder</p>
 *
 * @author salad
 */
public class GenericBuilder<T> implements Builder<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * 实例化器
	 */
	private final Supplier<T> instant;

	/**
	 * 修改器列表
	 */
	private final List<Consumer<T>> modifiers = new ArrayList<>();

	/**
	 * 构造
	 *
	 * @param instant 实例化器
	 */
	public GenericBuilder(Supplier<T> instant) {
		this.instant = instant;
	}

	/**
	 * 通过无参数实例化器创建GenericBuilder
	 *
	 * @param instant 实例化器
	 * @param <T>     目标类型
	 * @return GenericBuilder对象
	 */
	public static <T> GenericBuilder<T> of(Supplier<T> instant) {
		return new GenericBuilder<>(instant);
	}


	/**
	 * 调用无参数方法
	 *
	 * @param consumer 无参数Consumer
	 * @return GenericBuilder对象
	 */
	public GenericBuilder<T> with(Consumer<T> consumer) {
		modifiers.add(consumer);
		return this;
	}


	/**
	 * 调用1参数方法
	 *
	 * @param consumer 1参数Consumer
	 * @param p1       参数一
	 * @param <P1>     参数一类型
	 * @return GenericBuilder对象
	 */
	public <P1> GenericBuilder<T> with(BiConsumer<T, P1> consumer, P1 p1) {
		modifiers.add(instant -> consumer.accept(instant, p1));
		return this;
	}


	/**
	 * 构建
	 *
	 * @return 目标对象
	 */
	@Override
	public T build() {
		T value = instant.get();
		modifiers.forEach(modifier -> modifier.accept(value));
		modifiers.clear();
		return value;
	}


}
