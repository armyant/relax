package com.idea.relax.boot.async;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;


/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@Getter
@Setter
@ConfigurationProperties(RelaxAsyncProperties.PREFIX)
public class RelaxAsyncProperties {


	public static final String PREFIX = "relax.boot.async";

	/**
	 * 是否开启
	 */
	private boolean enabled = true;

	/**
	 * 核心线程数
	 */
	private Integer corePoolSize = 2;

	/**
	 * 最大线程数
	 */
	private Integer maxPoolSize = 50;

	/**
	 * 队列容量
	 */
	private Integer queueCapacity = 10000;

	/**
	 * 存活时间
	 */
	private Integer keepAliveSeconds = 300;

	/**
	 * 线程名称前缀
	 */
	private String threadNamePrefix = "relax-boot-";


}
