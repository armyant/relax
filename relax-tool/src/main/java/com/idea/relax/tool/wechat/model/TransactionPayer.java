package com.idea.relax.tool.wechat.model;

import lombok.Data;

/**
 * TransactionPayer
 */
@Data
public class TransactionPayer {
    /**
     * openid
     */
    private String openid;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }


}
