package com.idea.relax.log.logger;


import com.idea.relax.log.publisher.GeneralLogPublisher;
import com.idea.relax.log.support.utils.Func;
import com.idea.relax.log.support.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @className: RelaxLogger
 * @description: 通用日志工具类
 * @author: salad
 * @date: 2022/6/8
 **/
@Slf4j
public class RelaxLogger {


	/**
	 * debug级别日志
	 *
	 * @param logId   日志标识
	 * @param logData 要记录的日志数据
	 * @param args    日志参数
	 */
	public void debug(String logId, String logData, Object... args) {
		log(LogLevel.DEBUG, logId, logData, args);
	}

	/**
	 * info级别日志
	 *
	 * @param logId   日志标识
	 * @param logData 要记录的日志数据
	 * @param args    日志参数
	 */
	public void info(String logId, String logData, Object... args) {
		log(LogLevel.INFO, logId, logData, args);
	}

	/**
	 * warn级别日志
	 *
	 * @param logId   日志标识
	 * @param logData 要记录的日志数据
	 * @param args    日志参数
	 */
	public void warn(String logId, String logData, Object... args) {
		log(LogLevel.WARN, logId, logData, args);
	}

	/**
	 * error级别日志
	 *
	 * @param logId   日志标识
	 * @param logData 要记录的日志数据
	 * @param args    日志参数
	 */
	public void error(String logId, String logData, Throwable e, Object... args) {
		log(LogLevel.ERROR, logId, logData, e, args);
	}


	/**
	 * 可指定级别的日志
	 *
	 * @param logLevel 日志等级
	 * @param logId    日志标识
	 * @param logData  要记录的日志数据
	 * @param args     日志数据中要填充的参数
	 */
	public void log(LogLevel logLevel, String logId, String logData, Object... args) {
		log(logLevel, logId, logData, null, args);
	}

	/**
	 * 可指定级别的日志
	 *
	 * @param logLevel 日志等级
	 * @param logId    日志标识
	 * @param logData  要记录的日志数据
	 * @param e        异常（可选）
	 * @param args     日志数据中要填充的参数
	 */
	public void log(LogLevel logLevel, String logId, String logData, Throwable e, Object... args) {
		if (Func.hasEmpty(logLevel, logId, logData)) {
			throw new IllegalArgumentException("logLevel, logId, and logData are mandatory fields！");
		}
		String logDataFmt = StringUtil.format(logData, args);
		String logText = logId.concat(":").concat(logDataFmt);
		switch (logLevel) {
			case INFO:
				log.info(logText);
				break;
			case WARN:
				log.warn(logText);
				break;
			case ERROR:
				if (null != e) {
					log.error(logText, e);
				} else {
					log.error(logText);
				}
				break;
			default:
				log.debug(logText);
				break;
		}
		GeneralLogPublisher.build(logId, logDataFmt, logLevel).publish();
	}


}
