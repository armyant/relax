package com.idea.relax.log.service;


import com.idea.relax.log.model.ApiLogModel;

/**
 * @className: IApiLogService
 * @description: 持久化操作日志的接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface IApiLogService{

    /**
     * 持久化操作日志
     * @param apiLogModel 操作日志Model对象
     */
    void saveApiLog(ApiLogModel apiLogModel);

}
