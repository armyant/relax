//package com.idea.relax.exception.api;
//
//import com.idea.relax.exception.toolkit.RelaxUtil;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
///**
// * @author: 沉香
// * @date: 2023/2/20
// * @description:
// */
//public class ErrorCode3 implements IErrorCode {
//
//	private String code;
//
//	private String message = "";
//
//	private String tip;
//
//	private IErrorCode alterErrorCode;
//
//	private IErrorCode beforeErrorCode;
//
//
//	public ErrorCode3() {
//
//	}
//
//	//========================
//	//完全自定义code
//	//========================
//
//	/**
//	 * 设置新的ErrorCode
//	 *
//	 * @param newCode
//	 * @return
//	 */
//	public static IErrorCode code(IErrorCode newCode) {
//		ErrorCode3 errorCode = new ErrorCode3();
//		errorCode.setCode(newCode.getCode());
//		errorCode.setMessage(newCode.getMessage());
//		return errorCode;
//	}
//
//
//	/**
//	 * 设置新的code
//	 *
//	 * @param code
//	 * @return
//	 */
//	@Override
//	public IErrorCode setCode(String code) {
//		this.code = RelaxUtil.join(Collections.singletonList(code), "");
//		return this;
//	}
//
//	/**
//	 * 设置异常信息
//	 *
//	 * @param message
//	 * @return
//	 */
//	@Override
//	public IErrorCode setMessage(String message) {
//		this.message = message;
//		return this;
//	}
//
//	@Override
//	public String getTip() {
//		return tip;
//	}
//
//	@Override
//	public IErrorCode setTip(String tip) {
//		this.tip = tip;
//		return this;
//	}
//
//	@Override
//	public String getCode() {
//		String code = this.code;
//		if (this.alterErrorCode != null) {
//			code = joinErrorCodes(true, this.alterErrorCode);
//		}
//		if (this.beforeErrorCode != null) {
//			code = joinErrorCodes(false, this.beforeErrorCode);
//		}
//		return code;
//	}
//
//	@Override
//	public String getMessage() {
//		String message = this.message;
//		if (this.alterErrorCode != null) {
//			message = this.alterErrorCode.getMessage();
//		}
//		if (this.beforeErrorCode != null) {
//			message = this.beforeErrorCode.getMessage();
//		}
//		return message;
//	}
//
//
//
//	private String joinErrorCodes(boolean after,IErrorCode... codes) {
//		StringBuffer original = new StringBuffer(code);
//		List<String> codeSegmentList = new ArrayList<>();
//		if (after) {
//			for (IErrorCode code : codes) {
//				original.append(code.getCode());
//			}
//			return original.toString();
//		} else {
//			for (int i = 0; i < codes.length; i++) {
//				codeSegmentList.add(i, codes[i].getCode());
//			}
//			return RelaxUtil.join(codeSegmentList, "").concat(original.toString());
//		}
//	}
////
////	private String joinCodeSegment(boolean after, ICodeSegment... codeSegments) {
////		StringBuffer original = new StringBuffer(code);
////		List<String> codeSegmentList = new ArrayList<>();
////		if (after) {
////			for (ICodeSegment codeSegment : codeSegments) {
////				original.append(codeSegment.getSegment());
////			}
////			return original.toString();
////		} else {
////			for (int i = 0; i < codeSegments.length; i++) {
////				codeSegmentList.add(i, codeSegments[i].getSegment());
////			}
////			return RelaxUtil.join(codeSegmentList, "").concat(original.toString());
////		}
////	}
//
//
//}
