package com.idea.relax.cache.suppert.func;

import java.util.List;

/**
 * @className: IFunctionHandler
 * @description:
 * @author: salad
 * @date: 2022/10/16
 **/
public interface IFunctionHandler<T> {

    List<T> getFuncList();

}
