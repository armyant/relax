package com.idea.relax.boot.config;

import com.idea.relax.boot.security.sql.RelaxSqlInjectProperties;
import com.idea.relax.boot.security.sql.SqlInjectRequestFilter;
import com.idea.relax.boot.security.xss.RelaxXssProperties;
import com.idea.relax.boot.security.xss.XssRequestFilter;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import javax.servlet.DispatcherType;

/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@AllArgsConstructor
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(RelaxSqlInjectProperties.class)
public class SqlInjectConfiguration {

	private final RelaxSqlInjectProperties sqlInjectProperties;

	/**
	 * SQL注入过滤器
	 */
	@Bean
	public FilterRegistrationBean<SqlInjectRequestFilter> sqlInjectFilterRegistrationBean() {
		FilterRegistrationBean<SqlInjectRequestFilter> registration = new FilterRegistrationBean<>();
		registration.setDispatcherTypes(DispatcherType.REQUEST);
		registration.setFilter(new SqlInjectRequestFilter(sqlInjectProperties));
		registration.addUrlPatterns(sqlInjectProperties.getUrlPatterns());
		registration.setName("sqlInjectRequestFilter");
		registration.setOrder(Ordered.LOWEST_PRECEDENCE);
		return registration;
	}

}
