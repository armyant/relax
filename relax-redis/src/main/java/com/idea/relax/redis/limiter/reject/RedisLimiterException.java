package com.idea.relax.redis.limiter.reject;

import com.idea.relax.redis.support.utils.RelaxUtil;
import lombok.Getter;

import java.util.concurrent.TimeUnit;

/**
 * 限流异常
 *
 * @author
 */
@Getter
public class RedisLimiterException extends RuntimeException {
    private final String key;
    private final long count;
    private final long period;
    private final TimeUnit timeUnit;

    public RedisLimiterException(String errorMessage, String key, long count, long period, TimeUnit timeUnit) {
        super(RelaxUtil.format(errorMessage, key, String.valueOf(count / timeUnit.toSeconds(period))));
        this.key = key;
        this.count = count;
        this.period = period;
        this.timeUnit = timeUnit;
    }
}
