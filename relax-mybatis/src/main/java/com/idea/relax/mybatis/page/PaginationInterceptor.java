package com.idea.relax.mybatis.page;

import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.idea.relax.mybatis.interceptor.QueryInterceptor;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.sql.SQLException;
import java.util.List;

/**
 * @className: PageInterceptor
 * @description:
 * @author: salad
 * @date: 2023/4/8
 **/
@Setter
@Getter
public class PaginationInterceptor extends PaginationInnerInterceptor {

	private List<QueryInterceptor> queryInterceptors;

	@Override
	public boolean willDoQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
		this.queryInterceptorExec(executor, ms, parameter, rowBounds, resultHandler, boundSql);
		return super.willDoQuery(executor, ms, parameter, rowBounds, resultHandler, boundSql);
	}

	private void queryInterceptorExec(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
		if (null != queryInterceptors && !queryInterceptors.isEmpty()) {
			for (QueryInterceptor interceptor : queryInterceptors) {
				interceptor.intercept(executor, ms, parameter, rowBounds, resultHandler, boundSql);
			}
		}
	}

}
