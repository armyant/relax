package com.idea.relax.crypto.core;

import java.security.SecureRandom;

/**
 * 单例对象
 *
 * @author salad
 */
class SingletonFactory {

	/**
	 * SECURE_RANDOM
	 */
	public final static SecureRandom SECURE_RANDOM = new SecureRandom();


}
