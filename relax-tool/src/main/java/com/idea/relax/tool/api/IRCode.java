package com.idea.relax.tool.api;

import java.io.Serializable;

/**
 * 业务代码接口
 * @author salad
 */
public interface IRCode extends Serializable {

	/**
	 * 消息
	 *
	 * @return String
	 */
	String getMessage();

	/**
	 * 状态码
	 *
	 * @return int
	 */
	int getCode();

}
