package com.idea.relax.redis.support.proto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: ProtoWrapper
 * @description: Protostuff 无法直接序列化集合类对象，需要包装类包一下
 * @author: salad
 * @date: 2022/9/7
 **/
@Getter
@AllArgsConstructor
public class ProtoWrapper<T> {

    private final T value;

}
