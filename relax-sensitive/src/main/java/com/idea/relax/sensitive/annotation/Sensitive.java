package com.idea.relax.sensitive.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.idea.relax.sensitive.serializer.SensitiveSerializer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author: 沉香
 * @date: 2023/7/7
 * @description: 脱敏注解
 */
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(
	using = SensitiveSerializer.class
)
public @interface Sensitive {

	String value();

}
