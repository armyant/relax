package com.idea.relax.tool.core.tree;

/**
 * @author azhao
 */
public enum ErgodicStrategy {
    /**
     * 根节点id等于根节点父id
     */
    ROOT_ID_EQ_ROOT_PID,
    /**
     * 根节点id为空
     */
    ROOT_PID_IS_NULL,
    /**
     * 根节点不为空切不等于根节点id
     */
    ROOT_PID_IS_NOT_NULL
}
