package com.idea.relax.log.listener;


import com.idea.relax.log.event.GeneralLogEvent;
import com.idea.relax.log.model.GeneralLogModel;
import com.idea.relax.log.publisher.ILogPublisher;
import com.idea.relax.log.service.IGeneralLogService;
import com.idea.relax.log.service.IRelaxLogService;
import com.idea.relax.log.thread.RelaxLoggingThreadPoolManager;
import com.idea.relax.log.api.callback.RelaxFailureCallbackHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;

/**
 * @className: GeneralLogListener
 * @description: 通用日志事件的监听
 * @author: salad
 * @date: 2022/10/7
 **/
@Slf4j
@AllArgsConstructor
public class GeneralLogListener {

    private final IGeneralLogService generalLogService;

    private final IRelaxLogService relaxLogService;

    private final RelaxLoggingThreadPoolManager threadPoolManager;

    private final RelaxFailureCallbackHandler failureCallbackHandler;

    @SuppressWarnings("unchecked")
    @EventListener(GeneralLogEvent.class)
    public void listener(GeneralLogEvent event) {
        ILogPublisher.Wrapper<GeneralLogModel> wrapper =
                (ILogPublisher.Wrapper<GeneralLogModel>) event.getSource();
        threadPoolManager.getThreadPool().execute(
                new GeneralLogWorker(wrapper, generalLogService,
                        relaxLogService, failureCallbackHandler)
        );


    }

}