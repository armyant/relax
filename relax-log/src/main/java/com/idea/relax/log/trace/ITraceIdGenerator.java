package com.idea.relax.log.trace;

/**
 * @className: ITraceIdGenerator
 * @description: TraceId生成器
 * @author: salad
 * @date: 2023/1/9
 **/
public interface ITraceIdGenerator {

    /**
     * 生成TraceId
     * @return TraceId
     */
    String generate();

}
