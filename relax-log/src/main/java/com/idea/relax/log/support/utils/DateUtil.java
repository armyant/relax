package com.idea.relax.log.support.utils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 * @Author: aron
 * @Date: 2022/06/29/21:14
 * @Description: 日期工具类
 */
public class DateUtil {
    public static final String PATTERN_DATETIME_MS = "yyyy-MM-dd HH:mm:ss:SSS";
    public static final String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";

    public static LocalDateTime getDateTime() {
        return LocalDateTime.now();
    }

    public static Date now() {
        return new Date();
    }

    public static String format(TemporalAccessor temporal, String dateFormat) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return formatter.format(temporal);
    }
}
