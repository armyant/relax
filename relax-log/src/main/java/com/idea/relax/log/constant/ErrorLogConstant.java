package com.idea.relax.log.constant;


/**
 * @author: 沉香
 * @date: 2022/12/14
 * @description: 错误日志相关的常量
 */
public final class ErrorLogConstant {

    /**
     * 用作发布错误日志事件时的数据传输
     */
    public static final String ERR_LOG_MODEL = "model";

    /**
     * 用作发布错误日志事件时的数据传输
     */
    public static final String ERROR = "error";

}
