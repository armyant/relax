package com.idea.relax.tool.core.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * @author azhao
 */
@Data
public class TreeParam<T> implements Serializable {
    private List<T> source;
    private Comparator<T> comparator;
    private ErgodicStrategy ergodicStrategy = ErgodicStrategy.ROOT_PID_IS_NULL;

    public TreeParam(List<T> source) {
        this.source = source;
    }
}
