package com.idea.relax.log.trace;


/**
 * @className: DefaultTraceIdGenerator
 * @description: 默认的TraceId生成器
 * @author: salad
 * @date: 2023/1/9
 **/
public class DefaultTraceIdGenerator implements ITraceIdGenerator {

    @Override
    public String generate() {
        return GlobalIdGenerator.generate();
    }

}
