package com.idea.relax.log.appender;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import org.springframework.core.Ordered;

import static ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME;

/**
 * @author L.cm
 * @author salad(参考自L.cm的Mica)
 * @date: 2022/12/17
 * @description: logback的appender接口，如果想要扩展实现自定义的appender，请继承该接口
 */
public interface ILogbackAppender extends Ordered {

	/**
	 * 启动appender
	 *
	 * @param context LoggerContext
	 */
	void start(LoggerContext context);

	/**
	 * 重置appender
	 *
	 * @param context LoggerContext
	 */
	void reset(LoggerContext context);

	/**
	 * 应用appender
	 *
	 * @param context LoggerContext
	 */
	void apply(LoggerContext context);

	/**
	 * 如果用户已经声明了同名的Appender，则不会设置当前Appender
	 * @param context LoggerContext
	 * @param appenderName appenderName
	 * @param newAppender appender
	 */
	default void decideApplyAppender(LoggerContext context, String appenderName, Appender<ILoggingEvent> newAppender){
		if (null != context.getLogger(ROOT_LOGGER_NAME).getAppender(appenderName)){
			return;
		}
		context.getLogger(ROOT_LOGGER_NAME).detachAppender(appenderName);
		context.getLogger(ROOT_LOGGER_NAME).addAppender(newAppender);
	}


}
