package com.idea.relax.sensitive.processor;

/**
 * @author: 沉香
 * @date: 2023/7/6
 * @description: 脱敏处理器，子类可以实现不同算法来进行脱敏
 */
public interface IStringSensitiveProcessor extends ISensitiveProcessor<String> {

	/**
	 * 对数据进行脱敏处理
	 *
	 * @param value
	 * @return
	 */
	String process(String value);

	/**
	 * 名称
	 *
	 * @return
	 */
	String name();


	/**
	 * 该处理器对什么类型生效
	 * @return
	 */
	@Override
	default Class<String> getTargetType() {
		return String.class;
	}

}
