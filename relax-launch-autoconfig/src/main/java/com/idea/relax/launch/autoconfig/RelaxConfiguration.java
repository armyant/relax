package com.idea.relax.launch.autoconfig;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @className: RelaxConfiguration
 * @description:
 * @author: salad
 * @date: 2022/10/9
 **/
@AllArgsConstructor
@Configuration
@EnableConfigurationProperties(RelaxProperties.class)
public class RelaxConfiguration {

}
