package com.idea.relax.log.service;


import com.idea.relax.log.model.ApiLogModel;
import com.idea.relax.log.model.ErrorLogModel;
import com.idea.relax.log.model.GeneralLogModel;

/**
 * @className: IRelaxLogService
 * @description: 持久化日志的通用接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface IRelaxLogService {


    /**
     * 持久化操作日志
     *
     * @param apiLogModel 操作日志Model对象
     */
    default void saveApiLog(ApiLogModel apiLogModel) {

    }

    /**
     * 持久化错误日志
     *
     * @param errorLogModel 错误日志Model对象
     */
    default void saveErrorLog(ErrorLogModel errorLogModel) {

    }

    /**
     * 持久化通用日志
     *
     * @param generalLogModel 通用日志Model对象
     */
    default void saveGeneralLog(GeneralLogModel generalLogModel) {

    }


}
