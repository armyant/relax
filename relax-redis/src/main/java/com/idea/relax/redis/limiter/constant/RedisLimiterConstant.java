package com.idea.relax.redis.limiter.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: 沉香
 * @date: 2023/2/15
 * @description:
 */
public class RedisLimiterConstant {

    /**
     * 失败的默认返回值
     */
    public static final long LIMITER_FAIL_CODE = 0;

    public static final int MAX_ISOLATION_MODE_EXPRESSION = 3;



    public static final List<String> ISOLATION_EXPRESSION = new ArrayList<>(MAX_ISOLATION_MODE_EXPRESSION);


    static {
		ISOLATION_EXPRESSION.add("${spring.application.name}");
		ISOLATION_EXPRESSION.add("${spring.profiles.active}");
    }

}
