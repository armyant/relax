package com.idea.relax.datascope;

import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * @className: DefaultDataScopeConditionRegister
 * @description:
 * @author: salad
 * @date: 2023/1/25
 **/
public class DefaultDataScopeConditionRegister implements IDataScopeConditionRegister{

    private final Map<DataScopeEnum,Conditionor> conditionMap = new HashMap<>(16);

    @Override
    public void register(DataScopeEnum dataScopeEnum, Conditionor conditionor) {
        conditionMap.put(dataScopeEnum,conditionor);
    }

    @Override
    public Conditionor get(DataScopeEnum dataScopeEnum) {
        Conditionor conditionor = conditionMap.get(dataScopeEnum);
        Assert.notNull(conditionor,"数据权限无效，不在支持范围内！");
        return conditionor;
    }
}
