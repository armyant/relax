package com.idea.relax.sensitive.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.Set;

/**
 * @author: 沉香
 * @date: 2023/7/10
 * @description:
 */
@Data
@ConfigurationProperties(SensitiveProperties.PREFIX)
public class SensitiveProperties {

	/**
	 * 前缀
	 */
	public static final String PREFIX = "relax.sensitive";

	/**
	 * 是否开启
	 */
	private boolean enabled = true;


}
