package com.idea.relax.log.support;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: ResponseStatus
 * @description: 接口响应状态枚举类
 * @author: salad
 * @date: 2023/1/2
 **/
@Getter
@AllArgsConstructor
public enum ResponseStatus {

    /**
     * 成功
     */
    SUCCESS("SUCCESS"),

    /**
     * 失败
     */
    ERROR("ERROR");

    /**
     * 状态
     */
    private String state;


}
