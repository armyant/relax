package com.idea.relax.log.thread;

import com.idea.relax.log.props.RelaxLogProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @author: 沉香
 * @date: 2022/12/27
 * @description: 线程池管理器
 */
@Slf4j
public class RelaxLoggingThreadPoolManager {

    private final ThreadPoolExecutor threadPoolExecutor;

    public RelaxLoggingThreadPoolManager(RelaxLogProperties.AsyncThread props) {
        this.threadPoolExecutor = new ThreadPoolExecutor(
                props.getCorePoolSize(), props.getMaximumPoolSize(),
                props.getKeepAliveTime(), TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(props.getQueueCapacity()),
                new DefaultThreadFactory(), new DefaultRejectedExecutionHandler()
        );
    }

    public ThreadPoolExecutor getThreadPool() {
        return threadPoolExecutor;
    }

}
