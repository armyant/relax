package com.idea.relax.exception.toolkit;

import com.idea.relax.exception.api.*;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Optional;
import java.util.function.Consumer;


/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 业务异常工具类
 */
public class BusinessExceptionHelper {

	/**
	 * 高等级业务异常工具类
	 */
	public static final BusinessExceptionEnum H = BusinessExceptionEnum.H;

	/**
	 * 中等级业务异常工具类
	 */
	public static final BusinessExceptionEnum M = BusinessExceptionEnum.M;

	/**
	 * 低等级业务异常工具类
	 */
	public static final BusinessExceptionEnum L = BusinessExceptionEnum.L;

	/**
	 * 创建业务异常
	 *
	 * @param errorLevel 异常等级
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 * @return BusinessException
	 */
	public static BaseException error(IErrorLevel errorLevel, IErrorCode errorCode, Object... args) {
		return new BusinessException(errorLevel, errorCode, args);
	}

	/**
	 * 创建高等级的业务异常
	 *
	 * @param errorCode 异常码
	 * @param args      异常message的参数
	 * @return BusinessException
	 */
	public static BaseException high(IErrorCode errorCode, Object... args) {
		return error(ErrorLevel.HIGH, errorCode, args);
	}

	/**
	 * 创建中等级的业务异常
	 *
	 * @param errorCode 异常码
	 * @param args      异常message的参数
	 * @return BusinessException
	 */
	public static BaseException middle(IErrorCode errorCode, Object... args) {
		return error(ErrorLevel.MIDDLE, errorCode, args);
	}

	/**
	 * 创建低等级的业务异常
	 *
	 * @param errorCode 异常码
	 * @param args      异常message的参数
	 * @return BusinessException
	 */
	public static BaseException low(IErrorCode errorCode, Object... args) {
		return error(ErrorLevel.LOW, errorCode, args);
	}

	/**
	 * 断言表达式为true的情况下创建业务异常
	 *
	 * @param expression 断言的表达式
	 * @param errorLevel 异常等级
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 */
	public static void isTrueThrow(boolean expression, IErrorLevel errorLevel, IErrorCode errorCode, Object... args) {
		ExceptionAssert.isTrueThrow(expression, () -> error(errorLevel, errorCode, args));
	}

	/**
	 * 断言表达式为false的情况下创建业务异常
	 *
	 * @param expression 断言的表达式
	 * @param errorLevel 异常等级
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 */
	public static void isFalseThrow(boolean expression, IErrorLevel errorLevel, IErrorCode errorCode, Object... args) {
		ExceptionAssert.isFalseThrow(expression, () -> error(errorLevel, errorCode, args));
	}

	/**
	 * 断言值为Empty的情况下创建业务异常
	 *
	 * @param value      断言的值
	 * @param errorLevel 异常等级
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 * @param <T>
	 */
	public static <T> void isEmptyThrow(T value, IErrorLevel errorLevel, IErrorCode errorCode, Object... args) {
		ExceptionAssert.isEmptyThrow(value, () -> error(errorLevel, errorCode, args));
	}

	/**
	 * 断言值不为Empty的情况下创建业务异常
	 *
	 * @param value      断言的值
	 * @param errorLevel 异常等级
	 * @param errorCode  异常码
	 * @param args       异常message的参数
	 * @param <T>
	 */
	public static <T> void isNotEmptyThrow(T value, IErrorLevel errorLevel, IErrorCode errorCode, Object... args) {
		ExceptionAssert.isNotEmptyThrow(value, () -> error(errorLevel, errorCode, args));
	}


}
