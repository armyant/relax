package com.idea.relax.crypto.core;

import com.idea.relax.crypto.config.ICryptoType;

import java.util.List;

/**
 * @author: 沉香
 * @date: 2023/5/11
 * @description:
 */
public interface ICryptographerRegister {

	/**
	 * 注册ICryptographer
	 * @param cryptographer
	 */
	void register(ICryptographer cryptographer);

	/**
	 * 批量注册ICryptographer
	 * @param cryptographers
	 */
	void register(List<ICryptographer> cryptographers);

	/**
	 * 获取ICryptographer，获取不到返回null
	 * @param cryptoType
	 * @return
	 */
	ICryptographer get(ICryptoType cryptoType);

	/**
	 * 获取ICryptographer，获取不到抛出<link>java.util.NoSuchElementException</link>异常
	 * @param cryptoType
	 * @return
	 */
	ICryptographer getIfAbsentThrow(ICryptoType cryptoType);

}
