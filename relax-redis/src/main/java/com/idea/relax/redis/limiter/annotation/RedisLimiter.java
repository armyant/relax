package com.idea.relax.redis.limiter.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author: 沉香
 * @date: 2023/2/14
 * @description:
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface RedisLimiter {

    /**
     * 限流的 key 支持，必须：请保持唯一性
     *
     * @return key
     */
    String value();

    /**
     * 限流的参数，可选，支持 spEL # 读取方法参数和 @ 读取 spring bean
     *
     * @return param
     */
    String param() default "";


    /**
     * 限流的周期，单位：秒
     * @return
     */
    long period() default 60L;


    /**
     * 限流最大访问次数
     * @return
     */
    long count() default 100L;


    /**
     * 时间单位，默认为秒
     *
     * @return TimeUnit
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;



}
