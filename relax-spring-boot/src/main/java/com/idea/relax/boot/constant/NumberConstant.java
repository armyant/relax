package com.idea.relax.boot.constant;

/**
 * @author: 沉香
 * @date: 2023/4/7
 * @description:
 */
public final class NumberConstant {


	private final Integer NUM_0 = 0;

	private final Integer NUM_1 = 1;

	private final Integer NUM_2 = 2;

	private final Integer NUM_3 = 3;

	private final Integer NUM_4 = 4;

	private final Integer NUM_5 = 5;

	private final Integer NUM_6 = 6;

	private final Integer NUM_7 = 7;

	private final Integer NUM_8 = 8;

	private final Integer NUM_9 = 9;

	private final Integer NUM_10 = 10;


}
