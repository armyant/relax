package com.idea.relax.redis.lock.config;

/**
 * @author salad
 */
public enum RedissonMode {
    SINGLE,
    MASTER_SLAVE,
    SENTINEL,
    CLUSTER;
}
