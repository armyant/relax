package com.idea.relax.log.annotation;

import com.idea.relax.log.support.OperationType;

import java.lang.annotation.*;

/**
 * @className: ApiLog
 * @description: 操作日志注解
 * @author: salad
 * @date: 2022/6/1
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiLog {

    /**
     * 模块名称
     */
    String value();

    /**
     * 操作类型
     */
    OperationType operationType() default OperationType.OTHER;

    /**
     * 是否保存响应数据
     */
    boolean isSaveRespData() default true;

    /**
     * 描述信息 支持spEL表达式
     * 支持模板表达式，比如：
     * 用户：#{#userVo.userId}将订单：#{#orderVo.orderId}的状态
     * 由#{#orderStatusVo.lastStatus}修改为#{#orderStatusVo.currentStatus}
     */
    String description() default "";


}
