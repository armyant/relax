package com.idea.relax.boot.file;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;


/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@Getter
@Setter
@ConfigurationProperties(RelaxFileProperties.PREFIX)
public class RelaxFileProperties {

	public static final String PREFIX = "relax.boot.file";

	/**
	 *
	 */
	private String parentPath = System.getProperty("user.dir");

	/**
	 *
	 */
	private String uploadPath = "upload";


}
