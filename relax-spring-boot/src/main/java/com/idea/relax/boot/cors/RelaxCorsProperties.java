package com.idea.relax.boot.cors;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@Getter
@Setter
@ConfigurationProperties(RelaxCorsProperties.PREFIX)
public class RelaxCorsProperties {

	public static final String PREFIX = "relax.boot.cors";

	/**
	 * 是否开启
	 */
	private boolean enabled = false;

	/**
	 *
	 */
	private String pathPattern = "/**";

	/**
	 *
	 */
	private String allowedOrigins = "*";

	/**
	 *
	 */
	private String allowedHeaders = "*";

	/**
	 *
	 */
	private String[] allowedMethods = {"GET", "POST", "PUT", "DELETE", "OPTIONS"};

	/**
	 *
	 */
	private long maxAge = 3600;



}
