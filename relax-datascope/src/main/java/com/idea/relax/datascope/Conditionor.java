package com.idea.relax.datascope;

import org.apache.ibatis.mapping.BoundSql;

/**
 * @className: Conditionor
 * @description: 数据权限的条件承载器，实现不同的sql拼接条件的地方
 * @author: salad
 * @date: 2022/11/13
 **/
@FunctionalInterface
public interface Conditionor {

    String condition(DataScopeEnum type, String column,BoundSql boundSql,String targetSql);

}
