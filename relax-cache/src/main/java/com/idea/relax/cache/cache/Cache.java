package com.idea.relax.cache.cache;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.springframework.lang.Nullable;

public interface Cache {


	@Nullable
	Object get(Object key);


	@Nullable
	<T> T get(Object key, Callable<T> valueLoader);

	<T> T get(Object key, @Nullable Class<T> type);


	void put(Object key, @Nullable Object value);

	void put(Object key, @Nullable Object value, long ttl, TimeUnit unit);


	@Nullable
	default Object putIfAbsent(Object key, @Nullable Object value) {
		Object existingValue = get(key);
		if (existingValue == null) {
			put(key, value);
		}
		return existingValue;
	}


	void evict(Object key);


	default boolean evictIfPresent(Object key) {
		evict(key);
		return false;
	}


	void clear();


	default boolean invalidate() {
		clear();
		return false;
	}


}
