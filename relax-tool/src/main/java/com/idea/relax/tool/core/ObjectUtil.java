package com.idea.relax.tool.core;

import org.springframework.lang.Nullable;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @className: ObjectUtil
 * @description:
 * @author: salad
 * @date: 2022/10/6
 **/
public class ObjectUtil extends ObjectUtils {


    public static boolean isNotEmpty(@Nullable Object obj) {
        return !isEmpty(obj);
    }



    public static Map<String, String> toMap(Object object) {
        Map<String, String> dataMap = new HashMap<>(16);
        Class<?> clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                dataMap.put(field.getName(), String.valueOf(field.get(object)));
            } catch (IllegalAccessException e) {
                throw ExceptionUtil.unchecked(e);
            }
        }
        return dataMap;
    }

}
