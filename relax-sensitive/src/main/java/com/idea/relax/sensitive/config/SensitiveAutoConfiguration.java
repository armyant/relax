package com.idea.relax.sensitive.config;

import com.idea.relax.sensitive.factory.ISensitiveFactory;
import com.idea.relax.sensitive.factory.SensitiveFactory;
import com.idea.relax.sensitive.processor.DefaultProcessorsSupplier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * @author: 沉香
 * @date: 2023/7/10
 * @description: 脱敏工具包的自动配置类
 */
@Order
@Configuration(
	proxyBeanMethods = false
)
@EnableConfigurationProperties(SensitiveProperties.class)
@ConditionalOnProperty(prefix = SensitiveProperties.PREFIX, name = "enabled", matchIfMissing = true, havingValue = "true")
public class SensitiveAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean
	public ISensitiveFactory sensitiveFactory() {
		SensitiveFactory sensitiveFactory = new SensitiveFactory();
		sensitiveFactory.add(DefaultProcessorsSupplier.PROCESSORS.get());
		return sensitiveFactory;
	}

}
