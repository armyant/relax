package com.idea.relax.tool.core.enums;

import java.util.Objects;

/**
 * @author: 沉香
 * @date: 2023/5/8
 * @description:
 */
public interface IEnumKeyValueBase<K, T> extends IEnumValueBase<T> {

	K getKey();

}
