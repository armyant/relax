package com.idea.relax.exception.api;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 异常等级基类
 */
public interface IErrorLevel {

	/**
	 * 获取异常等级
	 *
	 * @return 异常等级
	 */
	String getLevel();


}
