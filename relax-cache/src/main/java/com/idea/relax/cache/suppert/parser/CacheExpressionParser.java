package com.idea.relax.cache.suppert.parser;

import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * @className: CacheExpressionParser
 * @description:
 * @author: salad
 * @date: 2022/10/9
 **/
public interface CacheExpressionParser {

    ExpressionParser EXPRESSION_PARSER = new SpelExpressionParser();

    EvaluationContext getEvaluationContext(ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler);

    Object parseExpression(String expression, ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler);

    String parseExpressionForKey(String expression, ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler);

    Boolean parseExpressionForCondition(String expression, ProceedingJoinPoint joinPoint, IFunctionHandler<IFunction> handler);

}
