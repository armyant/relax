package com.idea.relax.log.trace;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: ReqSource
 * @description: 请求的来源
 * @author: salad
 * @date: 2023/1/27
 **/
@Getter
@AllArgsConstructor
public enum ReqSource {


    /**
     * http
     */
    HTTP("Http"),

    /**
     * feign
     */
    FEIGN("Feign");


    private final String name;


}
