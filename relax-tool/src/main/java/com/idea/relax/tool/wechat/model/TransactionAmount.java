package com.idea.relax.tool.wechat.model;

import lombok.Setter;

/**
 * TransactionAmount
 */
@Setter
public class TransactionAmount {
    /**
     * currency
     */
    private String currency;
    /**
     * payerCurrency
     */
    private String payer_currency;
    /**
     * payerTotal
     */
    private Integer payer_total;
    /**
     * total
     */
    private Integer total;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPayerCurrency() {
        return payer_currency;
    }

    public void setPayerCurrency(String payerCurrency) {
        this.payer_currency = payerCurrency;
    }

    public Integer getPayerTotal() {
        return payer_total;
    }

    public void setPayerTotal(Integer payerTotal) {
        this.payer_total = payerTotal;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
