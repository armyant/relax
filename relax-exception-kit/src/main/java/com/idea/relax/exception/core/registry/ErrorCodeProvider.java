package com.idea.relax.exception.core.registry;

import com.idea.relax.exception.api.IErrorCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @className: ErrorCodeProvider
 * @description: 异常码提供者
 * @author: salad
 * @date: 2023/5/25
 **/
public class ErrorCodeProvider {

	private final List<IErrorCode> ERROR_CODES_POOL = new ArrayList<>();

	/**
	 * 添加异常码
	 *
	 * @param errorCodes 异常码枚举类
	 */
	public void add(IErrorCode... errorCodes) {
		if (errorCodes != null && errorCodes.length > 0) {
			ERROR_CODES_POOL.addAll(Arrays.asList(errorCodes));
		}
	}

	/**
	 * 获取异常码
	 *
	 * @return 异常码枚举类集合
	 */
	protected List<IErrorCode> getErrorCodes() {
		return ERROR_CODES_POOL;
	}

	/**
	 * 清空异常码
	 */
	protected void clearErrorCodes() {
		if (!ERROR_CODES_POOL.isEmpty()) {
			ERROR_CODES_POOL.clear();
		}
	}

}
