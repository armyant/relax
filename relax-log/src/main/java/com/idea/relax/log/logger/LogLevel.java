package com.idea.relax.log.logger;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: LogLevel
 * @description: 通用日志的级别
 * @author: salad
 * @date: 2022/10/7
 **/
@Getter
@AllArgsConstructor
public enum LogLevel {


    /**
     * debug级别
     */
    DEBUG("debug"),

    /**
     * info级别
     */
    INFO("info"),

    /**
     * warning级别
     */
    WARN("warn"),

    /**
     * error级别
     */
    ERROR("error");


    private final String level;


}
