package com.idea.relax.exception.core.registry;

import org.springframework.beans.factory.SmartInitializingSingleton;

/**
 * @author: 沉香
 * @date: 2023/5/25
 * @description: 能自动注册异常码注册器抽象类
 */
public abstract class AbstractAutoErrorCodeRegister implements IErrorCodeRegister, SmartInitializingSingleton {


	@Override
	public void afterSingletonsInstantiated() {
		registry();
	}

}
