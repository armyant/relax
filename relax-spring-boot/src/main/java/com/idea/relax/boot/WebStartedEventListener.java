package com.idea.relax.boot;

import com.idea.relax.tool.core.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;

/**
 * @author salad
 */
@Slf4j
@AutoConfiguration
public class WebStartedEventListener {

	@Async
	@Order
	@EventListener(WebServerInitializedEvent.class)
	public void afterStart(WebServerInitializedEvent event) {
		Environment environment = event.getApplicationContext().getEnvironment();
		String appName = environment.getProperty("spring.application.name","").toUpperCase();
		int localPort = event.getWebServer().getPort();
		String profile = StringUtil.join(environment.getActiveProfiles());
		log.info("---应用启动完成,当前应用名称:[{}],环境变量:[{}],使用的端口:[{}]---", appName, profile, localPort);
	}
}
