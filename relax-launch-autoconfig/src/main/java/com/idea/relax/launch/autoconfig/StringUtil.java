package com.idea.relax.launch.autoconfig;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringJoiner;

/**
 * @className: StringUtils
 * @description:
 * @author: salad
 * @date: 2022/2/25
 **/
class StringUtil extends org.springframework.util.StringUtils {

    public static boolean isBlank(final CharSequence cs) {
        return !hasText(cs);
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return hasText(cs);
    }

    public static String join(Collection<?> coll, String delimiter,
                              String prefix, String suffix,
                              String emptyValue, boolean skipEmpty) {
        if (coll == null || coll.isEmpty()) {
            return emptyValue;
        }

        Iterator<?> iterator = coll.iterator();
        StringJoiner joiner = new StringJoiner(delimiter, prefix, suffix);

        if (null != emptyValue){
            joiner.setEmptyValue(emptyValue);
        }

        while (iterator.hasNext()) {
            Object next = iterator.next();
            if (skipEmpty) {
                if (null != next && isNotBlank(String.valueOf(next))) {
                    joiner.add(String.valueOf(next));
                }
            } else {
                joiner.add(String.valueOf(next));
            }
        }
        return joiner.toString();
    }

    public static String join(Collection<?> coll, String delimiter, boolean isSkipEmpty) {
        return join(coll, delimiter, "", "", "", isSkipEmpty);
    }

    public static String join(Collection<?> coll, String delimiter) {
        return join(coll, delimiter, false);
    }

    public static String join(Collection<?> coll) {
        return join(coll, ",");
    }

    public static <T> String join(T[] array) {
        return join(Arrays.asList(array), ",");
    }

    public static String join(CharSequence ...array) {
        return join(Arrays.asList(array), ",");
    }

    public static String join(String delimiter,CharSequence ...array) {
        return join(Arrays.asList(array), delimiter);
    }


}
