package com.idea.relax.boot.autoconfig;

import com.idea.relax.launch.autoconfig.LaunchAutoConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Properties;

/**
 * @className: LogLaunchAutoConfigurer
 * @description:
 * @author: salad
 * @date: 2023/4/12
 **/
public class LogLaunchAutoConfigurer implements LaunchAutoConfigurer {


	@Override
	public void autoconfig(ConfigurableEnvironment environment, SpringApplication application) {
		Properties props = System.getProperties();
		props.setProperty("logging.config", "classpath:logback/logback-spring.xml");
	}

	@Override
	public int getOrder() {
		return 0;
	}
}
