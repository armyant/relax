package com.idea.relax.log.constant;

import com.idea.relax.log.props.LogLevel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @className: LogConstant
 * @description: 日志公共常量
 * @author: salad
 * @date: 2022/10/1
 **/
public final class LogConstant {


    /**
     * file日志appender默认日志等级
     */
    public static final Set<LogLevel> FILE_LOG_LEVELS = new HashSet<>();

    /**
     * kafka日志appender默认日志等级
     */
    public static final Set<LogLevel> KAFKA_LOG_LEVELS = new HashSet<>();

    /**
     * logstash日志appender默认日志等级
     */
    public static final Set<LogLevel> LOGSTASH_LOG_LEVELS = new HashSet<>();

    static {

        FILE_LOG_LEVELS.add(LogLevel.INFO);
        FILE_LOG_LEVELS.add(LogLevel.ERROR);

        KAFKA_LOG_LEVELS.add(LogLevel.INFO);
        KAFKA_LOG_LEVELS.add(LogLevel.ERROR);

        LOGSTASH_LOG_LEVELS.add(LogLevel.INFO);
        LOGSTASH_LOG_LEVELS.add(LogLevel.ERROR);

    }

    /**
     * 排除的包路径，用作stackHash的计算
     */
    public static final List<String> EXCLUSION_PATTERNS = Arrays.asList("^net\\.sf\\.cglib\\.proxy\\.MethodProxy\\.invoke",
            "^org\\.apache\\.*", "^sun\\.reflect\\.*", "^sun\\.reflect\\.*", "^java\\.lang\\.reflect\\.*");

    /**
     * 请求相关的属性字段，用作MDC的key
     */
    public static final List<String> REQ_MDC_FIELDS = Arrays.asList(
            "methodClass", "methodName", "requestMethod",
            "requestUrl", "requestParams", "requestBody",
            "userAgent", "visitorIp", "requestTime"
    );

    /**
     * 响应相关的属性字段，用作MDC的key
     */
    public static final List<String> RESP_MDC_FIELDS = Arrays.asList(
            "responseStatus", "responseData", "time"
    );

    /**
     * 异常相关的属性字段，用作MDC的key
     */
    public static final List<String> ERR_MDC_FIELDS = Arrays.asList(
            "errorName", "errorMessage", "errorFileName", "errorLineNumber"
    );

    /**
     * 服务相关属性字段，用作MDC的key
     */
    public static final List<String> SERVICE_MDC_FIELDS = Arrays.asList(
             "serverName", "appName","serverIp","appPort","env"
    );

    /**
     * 请求开始字样的日志打印
     */
    public static final String REQUEST_START_LOG = "================== Request start ==================\n";

    /**
     * 请求结束字样的日志打印
     */
    public static final String REQUEST_END_LOG = "================== Request end ====================\n";

    /**
     * 响应开始字样的日志打印
     */
    public static final String RESPONSE_START_LOG = "================== Response start =================\n";

    /**
     * 响应结束字样的日志打印
     */
    public static final String RESPONSE_END_LOG = "=================  Response end  ==================\n";

}
