package com.idea.relax.datascope.aron;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className: RelaxDataScopeProperties
 * @description:
 * @author: salad
 * @date: 2022/11/8
 **/
@Data
@ConfigurationProperties(prefix = RelaxDataScopeProperties.PREFIX)
public class RelaxDataScopeProperties {

	public static final String PREFIX = "relax.datascope";

    private boolean enabled = true;


}
