package com.idea.relax.mybatis.sql;

import lombok.Builder;
import lombok.Data;

/**
 * @author: 沉香
 * @date: 2023/5/19
 * @description:
 */
@Data
@Builder
public class ErrorCauseData {

	/**
	 *
	 */
	public static final ErrorCauseData EMPTY = ErrorCauseData.builder().build();

	/**
	 *
	 */
	private String className;

	/**
	 *
	 */
	private String fileName;

	/**
	 *
	 */
	private int lineNumber;

	/**
	 *
	 */
	private String methodName;


}
