package com.idea.relax.redis.limiter.reject;

import java.util.concurrent.TimeUnit;

/**
 * @author: 沉香
 * @date: 2023/2/15
 * @description:
 */
public class LimiterThrowErrRejectStrategy implements RedisLimiterRejectStrategy {

	private static final String ERROR_MESSAGE = "对资源：{}的访问已经超过限制，当前速率：{}";

	@Override
	public Object reject(String key, long count, long period, TimeUnit timeUnit) {
		throw new RedisLimiterException(ERROR_MESSAGE, key, count, period, timeUnit);
	}

}
