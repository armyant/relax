package com.idea.relax.tool.core.tree;

import java.lang.annotation.*;

/**
 * @author azhao
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TreeNode {
}
