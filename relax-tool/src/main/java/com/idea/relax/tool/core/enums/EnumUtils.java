package com.idea.relax.tool.core.enums;


import java.util.Objects;

/**
 * @className: EnumUtils
 * @description: 枚举工具类
 * @author: salad
 * @date: 2023/5/10
 **/
public final class EnumUtils {


	/**
	 * 判断指定的value是否在枚举中存在
	 *
	 * @param enums
	 * @param value
	 * @param <T>
	 * @return
	 */
	public static <T> boolean include(IEnumValueBase<T>[] enums, T value) {
		if (value == null) {
			return false;
		}
		for (IEnumValueBase<T> e : enums) {
			if (value.equals(e.getValue())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断指定的value是否在枚举中存在
	 *
	 * @param enumClass
	 * @param value
	 * @param <E>
	 * @param <V>
	 * @return
	 */
	public static <E extends Enum<? extends IEnumValueBase<V>>, V> boolean include(Class<E> enumClass, V value) {
		for (Enum<? extends IEnumValueBase<V>> e : enumClass.getEnumConstants()) {
			if (((IEnumValueBase<V>) e).getValue().equals(value)) {
				return true;
			}
		}
		return false;
	}

	public static <K, T> boolean include(IEnumKeyValueBase<K, T>[] enums, T value) {
		if (value == null) {
			return false;
		}
		for (IEnumKeyValueBase<K, T> e : enums) {
			if (equal(e, value)) {
				return true;
			}
		}
		return false;
	}

	public static <T> boolean equal(IEnumValueBase<T> enumValue, T value) {
		return Objects.equals(enumValue.getValue(), value);
	}

	public static <K, T> boolean keyEqual(IEnumKeyValueBase<K, T> enums, K key) {
		return Objects.equals(enums.getKey(), key);
	}


	public static <T> IEnumValueBase<T> ofValueEnum(IEnumValueBase<T>[] enums, T value) {
		if (value == null) {
			return null;
		}
		for (IEnumValueBase<T> e : enums) {
			if (equal(e, value)) {
				return e;
			}
		}
		return null;
	}

	public static <E extends Enum<? extends IEnumValueBase<T>>, T> IEnumValueBase<T> ofValueEnum(Class<E> enumClass, T value) {
		for (Enum<? extends IEnumValueBase<T>> e : enumClass.getEnumConstants()) {
			if (equal(((IEnumValueBase<T>) e), value)) {
				return ((IEnumValueBase<T>) e);
			}
		}
		return null;
	}

	public static <K, T> K getKey(IEnumKeyValueBase<K, T>[] enums, T value) {
		if (value == null) {
			return null;
		}
		for (IEnumKeyValueBase<K, T> e : enums) {
			if (value.equals(e.getValue())) {
				return e.getKey();
			}
		}
		return null;
	}


	public static <K, T> T getValue(IEnumKeyValueBase<K, T>[] enums, K key) {

		for (IEnumKeyValueBase<K, T> e : enums) {
			if (key.equals(e.getKey())) {
				return e.getValue();
			}
		}
		return null;
	}


	public static <K, T> IEnumKeyValueBase<K, T> ofKeyValueEnum(IEnumKeyValueBase<K, T>[] enums, T value) {
		if (value == null) {
			return null;
		}
		for (IEnumKeyValueBase<K, T> e : enums) {
			if (equal(e, value)) {
				return e;
			}
		}
		return null;
	}

	public static <E extends Enum<? extends IEnumKeyValueBase<K, T>>, K, T> IEnumKeyValueBase<K, T> ofKeyValueEnum(Class<E> enumClass, T value) {
		for (Enum<? extends IEnumKeyValueBase<K, T>> e : enumClass.getEnumConstants()) {
			if (equal(((IEnumKeyValueBase<K, T>) e), value)) {
				return ((IEnumKeyValueBase<K, T>) e);
			}
		}
		return null;
	}



}
