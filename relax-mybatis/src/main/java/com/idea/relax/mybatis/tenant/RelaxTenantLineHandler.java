package com.idea.relax.mybatis.tenant;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.idea.relax.mybatis.config.RelaxMybatisProperties;
import lombok.AllArgsConstructor;
import net.sf.jsqlparser.expression.Expression;

import java.util.Arrays;

/**
 * @className: RelaxTenantLineHandler
 * @description:
 * @author: salad
 * @date: 2023/4/30
 **/
@AllArgsConstructor
public class RelaxTenantLineHandler implements TenantLineHandler {

	private final RelaxMybatisProperties.Tenant properties;

	private final ITenantIdHandler tenantIdHandler;

	@Override
	public Expression getTenantId() {
		return tenantIdHandler.getTenantId();
	}

	@Override
	public String getTenantIdColumn() {
		return properties.getTenantIdColumn();
	}

	@Override
	public boolean ignoreTable(String tableName) {
		return Arrays.asList(properties.getIgnoreTables()).contains(tableName);
	}

}
