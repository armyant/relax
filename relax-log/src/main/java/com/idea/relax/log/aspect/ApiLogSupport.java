package com.idea.relax.log.aspect;

import com.idea.relax.log.annotation.ApiLog;
import com.idea.relax.log.props.RelaxLogProperties;
import com.idea.relax.log.publisher.ApiLogPublisher;
import com.idea.relax.log.support.MethodExecuteResult;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * @author: 沉香
 * @date: 2022/12/17
 * @description: 操作日志切面的支撑类
 */
@Slf4j
public class ApiLogSupport {


    public void handleAfterMethod(RelaxLogProperties.ApiLog props, MethodInvocation invocation,
                                  MethodExecuteResult methodExecuteResult) {
        Method method = invocation.getMethod();
        ApiLog apiLog = method.getAnnotation(ApiLog.class);
        //保存操作日志
        ApiLogPublisher.build(props.getMaxRespDataLength(),apiLog,invocation,methodExecuteResult).publish();
    }

}
