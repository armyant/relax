package com.idea.relax.sensitive.processor.impl;

import com.idea.relax.sensitive.processor.ISensitiveProcessor;

/**
 * @author: 沉香
 * @date: 2023/7/10
 * @description:
 */
public class CoverSensitiveProcessor implements ISensitiveProcessor<String> {

	public static final ISensitiveProcessor INSTANCE = new CoverSensitiveProcessor();

	@Override
	public String process(String value) {
		return "xxxx";
	}

	@Override
	public String name() {
		return "cover";
	}

	@Override
	public Class<String> getTargetType() {
		return String.class;
	}
}
