package com.idea.relax.boot.security.sql;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@Getter
@Setter
@ConfigurationProperties(RelaxSqlInjectProperties.PREFIX)
public class RelaxSqlInjectProperties {

	public static final String PREFIX = "relax.boot.sql-inject";

	private boolean enabled = true;

	private String[] urlPatterns = {"/*"};

	private ProcessStrategy processStrategy = ProcessStrategy.THROW;

	private List<String> skipUrls = new ArrayList<>();


}
