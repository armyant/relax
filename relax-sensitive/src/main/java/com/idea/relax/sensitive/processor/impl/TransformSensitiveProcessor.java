package com.idea.relax.sensitive.processor.impl;

import com.idea.relax.sensitive.processor.ISensitiveProcessor;

/**
 * @author: 沉香
 * @date: 2023/7/10
 * @description:
 */
public class TransformSensitiveProcessor implements ISensitiveProcessor<String> {

	public static final ISensitiveProcessor INSTANCE = new TransformSensitiveProcessor();

	@Override
	public String process(String value) {
		return "aaaa";
	}

	@Override
	public String name() {
		return "transform";
	}

	@Override
	public Class<String> getTargetType() {
		return String.class;
	}
}
