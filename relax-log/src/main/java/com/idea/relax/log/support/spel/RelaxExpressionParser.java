package com.idea.relax.log.support.spel;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.expression.EvaluationContext;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;

/**
 * @className: RelaxExpressionParser
 * @description: SPel表达式解析器接口
 * @author: salad
 * @date: 2022/10/9
 **/
public interface RelaxExpressionParser {


    /**
     * Create an {@link EvaluationContext}.
     * @return the evaluation context
     */
    EvaluationContext createContext(EvaluationContextRootObject rootObject, boolean isBeanFactoryResolver);

    /**
     * Create an {@link EvaluationContext}.
     *
     * @param method      the method
     * @param args        the method arguments
     * @param rootObject  rootObject
     * @param targetClass the target class
     * @return the evaluation context
     */
    EvaluationContext createContext(Method method, Object[] args, Class<?> targetClass, Object rootObject, @Nullable BeanFactory beanFactory);

    @Nullable
    Object eval(String expression, AnnotatedElementKey methodKey, EvaluationContext evalContext);

    @Nullable
    <T> T eval(String expression, AnnotatedElementKey methodKey, EvaluationContext evalContext, @Nullable Class<T> valueType);

    @Nullable
    String evalAsText(String expression, AnnotatedElementKey methodKey, EvaluationContext evalContext);

    boolean evalAsBool(String expression, AnnotatedElementKey methodKey, EvaluationContext evalContext);

    /**
     * Clear all caches.
     */
    void clear();

}
