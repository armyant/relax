package com.idea.relax.cache.annotation;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @className: CachePut
 * @description:
 * @author: salad
 * @date: 2022/10/16
 **/
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CachePut {

    @AliasFor("cacheNames")
    String value();

    @AliasFor("value")
    String cacheNames();

    String key() default "";

    String condition() default "";

}
