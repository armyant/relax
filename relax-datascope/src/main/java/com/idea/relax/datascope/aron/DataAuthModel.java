package com.idea.relax.datascope.aron;

import com.idea.relax.datascope.aron.Condition;
import lombok.Data;

/**
 * @className: DataAuthModel
 * @description: TODO 类描述
 * @author: salad
 * @date: 2023/5/3
 **/
@Data
public class DataAuthModel {

	private String code;

	private String[] value;

	private Condition[] condition;


}
