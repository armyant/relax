package com.idea.relax.tool.core.enums;

import lombok.AllArgsConstructor;

/**
 * @author: 沉香
 * @date: 2023/5/10
 * @description:
 */
@AllArgsConstructor
public enum UserType implements IEnumValueBase<String>{
	WEB("web"),
	APP("app");

	private final String value;

	@Override
	public String getValue() {
		return value;
	}
}
