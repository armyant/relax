package com.idea.relax.datascope;

import lombok.AllArgsConstructor;
import org.apache.ibatis.mapping.BoundSql;

/**
 * @className: RelaxDataScopeConditionProcessor
 * @description:
 * @author: salad
 * @date: 2022/11/13
 **/
@AllArgsConstructor
public class RelaxDataScopeConditionProcessor implements DataScopeConditionProcessor{

    private final IDataScopeConditionRegister dataScopeConditionRegister;


    @Override
    public String getCondition(DataScopeEnum type, String column, BoundSql boundSql, String targetSql){
        return dataScopeConditionRegister.get(type)
                .condition(type, column, boundSql, targetSql);
    }

}
