package com.idea.relax.exception.api;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 系统异常类
 */
public class SystemException extends BaseException {

	public SystemException(final IErrorLevel level, final IErrorCode code, final Object... args) {
		super(level, ErrorType.SYSTEM, code, args);
	}

}
