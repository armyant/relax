package com.idea.relax.log.constant;

import com.idea.relax.log.props.RelaxLogProperties;

/**
 * @className: PropsConstant
 * @description: 参数相关的常量
 * @author: salad
 * @date: 2022/10/1
 **/
public final class PropsConstant {


    /**
     * 用户代理的请求头参数名
     */
    public static final String USER_AGENT_HEADER = "user-agent";

    /**
     * traceId
     */
    public static final String TRACE_ID = "traceId";

    public static final String TRACE_ID_HEADER = "trace-id";

    public static final String REQ_SOURCE = "reqSource";

    public static final String REQ_SOURCE_HEADER = "req-source";

    public static final String ENTRANCE_URI = "entranceUri";

    public static final String ENTRANCE_URI_HEADER = "entrance-uri";

    /**
     * 通用日志反推出方法所在类名与方法名的默认堆栈数组下标
     */
    public static final int STACK_TRACE_ELEMENT_INDEX = 4;

    /**
     * 应用名称
     */
    public static final String APP_NAME_KEY = "spring.application.name";

    /**
     * 用来记录主启动类所在的包路径的属性
     */
    public static final String APP_PACKAGE_PREFIX_KEY = RelaxLogProperties.ErrorLog.PREFIX + ".app-package-prefix";


}
