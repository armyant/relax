package com.idea.relax.cache.cache.impl.redis;

import com.idea.relax.cache.cache.Cache;
import com.idea.relax.redis.toolkit.RedisOps;
import lombok.AllArgsConstructor;
import org.springframework.lang.Nullable;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @className: RedisCache
 * @description:
 * @author: salad
 * @date: 2022/10/6
 **/
@AllArgsConstructor
public class RedisCache implements Cache {

    private final RedisOps redisOps;

    @Override
    public Object get(Object key) {
        return redisOps.get(String.valueOf(key));
    }

    @Override
    @SuppressWarnings("unchecked")
    @Nullable
    public <T> T get(Object key, @Nullable Class<T> type) {
        Object value = get(key);
        if (value != null && type != null && !type.isInstance(value)) {
            throw new IllegalStateException(
                    "Cached value is not of required type [" + type.getName() + "]: " + value);
        }
        return (T) value;
    }

    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        return null;
    }

    @Override
    public void put(Object key, Object value) {
        redisOps.set(String.valueOf(key),value);
    }

    @Override
    public void put(Object key, Object value, long ttl, TimeUnit unit) {
        redisOps.setEx(String.valueOf(key),value,ttl,unit);
    }

    @Override
    public void evict(Object key) {
        redisOps.del(String.valueOf(key));
    }

    @Override
    public void clear() {
        Set<String> keys = Optional.ofNullable(redisOps.keys("*")).orElse(Collections.emptySet());
        redisOps.del(keys);
    }

}
