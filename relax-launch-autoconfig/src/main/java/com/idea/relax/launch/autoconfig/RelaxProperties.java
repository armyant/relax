package com.idea.relax.launch.autoconfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @className: RelaxProperties
 * @description:
 * @author: salad
 * @date: 2022/10/23
 **/
@Data
@ConfigurationProperties(prefix = RelaxProperties.PREFIX)
public class RelaxProperties {

	public final static String PREFIX = "relax.metadata";

	private final Map<String, String> props = new HashMap<>(16);

	public String getPropAsString(String key, String defaultValue) {
		String value = props.get(key);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

	public String getPropAsString(String key) {
		return getPropAsString(key, null);
	}

	public Boolean getPropAsBoolean(String key) {
		String prop = getPropAsString(key);
		return Boolean.valueOf(prop);
	}

	public Integer getPropAsInteger(String key) {
		String prop = getPropAsString(key);
		return Integer.valueOf(prop);
	}

	public Long getPropAsLong(String key) {
		String prop = getPropAsString(key);
		return Long.valueOf(prop);
	}

}
