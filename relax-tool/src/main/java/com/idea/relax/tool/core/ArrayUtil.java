package com.idea.relax.tool.core;

/**
 * @className: ArrayUtil
 * @description:
 * @author: salad
 * @date: 2023/5/2
 **/
public class ArrayUtil {


	/**
	 *
	 * @param target
	 * @param index
	 * @param <T>
	 * @return
	 */
	public static <T> T get(T[] target, int index) {
		return get(target, index, null);
	}

	/**
	 *
	 * @param target
	 * @param index
	 * @param <T>
	 * @return
	 */
	public static <T> T get(T[] target, int index, T defaultValue) {
		T t;
		try {
			t = target[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			return defaultValue;
		}
		return t;
	}

}
