package com.idea.relax.exception.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 内置的异常等级枚举类
 */
@AllArgsConstructor
public enum ErrorLevel implements IErrorLevel {

	/**
	 * 低等级异常
	 */
	LOW("L"),

	/**
	 * 中等级异常
	 */
	MIDDLE("M"),

	/**
	 * 高等级异常
	 */
	HIGH("H");


	/**
	 * 异常等级
	 */
	private final String level;

	@Override
	public String getLevel() {
		return level;
	}
}
