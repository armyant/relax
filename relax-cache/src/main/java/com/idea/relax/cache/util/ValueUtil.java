package com.idea.relax.cache.util;

import org.springframework.util.ObjectUtils;

import java.util.Base64;

/**
 * @className: ValueUtil
 * @description:
 * @author: salad
 * @date: 2022/10/10
 **/
public class ValueUtil extends ObjectUtils {

    public static boolean isNotEmpty(Object value){
        return !isEmpty(value);
    }


}
