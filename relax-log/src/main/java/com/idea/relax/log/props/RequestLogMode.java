package com.idea.relax.log.props;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

/**
 * @className: RequestLogMode
 * @description: 请求日志的模式
 * @author: salad
 * @date: 2022/6/9
 **/
@Getter
@AllArgsConstructor
public enum RequestLogMode {

    /**
     * 输出请求头
     */
    HEADERS(0),

    /**
     * 输出请求参数
     */
    REQ_PARAMS(1),

    /**
     * 输出请求体
     */
    REQ_BODY(2),

    /**
     * 输出Uri
     */
    URI(3),

    /**
     * 输出响应数据
     */
    RESP_DATA(4),

    /**
     * 输出TraceId
     */
    TRACE_ID(5);


    private final int modeId;

    public boolean eq(RequestLogMode mode) {
        return this.getModeId() == mode.getModeId();
    }

    public boolean gt(RequestLogMode mode) {
        return this.getModeId() > mode.getModeId();
    }

    public boolean lt(RequestLogMode mode) {
        return this.getModeId() < mode.getModeId();
    }

    public boolean containsOf(Set<RequestLogMode> modes) {
        if (null == modes || modes.isEmpty()){
            return false;
        }
        for (RequestLogMode mode : modes) {
            if (mode.getModeId() == this.getModeId()){
                return true;
            }
        }
        return false;
    }


}
