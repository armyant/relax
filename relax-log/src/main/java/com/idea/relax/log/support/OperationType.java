package com.idea.relax.log.support;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: OperationType
 * @description: 操作类型
 * @author: salad
 * @date: 2022/10/2
 **/
@Getter
@AllArgsConstructor
public enum OperationType {


    /**
     * 登录操作
     */
    LOGIN("登录操作"),

    /**
     * 登出操作
     */
    LOGOUT("登出操作"),

    /**
     * 查询操作
     */
    SELECT("查询操作"),

    /**
     * 新增操作
     */
    INSERT("新增操作"),

    /**
     * 修改操作
     */
    UPDATE("修改操作"),

    /**
     * 删除操作
     */
    DELETE("删除操作"),

    /**
     * 导入数据
     */
    EXPORT("导入数据"),

    /**
     * 导入数据
     */
    IMPORT("导入数据"),

    /**
     * 上传文件
     */
    UPLOAD("上传文件"),

    /**
     * 下载文件
     */
    DOWNLOAD("下载文件"),

    /**
     * 计算数据
     */
    CALC_DATA("计算数据"),

    /**
     * 其它操作
     */
    OTHER("其它操作");

    /**
     * 操作名称
     */
    private final String name;

}
