package com.idea.relax.log.support.utils;

import org.slf4j.MDC;

import java.util.List;
import java.util.Objects;

/**
 * @author: 沉香
 * @date: 2022/12/13
 * @description:
 */
@SuppressWarnings("all")
public class MDCUtil {

    public static void puts(List<String> keys,Object ...values){
        if (keys.size() != values.length){
            throw new IllegalArgumentException("The length of 'keys' must be the same as the length of 'values'!");
        }
        for (int i = 0; i < keys.size(); i++){
            if (!Objects.isNull(values[i])){
                MDC.put(keys.get(i),String.valueOf(values[i]));
            }
        }
    }

    public static void putsFromStrList(List<String> keys,List<String> values){
        if (keys.size() != values.size()){
            throw new IllegalArgumentException("The length of 'keys' must be the same as the length of 'values'!");
        }
        for (int i = 0; i < keys.size(); i++){
            if (!Objects.isNull(values.get(i))){
                MDC.put(keys.get(i),values.get(i));
            }
        }
    }

    public static void removes(List<String> keys){
        if (null == keys || keys.size() == 0){
            throw new IllegalArgumentException("'keys' is invalid!");
        }
        for (int i = 0; i < keys.size(); i++){
            String key = keys.get(i);
            if (null != key
                    && key.trim().length() > 0){
                MDC.remove(key);
            }

        }
    }

}
