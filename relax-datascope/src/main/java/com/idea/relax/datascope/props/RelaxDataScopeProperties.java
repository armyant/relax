package com.idea.relax.datascope.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className: RelaxDataScopeProperties
 * @description:
 * @author: salad
 * @date: 2022/11/8
 **/
@Data
@ConfigurationProperties(prefix = "relax.datascope")
public class RelaxDataScopeProperties {

    boolean enabled = true;

    private String column = "dept";


}
