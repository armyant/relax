package com.idea.relax.datascope.aron;

import org.apache.ibatis.mapping.BoundSql;

/**
 * @className: IDataScopeHandler
 * @description:
 * @author: salad
 * @date: 2022/11/13
 **/
public interface IDataScopeHandler {

    String conditionSql(DataAuthModel model, BoundSql boundSql, String targetSql);

}
