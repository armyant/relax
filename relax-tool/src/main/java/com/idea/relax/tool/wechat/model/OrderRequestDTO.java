package com.idea.relax.tool.wechat.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: 沉香
 * @date: 2023/3/7
 * @description:
 */
@Data
public class OrderRequestDTO {

    /**
     * 商品描述
     */
    private String description;

    /**
     * 商户订单号,商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一
     */
    private String outTradeNo;

    /**
     * 支付金额，单位：分
     */
    private Integer amount;

    public OrderRequestDTO(String description, String outTradeNo, BigDecimal amount) {
        this.description = description;
        this.outTradeNo = outTradeNo;
        this.amount = amount.multiply(new BigDecimal("100")).intValue();
    }

}
