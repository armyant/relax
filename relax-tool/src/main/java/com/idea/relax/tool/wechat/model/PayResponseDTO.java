package com.idea.relax.tool.wechat.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 沉香
 * @date: 2023/3/7
 * @description:
 */
@Data
@ApiModel(value = "PayResponseDTO", description = "下单支付响应信息")
public class PayResponseDTO {

    /**
     * appId
     */
    @ApiModelProperty("appId")
    private String appId;

    /**
     * 时间戳
     */
    @ApiModelProperty("时间戳")
    private String timestamp;

    /**
     * 随机字符串
     */
    @ApiModelProperty("随机字符串")
    private String nonceStr;

    /**
     * 订单详情扩展字符串，小程序下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
     */
    @ApiModelProperty("订单详情扩展字符串，小程序下单接口返回的prepay_id参数值，提交格式如：prepay_id=***")
    private String packageVal;

    /**
     * 若未设置signType，默认值为 WECHATPAY2-SHA256-RSA2048
     */
    @ApiModelProperty("若未设置signType，默认值为 WECHATPAY2-SHA256-RSA2048")
    private String signType;

    /**
     * 签名，使用字段appId、timeStamp、nonceStr、package计算得出的签名值
     */
    @ApiModelProperty("签名")
    private String paySign;

}
