package com.idea.relax.redis.lock.config;

import com.idea.relax.redis.config.RelaxRedisProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @className: RelaxRedisLockProperties
 * @description: 分布式锁的配置
 * @author: salad
 * @date: 2022/9/2
 **/
@Getter
@Setter
@Component
@ConfigurationProperties(RelaxRedisLockProperties.PREFIX)
public class RelaxRedisLockProperties {

    /**
     *
     */
    public static final String PREFIX = RelaxRedisProperties.PREFIX + ".lock";

    /**
     *
     */
    private String lockPrefix = "relax_lock:";

    /**
     * 是否启用分布式锁
     */
    private Boolean enabled = Boolean.FALSE;

    /**
     * redis地址
     */
    private String address = "redis://127.0.0.1:6379";

    /**
     * redis客户端密码（选填）
     */
    private String password;

    /**
     * redis使用的数据库
     */
    private Integer database = 0;

    /**
     * 池的大小
     */
    private Integer poolSize = 20;


    private Integer idleSize = 5;


    private Integer idleTimeout = 60000;

    /**
     * 连接超时时间
     */
    private Integer connectionTimeout = 3000;

    /**
     * 超时时间
     */
    private Integer timeout = 10000;

    /**
     * 架构模式（单机、主从）
     */
    private RedissonMode redissonMode = RedissonMode.SINGLE;

    /**
     * 主节点地址
     */
    private String masterAddress;

    /**
     * 从节点地址
     */
    private String[] slaveAddress;

    /**
     * 主节点名称
     */
    private String masterName;

    /**
     * sentinel地址
     */
    private String[] sentinelAddress;

    /**
     * redis集群模式下节点地址
     */
    private String[] nodeAddress;

}
