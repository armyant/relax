package com.idea.relax.boot.config;

import com.idea.relax.boot.security.xss.RelaxXssProperties;
import com.idea.relax.boot.security.xss.XssRequestFilter;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import javax.servlet.DispatcherType;

/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@AllArgsConstructor
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(RelaxXssProperties.class)
@ConditionalOnProperty(prefix = RelaxXssProperties.PREFIX, name = ".enabled", matchIfMissing = true)
public class XssConfiguration {

	private final RelaxXssProperties xssProperties;

	/**
	 * XSS过滤器
	 */
	@Bean
	public FilterRegistrationBean<XssRequestFilter> xssFilterRegistration() {
		FilterRegistrationBean<XssRequestFilter> registration = new FilterRegistrationBean<>();
		registration.setDispatcherTypes(DispatcherType.REQUEST);
		registration.setFilter(new XssRequestFilter(xssProperties));
		registration.addUrlPatterns(xssProperties.getUrlPatterns());
		registration.setName("xssRequestFilter");
		registration.setOrder(Ordered.LOWEST_PRECEDENCE);
		return registration;
	}

}
