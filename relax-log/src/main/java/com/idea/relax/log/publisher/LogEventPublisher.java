package com.idea.relax.log.publisher;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.*;
import org.springframework.stereotype.Component;

/**
 * @className: LogEventPublisher
 * @description: 日志事件发布器工具类
 * @author: salad
 * @date: 2022/6/3
 **/
@Slf4j
@Component
public class LogEventPublisher implements ApplicationEventPublisherAware {

    private static ApplicationEventPublisher publisher;

    public static void publish(ApplicationEvent event) {

        if (publisher != null) {
            try {
                publisher.publishEvent(event);
            } catch (Exception e) {
                log.error("Failed to publish the log event,event name:["+event.getClass().getSimpleName()+"]",e);
                throw e;
            }
        }
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        LogEventPublisher.publisher = publisher;
    }


}