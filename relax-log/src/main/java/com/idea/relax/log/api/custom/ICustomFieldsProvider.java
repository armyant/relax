package com.idea.relax.log.api.custom;

import java.util.Map;

/**
 * @author: 沉香
 * @date: 2023/2/2
 * @description: 自定义日志字段的接口，支持多实现
 */
public interface ICustomFieldsProvider {

	/**
	 * 获取自定义字段的ValueProvider对象
	 *
	 * @return 返回值是一个Map，其中key是字段名称，value是ValueProvider对象
	 */
	Map<String, ValueProvider> getProviders();


}

