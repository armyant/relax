package com.idea.relax.cache.suppert.parser;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * @className: EvaluationContextRootObject
 * @description:
 * @author: salad
 * @date: 2022/10/16
 **/
@Data
public class EvaluationContextRootObject {

    /**
     * 类名
     */
    String className;

    /**
     * 方法名
     */
    String methodName;

    /**
     * 目标方法
     */
    Method method;

    /**
     * 目标类对象
     */
    Object target;

    /**
     * 目标类Class
     */
    Class<?> targetClass;

    /**
     * 请求参数
     */
    Object[] args;


}
