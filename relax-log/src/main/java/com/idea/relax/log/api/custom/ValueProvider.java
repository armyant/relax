package com.idea.relax.log.api.custom;

/**
 * @author: 沉香
 * @date: 2023/2/2
 * @description: 自定义日志字段的Provider
 */
@FunctionalInterface
public interface ValueProvider {

	/**
	 * 获取自定义日志字段的值
	 *
	 * @return 自定义日志字段的值
	 */
	String get();

}
