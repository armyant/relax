package com.idea.relax.log.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @className: ErrorLogModel
 * @description: 错误日志Model对象
 * @author: salad
 * @date: 2022/6/3
 **/
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ErrorLogModel extends LogCommonModel {

    protected static final long serialVersionUID = 1L;

    /**
     * 堆栈信息
     */
    private String stackTrace;

    /**
     * 异常名称
     */
    private String errorName;

    /**
     * 异常信息
     */
    private String errorMessage;

    /**
     * 产生错误的Java文件名
     */
    private String errorFileName;

    /**
     * 代码错误行数
     */
    private Integer errorLineNumber;

    /**
     * 状态
     */
    protected String status;

    /**
     * 目标接口执行时间,单位：ms
     */
    protected Long time;


}
