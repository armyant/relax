package com.idea.relax.crypto.core;

import com.idea.relax.crypto.config.CryptoType;
import org.springframework.stereotype.Component;

/**
 * @author: 沉香
 * @date: 2023/3/19
 * @description:
 */
@Component
public class DesCryptographer implements ICryptographer{

	private static final CryptoType THIS_CRYPTO_TYPE = CryptoType.DES;

	@Override
	public CryptoType cryptoType() {
		return THIS_CRYPTO_TYPE;
	}

	@Override
	public boolean support(CryptoType cryptoType) {
		return THIS_CRYPTO_TYPE.equals(cryptoType);
	}

	@Override
	public byte[] decryptFormBase64(byte[] data, String password) {
		if (Func.isEmpty(password)){
			throw new RuntimeException("");
		}
		return DesUtil.decryptFormBase64(data, password);
	}

	@Override
	public String encryptToBase64(byte[] data, String password) {
		if (Func.isEmpty(password)){
			throw new RuntimeException("");
		}
		return DesUtil.encryptToBase64(data, password);
	}
}
