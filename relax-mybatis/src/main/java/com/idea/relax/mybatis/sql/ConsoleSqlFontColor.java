package com.idea.relax.mybatis.sql;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: SqlFontColors
 * @description:
 * @author: salad
 * @date: 2022/10/23
 **/
@Getter
@AllArgsConstructor
public enum ConsoleSqlFontColor {


    /**
     * 黑色
     */
    BLACK(ConsoleColors.BLACK),

    /**
     *红色
     */
    RED(ConsoleColors.RED),

    /**
     *绿色
     */
    GREEN(ConsoleColors.GREEN),

    /**
     *黄色
     */
    YELLOW(ConsoleColors.YELLOW),

    /**
     *蓝色
     */
    BLUE(ConsoleColors.BLUE),

    /**
     *白色
     */
    WHITE(ConsoleColors.WHITE);

    /**
     * 颜色
     */
    private final String colorText;

}
