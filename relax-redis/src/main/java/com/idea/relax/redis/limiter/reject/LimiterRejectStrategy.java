package com.idea.relax.redis.limiter.reject;

public enum LimiterRejectStrategy {

	/**
	 * 抛出异常
	 */
	THROW_ERR,

	/**
	 * 静默
	 */
	SILENCE,

	/**
	 * 自定义
	 */
	CUSTOMIZE
}
