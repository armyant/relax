package com.idea.relax.log.constant;


/**
 * @author: 沉香
 * @date: 2022/12/14
 * @description: 操作日志相关的常量
 */
public final class ApiLogConstant {

    /**
     * 用作发布操作日志事件时的数据传输
     */
    public static final String API_LOG_MODEL = "model";

    /**
     * 用作发布操作日志事件时的数据传输
     */
    public static final String ROOT_OBJECT = "rootObject";

    /**
     * 操作日志注解的切入点表达式
     */
    public static final String API_LOG_EXP = "@annotation(com.idea.relax.log.annotation.ApiLog)";

}
