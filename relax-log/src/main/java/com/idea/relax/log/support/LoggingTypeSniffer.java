package com.idea.relax.log.support;

import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

/**
 * @className: LoggingTypeSniffer
 * @description: 用于嗅探应用使用的日志框架是什么
 * @author: salad
 * @date: 2023/1/22
 **/
public class LoggingTypeSniffer {

    private static final String LOG4J2_FACTORY = "org.apache.logging.slf4j.Log4jLoggerFactory";

    private static final String LOGBACK_FACTORY = "ch.qos.logback.classic.LoggerContext";

    public static LoggingType sniff() {
        ILoggerFactory factory = LoggerFactory.getILoggerFactory();
        if (LOG4J2_FACTORY.equals(factory.getClass().getName())) {
            return LoggingType.LOG4J2;
        } else if (LOGBACK_FACTORY.equals(factory.getClass().getName())) {
            return LoggingType.LOGBACK;
        }
        return LoggingType.OTHER;
    }


}
