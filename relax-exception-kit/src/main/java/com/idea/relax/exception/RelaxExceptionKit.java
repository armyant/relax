package com.idea.relax.exception;

/**
 * @className: RelaxExceptionKit
 * @description:
 * @author: salad
 * @date: 2023/1/25
 **/
public class RelaxExceptionKit {

	public static final String AUTHOR = "Salad";

	public static final String VERSION = "1.0.0-RELEASE";

}
