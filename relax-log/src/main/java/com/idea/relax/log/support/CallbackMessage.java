package com.idea.relax.log.support;


import java.util.Map;

/**
 * @className: CallbackMessage
 * @description: 日志的异步存储任务执行错误时的回调信息类
 * @author: salad
 * @date: 2023/1/7
 **/
public class CallbackMessage {

    /**
     * 执行任务的线程
     */
    private final Thread thread;

    /**
     * 抛出的异常
     */
    private final Throwable throwable;

    /**
     * 日志相关的数据
     */
    private final Map<String,Object> data;

    public CallbackMessage(Thread thread, Throwable throwable, Map<String, Object> data) {
        this.thread = thread;
        this.throwable = throwable;
        this.data = data;
    }

    public Thread getThread() {
        return thread;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public Map<String, Object> getData() {
        return data;
    }

}
