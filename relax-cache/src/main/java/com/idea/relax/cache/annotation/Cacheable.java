package com.idea.relax.cache.annotation;

import com.idea.relax.redis.lock.support.LockType;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @className: Cacheable
 * @description:
 * @author: salad
 * @date: 2022/10/7
 **/
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cacheable {

    @AliasFor("cacheNames")
    String value() default "";

    @AliasFor("value")
    String cacheNames() default "";

    String key() default "";

    long ttl() default -1L;

    String condition() default "";

    LockType lockType() default LockType.REENTRANT;

}
