package com.idea.relax.redis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className: RelaxRedisProperties
 * @description:
 * @author: salad
 * @date: 2022/9/6
 **/
@Data
@ConfigurationProperties(RelaxRedisProperties.PREFIX)
public class RelaxRedisProperties {

    /**
     *
     */
    public static final String PREFIX = "relax.redis";

    /**
     *
     */
    private final SerializerType serializerType = SerializerType.JSON;


}
