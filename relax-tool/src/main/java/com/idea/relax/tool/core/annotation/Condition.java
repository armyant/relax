package com.idea.relax.tool.core.annotation;

/**
 * @author: 沉香
 * @date: 2023/3/20
 * @description:
 */
public enum Condition {

	/**
	 * 属性为空
	 */
	EMPTY,

	/**
	 * 属性不为空
	 */
	NOT_EMPTY,

	/**
	 * 属性等于某个值
	 */
	EQ

}
