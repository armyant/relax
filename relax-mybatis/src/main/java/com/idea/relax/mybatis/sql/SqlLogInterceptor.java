package com.idea.relax.mybatis.sql;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.idea.relax.mybatis.config.RelaxMybatisProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author salad
 */
@Slf4j
@Intercepts({
	@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class}),
	@Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class SqlLogInterceptor implements Interceptor, EnvironmentPostProcessor {

	private final RelaxMybatisProperties properties;

	public SqlLogInterceptor(RelaxMybatisProperties properties) {
		this.properties = properties;
	}

	private String mainClassPackage;


	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
		Object parameter = null;
		if (invocation.getArgs().length > 1) {
			parameter = invocation.getArgs()[1];
		}
		String sqlId = mappedStatement.getId();
		BoundSql boundSql = mappedStatement.getBoundSql(parameter);
		Configuration configuration = mappedStatement.getConfiguration();
		long start = System.currentTimeMillis();
		Object ret;
		long time = 0L;
		try {
			ret = invocation.proceed();
			time = System.currentTimeMillis() - start;
		} catch (Exception e) {
			ErrorCauseData errorCauseData = wrapError(e);
			outSql(configuration, boundSql, time, sqlId, errorCauseData);
			throw e;
		}
		outSql(configuration, boundSql, time, sqlId, null);
		return ret;
	}

	private ErrorCauseData wrapError(Exception e) {
		if (mainClassPackage != null) {
			StackTraceElement stackTraceElement = Exceptions.getStackTraceByPrefix(e, mainClassPackage);
			if (stackTraceElement != null) {
				String className = stackTraceElement.getClassName();
				String fileName = stackTraceElement.getFileName();
				int lineNumber = stackTraceElement.getLineNumber();
				String methodName = stackTraceElement.getMethodName();
				return ErrorCauseData.builder()
					.fileName(fileName)
					.className(className)
					.methodName(methodName)
					.lineNumber(lineNumber)
					.build();
			}
		}
		return ErrorCauseData.EMPTY;
	}

	private void outSql(Configuration configuration, BoundSql boundSql, long time, String sqlId, ErrorCauseData errorCauseData) {
		Object parameterObject = boundSql.getParameterObject();
		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
		//替换空格、换行、tab缩进等
		String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
		if (parameterMappings.size() > 0 && parameterObject != null) {
			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
			if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
				sql = sql.replaceFirst("\\?", getParameterValue(parameterObject));
			} else {
				MetaObject metaObject = configuration.newMetaObject(parameterObject);
				for (ParameterMapping parameterMapping : parameterMappings) {
					String propertyName = parameterMapping.getProperty();
					if (metaObject.hasGetter(propertyName)) {
						Object obj = metaObject.getValue(propertyName);
						sql = sql.replaceFirst("\\?", getParameterValue(obj));
					} else if (boundSql.hasAdditionalParameter(propertyName)) {
						Object obj = boundSql.getAdditionalParameter(propertyName);
						sql = sql.replaceFirst("\\?", getParameterValue(obj));
					}
				}
			}
		}
		if (errorCauseData != null) {
			errorLog(sql, sqlId, errorCauseData);
		} else {
			infoLog(time, sql, sqlId);
		}
	}

	private static String getParameterValue(Object obj) {
		String value;
		if (obj instanceof String) {
			value = "'" + obj.toString() + "'";
		} else if (obj instanceof Date) {
			DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
			value = "'" + formatter.format(new Date()) + "'";
		} else {
			if (obj != null) {
				value = obj.toString();
			} else {
				value = "";
			}
		}
		return value.replace("$", "\\$");
	}

	/**
	 * 日志模式 两种
	 * 日志类型 两种
	 *
	 * @param time
	 * @param sql
	 * @param sqlId
	 */
	private void infoLog(long time, String sql, String sqlId) {
		StringBuilder sqlLog = new StringBuilder();
		if (properties.getSqlLog().getSqlOutputMode()
			.equals(RelaxMybatisProperties.SqlOutputMode.LOG)) {
			sqlLog.append("\n==============  Sql Start  ==============")
				.append("\nExecute Time：").append(time).append(" ms")
				.append("\nExecute ID  ：").append(sqlId)
				.append("\nExecute SQL ：").append(sql)
				.append("\n==============  Sql  End   ==============\n");
			log.info(sqlLog.toString());
		} else {
			sqlLog.append(properties.getSqlLog().getConsoleSqlFontColor().getColorText())
				.append("\n==============  Sql Start  ==============");
			sqlLog.append("\nExecute ID  ：").append(sqlId)
				.append("\nExecute SQL ：").append(sql);
			sqlLog.append("\n==============  Sql  End   ==============\n")
				.append(ConsoleColors.RESET);
			System.out.println(sqlLog);
		}
	}

	private void errorLog(String sql, String sqlId, ErrorCauseData errorCauseData) {
		StringBuilder sqlLog = new StringBuilder();
		if (properties.getSqlLog().getSqlOutputMode()
			.equals(RelaxMybatisProperties.SqlOutputMode.LOG)) {
			sqlLog.append("\n==============  Sql Start  ==============")
				.append("\nExecute ID  ：").append(sqlId)
				.append("\nExecute SQL ：").append(sql)
				.append("\nError Cause ：").append(errorCauseData.getClassName()).append(".")
				.append(errorCauseData.getMethodName()).append("[").append(errorCauseData.getLineNumber()).append("]")
				.append("\n==============  Sql  End   ==============\n");
			log.error(sqlLog.toString());
		} else {
			sqlLog.append(ConsoleSqlFontColor.RED.getColorText())
				.append("\n==============  Sql Start  ==============")
				.append("\nExecute ID  ：").append(sqlId)
				.append("\nExecute SQL ：").append(sql)
				.append("\nError Cause ：").append(errorCauseData.getClassName()).append(".")
				.append(errorCauseData.getMethodName()).append("[").append(errorCauseData.getLineNumber()).append("]")
				.append("\n==============  Sql  End   ==============\n")
				.append(ConsoleColors.RESET);
			System.out.println(sqlLog);
		}
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}


	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
		mainClassPackage = application.getMainApplicationClass().getPackage().getName();
	}

}
