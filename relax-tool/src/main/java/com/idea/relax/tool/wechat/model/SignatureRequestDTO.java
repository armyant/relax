package com.idea.relax.tool.wechat.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Doc 微信通知接口头部信息，需要做签名验证
 * 文档地址: https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay4_1.shtml
 * @See @see Serializable
 * @Author Mz
 * @Date 2023/02/24
 * @Version 1.0.0
 */
@Data
@NoArgsConstructor
public class SignatureRequestDTO implements Serializable {

    private static final long serialVersionUID = -1L;

    /**
     * 时间戳
     */
    private String timeStamp;

    /**
     * 随机串
     */
    private String nonce;

    /**
     * 已签名字符串
     */
    private String signature;

    /**
     * 证书序列号
     */
    private String serial;

    /**
     * 验签类型
     */
    private String signatureType;

    /**
     * Http请求体
     */
    private String body;

}
