package com.idea.relax.cache.suppert.lock;

import com.idea.relax.redis.lock.support.LockType;
import com.idea.relax.redis.lock.client.RedisLockClient;
import lombok.AllArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @className: CacheLockClientImpl
 * @description:
 * @author: salad
 * @date: 2022/10/10
 **/
@AllArgsConstructor
public class CacheLockClientImpl implements CacheLockClient{

    private final RedisLockClient redisLockClient;

    @Override
    public boolean tryLock(String lockName, LockType lockType,
                           long waitTime, long leaseTime, TimeUnit timeUnit) throws InterruptedException {
        return redisLockClient.tryLock(lockName, lockType, waitTime, leaseTime, timeUnit);
    }


    @Override
    public void unLock(String lockName, LockType lockType) {
        redisLockClient.unLock(lockName, lockType);
    }


}
