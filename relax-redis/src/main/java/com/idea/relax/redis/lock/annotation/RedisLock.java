package com.idea.relax.redis.lock.annotation;

import com.idea.relax.redis.lock.support.LockType;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Redis分布式锁的注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface RedisLock {

    /**
     * 锁的名称
     */
    String value();

    /**
     * spEL表达式
     */
    String param() default "";

    /**
     * 等待时间
     */
    long waitTime() default 30L;

    /**
     * 锁的租借时间
     */
    long leaseTime() default 100L;

    /**
     * 时间单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 锁类型：公平锁 非公平锁
     */
    LockType type() default LockType.FAIR;
}
