package com.idea.relax.cache.config;

import com.idea.relax.cache.props.RelaxCacheProperties;
import com.idea.relax.cache.suppert.lock.CacheLockClient;
import com.idea.relax.cache.suppert.lock.CacheLockClientImpl;
import com.idea.relax.cache.suppert.func.IFunction;
import com.idea.relax.cache.suppert.func.IFunctionHandler;
import com.idea.relax.cache.suppert.func.RelaxFunctionHandler;
import com.idea.relax.cache.suppert.parser.CacheExpressionParser;
import com.idea.relax.cache.suppert.parser.CacheSpElExpressionParser;
import com.idea.relax.redis.lock.client.RedisLockClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @className: RelaxCacheAutoConfiguration
 * @description:
 * @author: salad
 * @date: 2022/10/9
 **/
@Configuration(
        proxyBeanMethods = false
)
@EnableConfigurationProperties(RelaxCacheProperties.class)
public class RelaxCacheAutoConfiguration {


    //    @Bean
//    @ConditionalOnMissingBean(Cache.class)
//    @ConditionalOnSingleCandidate
//    public Cache cache(RedisOps redisOps){
//        return new RedisCache(redisOps);
//    }
//
    @Bean
    @ConditionalOnMissingBean(CacheExpressionParser.class)
    @ConditionalOnSingleCandidate
    public CacheExpressionParser cacheKeySpElExpressionParser(){
        return new CacheSpElExpressionParser();
    }


    @Bean
    @ConditionalOnMissingBean(IFunctionHandler.class)
    @ConditionalOnSingleCandidate
    public IFunctionHandler<IFunction> functionHandler(){
        return new RelaxFunctionHandler();
    }

    @Bean
    @ConditionalOnMissingBean(CacheLockClient.class)
    @ConditionalOnSingleCandidate
    public CacheLockClient cacheLockClient(RedisLockClient redisLockClient){
        return new CacheLockClientImpl(redisLockClient);
    }



}
