local fieldIndex=3
local valueIndex=4
local key=KEYS[1]
local fieldCount=ARGV[1]
local expired=ARGV[2]
local count=0
for i=1,fieldCount,1 do
  redis.pcall('HSET',key,ARGV[fieldIndex],ARGV[valueIndex])
  fieldIndex=fieldIndex+2
  valueIndex=valueIndex+2
  count=count+1
end
redis.pcall('EXPIRE',key,expired)
return count