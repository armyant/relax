package com.idea.relax.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.idea.relax.log.support.utils.StringPool;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @className: LogCommonModel
 * @description: 日志公共Model对象
 * @author: salad
 * @date: 2022/6/1
 **/
@Data
@Accessors(chain = true)
public class LogCommonModel implements Serializable {

    protected static final long serialVersionUID = 1L;


    /**
     * 应用的服务名称
     */
    protected String appName;

    /**
     * 服务器的IP
     */
    protected String serverIp;

    /**
     * 应用的端口
     */
    protected Integer appPort;

    /**
     * 服务器名
     */
    protected String serverName;

    /**
     * 环境
     */
    protected String env;

    /**
     * 请求来源方的IP
     */
    protected String visitorIp;

    /**
     * 用户代理
     */
    protected String userAgent;

    /**
     * 请求URI
     */
    protected String requestUri;

    /**
     * 请求方式
     */
    protected String requestMethod;

    /**
     * 方法所在的类
     */
    protected String methodClass;

    /**
     * 方法名
     */
    protected String methodName;

    /**
     * 请求提交的数据
     */
    protected String requestParams = StringPool.EMPTY;

    /**
     * 创建人，默认是空的，需要手动去设置
     */
    protected String createUser;

    /**
     * 请求时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    protected Date requestTime;


}
