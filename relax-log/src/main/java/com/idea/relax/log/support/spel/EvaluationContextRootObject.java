package com.idea.relax.log.support.spel;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * @className: EvaluationContextRootObject
 * @description: SPel表达式的Root对象
 * @author: salad
 * @date: 2022/10/16
 **/
@Data
public class EvaluationContextRootObject {

    /**
     * 类名
     */
    String className;

    /**
     * 方法名
     */
    String methodName;

    /**
     * 目标方法
     */
    Method method;

    /**
     * 目标类对象
     */
    Object target;

    /**
     * 目标类Class
     */
    Class<?> targetClass;

    /**
     * 请求参数
     */
    Object[] args;


}
