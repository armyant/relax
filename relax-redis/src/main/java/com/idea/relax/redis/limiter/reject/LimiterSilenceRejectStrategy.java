package com.idea.relax.redis.limiter.reject;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author: 沉香
 * @date: 2023/2/15
 * @description:
 */
@Slf4j
public class LimiterSilenceRejectStrategy implements RedisLimiterRejectStrategy {

    @Override
    public Object reject(String key, long count, long period, TimeUnit timeUnit) {
        log.warn("您的访问次数已超限：{}，速率：{}", key, count / timeUnit.toSeconds(period));
        //什么都不做...
        return null;
    }

}
