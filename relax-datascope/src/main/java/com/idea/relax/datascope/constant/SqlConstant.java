package com.idea.relax.datascope.constant;

/**
 * @className: SqlConstant
 * @description:
 * @author: salad
 * @date: 2022/11/8
 **/
public interface SqlConstant {

	String SCOPE = "scope";

	String SELECT_PREFIX = "select " + SCOPE + ".* from ({}) " + SCOPE;

//    String WHERE = "where scope.{} in ({})";

	String WHERE = "where {}";


}
