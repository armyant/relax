package com.idea.relax.tool.core;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Bean属性
 * 参考自blade-tool
 *
 * @author Chill
 */
@Getter
@AllArgsConstructor
public class BeanProperty {
	private final String name;
	private final Class<?> type;
}
