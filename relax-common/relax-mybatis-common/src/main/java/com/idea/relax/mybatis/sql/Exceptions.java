package com.idea.relax.mybatis.sql;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 异常工具类
 */
class Exceptions {

	/**
	 * 获取指定包名为前缀的堆栈异常信息
	 *
	 * @param throwable 异常
	 * @param prefix    指定的包前缀
	 * @return 堆栈信息
	 */
	public static StackTraceElement getStackTraceByPrefix(Throwable throwable, String prefix) {
		StackTraceElement[] trace = throwable.getStackTrace();
		for (StackTraceElement e : trace) {
			if (e.getClassName().startsWith(prefix)) {
				return e;
			}
		}
		int length = prefix.split(".").length;
		if (length == 1) {
			return null;
		}
		prefix = prefix.substring(0, prefix.lastIndexOf('.'));
		getStackTraceByPrefix(throwable, prefix);
		return null;
	}


}
