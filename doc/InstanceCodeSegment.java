package com.idea.relax.exception.segments;

import com.idea.relax.exception.RelaxToolProperties;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author: 沉香
 * @date: 2023/2/21
 * @description:
 */
@Component
@AllArgsConstructor
public class InstanceCodeSegment implements ICodeSegment{

	private RelaxToolProperties properties;


	@Override
	public String getSegment() {
		return properties.getInstanceCodeSegment();
	}

}
