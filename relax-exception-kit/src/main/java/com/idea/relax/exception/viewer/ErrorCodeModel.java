package com.idea.relax.exception.viewer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Comparator;

/**
 * @className: ErrorCodeModel
 * @description: 获取异常码接口的响应实体
 * @author: salad
 * @date: 2023/5/25
 **/
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "code")
public class ErrorCodeModel{

	/**
	 * 异常码
	 */
	@JsonProperty("异常码")
	String code;

	/**
	 * 异常message
	 */
	@JsonProperty("异常信息")
	String message;

	/**
	 * 用户提示
	 */
	@JsonProperty("用户提示")
	String userTip;

	/**
	 * 枚举值名称
	 */
	@JsonProperty("枚举值名称")
	String enumName;

	/**
	 * 异常枚举类名称
	 */
	@JsonProperty("异常枚举类名称")
	String className;

	public ErrorCodeModel(String code, String message) {
		this.code = code;
		this.message = message;
	}


	public ErrorCodeModel(String code, String message, String enumName) {
		this.code = code;
		this.message = message;
		this.enumName = enumName;
	}

	public ErrorCodeModel(String code, String message, String userTip, String enumName) {
		this.code = code;
		this.message = message;
		this.userTip = userTip;
		this.enumName = enumName;
	}

	public ErrorCodeModel(String code, String message, String userTip, String enumName, String className) {
		this.code = code;
		this.message = message;
		this.userTip = userTip;
		this.enumName = enumName;
		this.className = className;
	}



}
