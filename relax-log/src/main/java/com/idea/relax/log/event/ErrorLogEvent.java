package com.idea.relax.log.event;

import com.idea.relax.log.publisher.ILogPublisher;
import org.springframework.context.ApplicationEvent;

/**
 * @className: ErrorLogEvent
 * @description: 错误日志的事件,当记录错误日志时，会发布该事件
 * @author: salad
 * @date: 2022/10/1
 **/
public class ErrorLogEvent extends ApplicationEvent {

    public ErrorLogEvent(ILogPublisher.Wrapper source) {
        super(source);
    }

}
