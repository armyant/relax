package com.idea.relax.log.service;


import com.idea.relax.log.model.ErrorLogModel;

/**
 * @className: IErrorLogService
 * @description: 持久化错误日志的接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface IErrorLogService{

    /**
     * 持久化错误日志
     * @param errorLogModel 错误日志Model对象
     */
    void saveErrorLog(ErrorLogModel errorLogModel);


}
