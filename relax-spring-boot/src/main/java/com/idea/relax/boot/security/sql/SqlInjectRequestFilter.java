package com.idea.relax.boot.security.sql;

import lombok.AllArgsConstructor;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @className: SqlInjectRequestFilter
 * @description:
 * @author: salad
 * @date: 2023/4/5
 **/
@AllArgsConstructor
public class SqlInjectRequestFilter implements Filter {

	private final RelaxSqlInjectProperties properties;

	private final AntPathMatcher ANT_PATH_MATCHER = new AntPathMatcher();

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {

		String path = ((HttpServletRequest) request).getServletPath();
		if (!properties.isEnabled() || isSkipUrl(path)) {
			chain.doFilter(request, response);
		}
		ProcessStrategy processStrategy = properties.getProcessStrategy();
		//SQL注入过滤
		SqlInjectHttpServletRequestWrapper requestWrapper = new SqlInjectHttpServletRequestWrapper((HttpServletRequest) request, processStrategy);
		chain.doFilter(requestWrapper, response);

	}

	private boolean isSkipUrl(String path) {
		return properties.getSkipUrls().stream().anyMatch(pattern -> ANT_PATH_MATCHER.match(pattern, path));
	}

}
