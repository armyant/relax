package com.idea.relax.redis.support.utils;

import java.util.Base64;

/**
 * @author: 沉香
 * @date: 2023/1/4
 * @description:
 */
class Base64Util {

    public static String toBase64String(String obj) throws Exception {
        if (null == obj || obj.trim().length() == 0) {
            return null;
        }
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] bytes = obj.getBytes("UTF-8");
        return encoder.encodeToString(bytes);
    }

}
