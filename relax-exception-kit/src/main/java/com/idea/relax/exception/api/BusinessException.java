package com.idea.relax.exception.api;

/**
 * @author: 沉香
 * @date: 2023/2/28
 * @description: 业务异常类
 */
public class BusinessException extends BaseException {

	public BusinessException(final IErrorLevel level, final IErrorCode code, final Object... args) {
		super(level, ErrorType.BUSINESS, code, args);
	}

}
