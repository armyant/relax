package com.idea.relax.mybatis.interceptor;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.core.Ordered;

/**
 * @className: QueryInterceptor
 * @description: 查询拦截器
 * @author: salad
 * @date: 2023/4/8
 **/
@SuppressWarnings("rawtypes")
public interface QueryInterceptor extends Ordered{

	/**
	 * 对查询操作进行拦截
	 *
	 * @param executor
	 * @param ms
	 * @param parameter
	 * @param rowBounds
	 * @param resultHandler
	 * @param boundSql
	 */
	void intercept(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql);

}
