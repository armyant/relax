package com.idea.relax.mybatis.config;

import com.idea.relax.mybatis.sql.ConsoleSqlFontColor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className: RelaxMybatisProperties
 * @description: TODO 类描述
 * @author: salad
 * @date: 2022/10/23
 **/
@Getter
@ConfigurationProperties(prefix = RelaxMybatisProperties.PREFIX)
public class RelaxMybatisProperties {

	public final static String PREFIX = "relax.mybatis-plus";

	private final Pagination pagination = new Pagination();

	private final SqlLog sqlLog = new SqlLog();

	private final Tenant tenant = new Tenant();

	private final Toolkit toolkit = new Toolkit();


	@Getter
	@Setter
	public static class Toolkit {

		public static final String PREFIX = RelaxMybatisProperties.PREFIX + ".toolkit";

		private String[] queryWrapperExcludeFields = {"createUser", "createDept", "createTime", "updateUser", "updateTime", "isDeleted"};

	}

	@Getter
	@Setter
	public static class Pagination {

		public static final String PREFIX = RelaxMybatisProperties.PREFIX + ".pagination";

		/**
		 * 是否开启
		 */
		private boolean enabled = true;

		/**
		 * 分页最大数
		 */
		private Long pageLimit = 500L;

		/**
		 * 溢出总页数后是否进行处理
		 */
		private boolean overflow = false;

	}

	@Getter
	@Setter
	public static class SqlLog {

		public static final String PREFIX = RelaxMybatisProperties.PREFIX + ".sql-log";

		/**
		 * 是否开启SQL打印
		 */
		private boolean enabled = true;

		/**
		 * SQL输出模式（控制台、日志文件）
		 */
		private SqlOutputMode sqlOutputMode = SqlOutputMode.CONSOLE;

		/**
		 * 颜色
		 */
		private final ConsoleSqlFontColor consoleSqlFontColor = ConsoleSqlFontColor.GREEN;


	}

	public enum SqlOutputMode {

		CONSOLE,

		LOG


	}

	@Getter
	@Setter
	public static class Tenant {

		public static final String PREFIX = RelaxMybatisProperties.PREFIX + ".tenant";

		/**
		 * 是否开启
		 */
		private boolean enabled = true;

		/**
		 * 对应数据库租户ID的列名
		 */
		private String tenantIdColumn = "tenant_id";

		/**
		 * 不需要参与租户的表
		 */
		private String[] ignoreTables = {};

	}
}
