package com.idea.relax.sensitive.processor;

import com.idea.relax.sensitive.processor.impl.CoverSensitiveProcessor;
import com.idea.relax.sensitive.processor.impl.TransformSensitiveProcessor;

import java.util.function.Supplier;

/**
 * @className: DefaultProcessorsSupplier
 * @description:
 * @author: salad
 * @date: 2023/7/16
 **/
public interface DefaultProcessorsSupplier {

	Supplier<ISensitiveProcessor<Object>[]> PROCESSORS = () -> new ISensitiveProcessor[]{CoverSensitiveProcessor.INSTANCE, TransformSensitiveProcessor.INSTANCE};


}
