package com.idea.relax.redis.lock.support;

@FunctionalInterface
public interface CheckedSupplier<T> {
    T get() throws Throwable;
}
