package com.idea.relax.exception.api;

/**
 * @author: 沉香
 * @date: 2023/2/20
 * @description: 异常码基类，自定义异常码必须实现该类
 */
public interface IErrorCode {


	//*********************************************
	// 				 必须要实现的方法
	//*********************************************

	/**
	 * 获取异常码
	 *
	 * @return 异常码
	 */
	String getErrorCode();

	/**
	 * 获取异常message
	 *
	 * @return message
	 */
	String getMessage();

	//*********************************************
	// 				可选的方法
	//*********************************************

	/**
	 * 获取父级CodeSegment
	 *
	 * @return
	 */
	default ICodeSegment leftCodeSegment() {
		return null;
	}

	/**
	 * 获取状态码，默认为500
	 *
	 * @return 状态码
	 */
	default Integer getCode() {
		return 500;
	}

	/**
	 * 获取用户提示语
	 *
	 * @return 用户提示语
	 */
	default String getUserTip() {
		return null;
	}


}
