package com.idea.relax.redis;

/**
 * @className: RelaxRedis
 * @description:
 * @author: salad
 * @date: 2023/2/15
 **/
public class RelaxRedis {

    public static final String AUTHOR = "Salad";

    public static final String VERSION = "1.0.0-RELEASE";

}
