package com.idea.relax.log.model;

import com.idea.relax.log.support.utils.StringPool;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @className: ApiLogModel
 * @description: 操作日志Model对象
 * @author: salad
 * @date: 2022/6/1
 **/
@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ApiLogModel extends LogCommonModel {

    protected static final long serialVersionUID = 1L;

    /**
     * 日志模块名
     */
    private String title;

    /**
     * 操作类型
     */
    private String operationType;

    /**
     * 操作描述
     */
    private String description;

    /**
     * 响应的数据
     */
    protected String responseData = StringPool.EMPTY;

    /**
     * 状态
     */
    protected String status;

    /**
     * 目标接口执行时间,单位：ms
     */
    protected Long time;


}
