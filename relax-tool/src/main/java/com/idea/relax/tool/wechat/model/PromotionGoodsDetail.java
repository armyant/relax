package com.idea.relax.tool.wechat.model;

import lombok.Setter;

/**
 * PromotionGoodsDetail
 */
@Setter
public class PromotionGoodsDetail {
    /**
     * 商品编码 说明：商品编码
     */
    private String goods_id;
    /**
     * 商品数量 说明：商品数量
     */
    private Integer quantity;
    /**
     * 商品价格 说明：商品价格
     */
    private Integer unit_price;
    /**
     * 商品优惠金额 说明：商品优惠金额
     */
    private Integer discount_amount;
    /**
     * 商品备注 说明：商品备注
     */
    private String goods_remark;

    public String getGoodsId() {
        return goods_id;
    }

    public void setGoodsId(String goodsId) {
        this.goods_id = goodsId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getUnitPrice() {
        return unit_price;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unit_price = unitPrice;
    }

    public Integer getDiscountAmount() {
        return discount_amount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discount_amount = discountAmount;
    }

    public String getGoodsRemark() {
        return goods_remark;
    }

    public void setGoodsRemark(String goodsRemark) {
        this.goods_remark = goodsRemark;
    }

}
