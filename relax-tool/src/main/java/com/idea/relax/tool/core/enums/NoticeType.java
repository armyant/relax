package com.idea.relax.tool.core.enums;

import lombok.AllArgsConstructor;

/**
 * @author: 沉香
 * @date: 2023/5/10
 * @description:
 */
@AllArgsConstructor
public enum NoticeType implements IEnumKeyValueBase<String, String> {

	/**
	 * 消费者公告
	 */
	CONSUMER_TYPE("消费者公告", "1"),

	/**
	 * 本机构公告
	 */
	UNIT_TYPE("本机构公告", "2"),

	/**
	 * 平台公告
	 */
	PLATFORM_TYPE("平台公告", "3");

	private String key;

	private String value;

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getValue() {
		return value;
	}
}
