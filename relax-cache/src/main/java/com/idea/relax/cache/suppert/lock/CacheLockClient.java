package com.idea.relax.cache.suppert.lock;


import com.idea.relax.redis.lock.support.LockType;

import java.util.concurrent.TimeUnit;

/**
 * @className: CacheLockClient
 * @description: TODO 类描述
 * @author: salad
 * @date: 2022/10/10
 **/
public interface CacheLockClient {

    /**
     * 尝试获取锁
     * @param lockName
     * @param lockType
     * @param waitTime
     * @param leaseTime
     * @param timeUnit
     * @return
     * @throws InterruptedException
     */
    boolean tryLock(String lockName, LockType lockType, long waitTime,
                    long leaseTime, TimeUnit timeUnit) throws InterruptedException;


    /**
     * 释放锁
     * @param lockName
     * @param lockType
     */
    void unLock(String lockName, LockType lockType);


}
