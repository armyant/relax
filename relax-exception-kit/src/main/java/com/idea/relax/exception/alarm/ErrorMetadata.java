package com.idea.relax.exception.alarm;

import com.idea.relax.exception.api.IErrorCode;
import com.idea.relax.exception.api.IErrorLevel;
import com.idea.relax.exception.api.IErrorType;
import lombok.Data;

/**
 * @author: 沉香
 * @date: 2023/2/22
 * @description: 异常元数据
 */
@Data
public class ErrorMetadata {

	/**
	 * 异常等级
	 */
	private IErrorLevel errorLevel;

	/**
	 * 异常类型
	 */
	private IErrorType errorType;

	/**
	 * 异常码
	 */
	private IErrorCode errorCode;

}
