package com.toplion.project.tools.service;

import cn.hutool.core.thread.ThreadUtil;
import com.toplion.project.tools.constants.Config;
import com.toplion.project.tools.domain.PodLogWatchThread;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import lombok.extern.slf4j.Slf4j;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author asm
 */
@Slf4j
public class K8sService {
    private static K8sService k8sService;

    private final Config config = Config.getInstance();

    public static K8sService getInstance() {
        if (k8sService == null) {
            k8sService = new K8sService();
        }
        return k8sService;
    }

    private KubernetesClient kubernetesClient = getKubernetesClient();

    public KubernetesClient getKubernetesClient() {
        if (kubernetesClient == null) {
            io.fabric8.kubernetes.client.Config k8sConfig = new ConfigBuilder().withMasterUrl(config.getK8sMasterUrl()).withTrustCerts(true).withCaCertData(config.getK8sCaCert()).withClientCertData(config.getK8sClientCert()).withClientKeyData(config.getK8sClientKey()).build();
            kubernetesClient = new KubernetesClientBuilder().withConfig(k8sConfig).build();
        }
        return kubernetesClient;
    }

    /**
     * 重启
     *
     * @param namespace
     * @param deploymentName
     * @return
     */
    public Deployment restart(String namespace, String deploymentName) {
        return kubernetesClient.apps().deployments().inNamespace(namespace).withName(deploymentName).rolling().restart();
    }

    public Deployment getDeployment(String namespace, String deploymentName) {
        return kubernetesClient.apps().deployments().inNamespace(namespace).withName(deploymentName).get();
    }

    /**
     * 监控日志
     * 终止直接执行线程 thread.interrupt();
     */
    public Thread watchLog(String namespace, String podName, Consumer<String> consumer) {
        PodLogWatchThread podLogWatchThread = new PodLogWatchThread();
        podLogWatchThread.setNamespace(namespace);
        podLogWatchThread.setPodName(podName);
        podLogWatchThread.setKubernetesClient(getInstance().kubernetesClient);
        podLogWatchThread.setConsumer(consumer);
        Thread thread = new Thread(podLogWatchThread);
        thread.start();
        return thread;
    }

    public List<String> getPodNameList(String namespace) {
        return kubernetesClient.pods().inNamespace(namespace).list().getItems().stream().map(Pod::getMetadata).map(ObjectMeta::getName).collect(Collectors.toList());
    }


    public void watch() {
        List<String> podNameList = getPodNameList(config.getK8sNamespace());
        watchLog(config.getK8sNamespace(), podNameList.get(0), Console::printlnOut);
    }

    public boolean k8sRestart() {
        Console.printlnStep("重启k8s");
        try {
            restart(config.getK8sNamespace(), config.getK8sDeploymentName());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}





