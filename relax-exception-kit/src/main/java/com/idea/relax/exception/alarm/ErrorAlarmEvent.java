package com.idea.relax.exception.alarm;

import org.springframework.context.ApplicationEvent;

/**
 * @author: 沉香
 * @date: 2023/2/22
 * @description: 异常告警事件
 */
public class ErrorAlarmEvent extends ApplicationEvent {

    public ErrorAlarmEvent(ErrorMetadata source) {
        super(source);
    }

}
