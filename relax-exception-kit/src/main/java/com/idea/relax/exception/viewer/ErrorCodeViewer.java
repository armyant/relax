package com.idea.relax.exception.viewer;

import com.idea.relax.exception.config.ExceptionKitProperties;
import com.idea.relax.exception.constant.EkConstant;
import com.idea.relax.exception.core.manager.ErrorCodeManager;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author: 沉香
 * @date: 2023/5/25
 * @description: 异常码文档相关接口
 */
@RestController
@RequestMapping(EkConstant.VIEW_URL)
@AllArgsConstructor
public class ErrorCodeViewer {

	private final ExceptionKitProperties properties;

	/**
	 * 返回列表形式的异常码文档
	 *
	 * @return HTML页面
	 */
	@GetMapping(produces = MediaType.TEXT_HTML_VALUE)
	public String welcome() {
		return WelcomeHtml.buildWelHtml(properties.getDocTitle(), ErrorCodeManager.getCodeList());
	}

	/**
	 * 返回JSON形式的异常码文档，一级异常码不会在JSON数据中展示
	 *
	 * @return JSON
	 */
	@GetMapping(EkConstant.VIEW_TREE_URL)
	public Set<NodeModel> tree() {
		return ErrorCodeManager.getCodeTree();
	}


}
