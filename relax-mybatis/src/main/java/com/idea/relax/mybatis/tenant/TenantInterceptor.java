package com.idea.relax.mybatis.tenant;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import lombok.AllArgsConstructor;

/**
 * @className: TenantInterceptor
 * @description:
 * @author: salad
 * @date: 2023/4/30
 **/
public class TenantInterceptor extends TenantLineInnerInterceptor {

	@Override
	public void setTenantLineHandler(TenantLineHandler tenantLineHandler) {
		super.setTenantLineHandler(tenantLineHandler);
	}



}
