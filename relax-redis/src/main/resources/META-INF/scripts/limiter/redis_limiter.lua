-- 当前时间戳
local now = ARG[1]
-- 超时的时间周期，单位：毫秒
local period = ARG[2]
-- 限流的大小
local count = ARG[3]
-- 过期的时间点
local expired = ARG[4]
-- 被限流的资源标识key
local key = KEYS[1]
-- 先add一个成员
redis.pcall("ZADD",key,now,now)
-- 然后，清除过期的数据,移除指定区间内的所有成员
redis.pcall('ZREMRANGEBYSCORE', key, 0, expired)
-- 最后，统计剩余成员的数量
local limitNumber = tonumber(redis.pcall("ZCARD",key))
if limitNumber > count then
    -- 达到限流大小 返回 0
    return 0;
else
    -- 毫秒为单位设置 key 的生存时间
    redis.pcall('EXPIRE', key, tonumber(period))
    return limitNumber;
end

