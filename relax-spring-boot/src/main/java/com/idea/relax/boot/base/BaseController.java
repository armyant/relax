package com.idea.relax.boot.base;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.idea.relax.boot.file.FileObject;
import com.idea.relax.boot.file.FileObjects;
import com.idea.relax.boot.file.FileUtil;
import com.idea.relax.mybatis.page.PageReqVO;
import com.idea.relax.mybatis.toolkit.Condition;
import com.idea.relax.tool.core.excel.easyexcel.ExcelUtils;
import com.idea.relax.tool.core.excel.easyexcel.Importer;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: 沉香
 * @date: 2023/4/21
 * @description:
 */
public class BaseController {


	public <T> QueryWrapper<T> getQueryWrapper(T target, String... exclude) {
		return Condition.getQueryWrapper(target, exclude);
	}

	public <T> IPage<T> getPage(PageReqVO pageReqVO) {
		return Condition.getPage(pageReqVO);
	}

	public void download(HttpServletResponse response, String absFilePath, String fileName) throws Exception {
		FileUtil.download(absFilePath, fileName, response);
	}

	public <T> void exportExcel(HttpServletResponse response,
								String fileName, String sheetName,
								List<T> dataList, Class<T> clazz) throws IOException {
		ExcelUtils.export(response, fileName, sheetName, dataList, clazz);
	}

	public <T> void saveExcel(MultipartFile file, Class<T> clazz, int sheetNo, Importer<T> importer) {
		ExcelUtils.importSave(file, clazz, sheetNo, importer);
	}

	public FileObject getFileObject(MultipartFile file) {
		return new FileObject(file);
	}

	public FileObjects getFileObjects(MultipartFile[] file) {
		List<FileObject> fileObjects = new ArrayList<>();
		for (MultipartFile multipartFile : file) {
			fileObjects.add(new FileObject(multipartFile));
		}
		return new FileObjects(fileObjects);
	}


}
