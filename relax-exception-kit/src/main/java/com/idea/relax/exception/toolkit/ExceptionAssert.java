package com.idea.relax.exception.toolkit;


import org.springframework.util.ObjectUtils;

import java.util.function.Supplier;

/**
 * @author: 沉香
 * @date: 2023/2/21
 * @description: 异常断言类
 */
public class ExceptionAssert {


	/**
	 * 断言表达式为true的情况下抛出异常
	 *
	 * @param expression        断言的表达式
	 * @param exceptionSupplier 异常的供给者
	 */
	public static void isTrueThrow(boolean expression, Supplier<RuntimeException> exceptionSupplier) {
		if (expression) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * 断言表达式为false的情况下抛出异常
	 *
	 * @param expression        断言的表达式
	 * @param exceptionSupplier 异常的供给者
	 */
	public static void isFalseThrow(boolean expression, Supplier<RuntimeException> exceptionSupplier) {
		if (!expression) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * 断言值为Empty的情况下抛出异常
	 *
	 * @param value             断言的值
	 * @param exceptionSupplier 异常的供给者
	 * @param <T>
	 */
	public static <T> void isEmptyThrow(T value, Supplier<RuntimeException> exceptionSupplier) {
		if (ObjectUtils.isEmpty(value)) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * 断言值不为Empty的情况下抛出异常
	 *
	 * @param value             断言的值
	 * @param exceptionSupplier 异常的供给者
	 * @param <T>
	 */
	public static <T> void isNotEmptyThrow(T value, Supplier<RuntimeException> exceptionSupplier) {
		if (!ObjectUtils.isEmpty(value)) {
			throw exceptionSupplier.get();
		}
	}

}
