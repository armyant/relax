package com.idea.relax.cache.suppert.func;


import java.util.*;

/**
 * @className: RelaxFunctionHandler
 * @description:
 * @author: salad
 * @date: 2022/5/28
 **/
public class RelaxFunctionHandler implements IFunctionHandler<IFunction>{

    @Override
    public List<IFunction> getFuncList() {
        List<IFunction> funList = new ArrayList<>();
        funList.add(new Func());
        return funList;
    }

}
