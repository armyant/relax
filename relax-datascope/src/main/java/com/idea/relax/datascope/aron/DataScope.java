package com.idea.relax.datascope.aron;


import java.lang.annotation.*;

/**
 * 数据权限定义
 *
 * @author salad
 * 不同角色  查询字段不一样 查询的值不一样
 * 字段需要提取出来，值需要提取出来
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DataScope {

	/**
	 * 编号
	 */
	String code();

	/**
	 * 子SQL，支持占位符
	 * 比如：
	 * value = {isOpen = 1,type = {types}}
	 * {types} 需要实现<link>com.idea.relax.datascope.aron.IDataAuthValueProvider</link>接口
	 * @return
	 */
	String[] value();

	/**
	 * 子SQL的连接符，AND 或者 OR，默认全是AND连接
	 *
	 * @return
	 */
	Condition[] condition() default Condition.ADN;

}

