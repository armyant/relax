package com.idea.relax.log.support;

/**
 * @author salad
 */

public enum LoggingType {
    /**
     * logback
     */
    LOGBACK,
    /**
     * log4j2
     */
    LOG4J2,
    /**
     * other
     */
    OTHER
}