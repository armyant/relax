package com.idea.relax.tool.core;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @className: ListUtil
 * @description:
 * @author: salad
 * @date: 2022/4/10
 **/
public class ListUtil {


    public static <T> List<T> filter(List<T> target, Predicate<? super T> filter) {
        Objects.requireNonNull(target);
        return stream(target).filter(filter).collect(Collectors.toList());
    }

    private static <T> Stream<T> stream(List<T> target) {
        return target.stream();
    }

}
