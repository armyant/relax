package com.idea.relax.launch.autoconfig;

import lombok.Data;

/**
 * @author: 沉香
 * @date: 2023/4/14
 * @description:
 */
@Data
public class ServerMetadata {

	/**
	 * 服务器hostname
	 */
	private String hostName;

	/**
	 * 服务器IP
	 */
	private String ip;

	/**
	 * 应用端口
	 */
	private Integer port;

	/**
	 * 应用名称
	 */
	private String appName;

	/**
	 * 应用环境
	 */
	private String env;

	/**
	 * IP:PORT
	 */
	private String serverAddr;

}
