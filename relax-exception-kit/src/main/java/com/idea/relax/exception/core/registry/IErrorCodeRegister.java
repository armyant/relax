package com.idea.relax.exception.core.registry;

/**
 * @author: 沉香
 * @date: 2023/6/13
 * @description: 异常码注册器接口
 */
public interface IErrorCodeRegister {

	/**
	 * 注册异常码
	 */
	void registry();

}
