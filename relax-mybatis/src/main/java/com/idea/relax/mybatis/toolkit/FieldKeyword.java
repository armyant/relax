package com.idea.relax.mybatis.toolkit;

import com.idea.relax.mybatis.config.RelaxMybatisProperties;
import lombok.AllArgsConstructor;

import java.util.Arrays;

/**
 * @className: FieldKeyWord
 * @description:
 * @author: salad
 * @date: 2021/11/21
 **/
@AllArgsConstructor
public class FieldKeyword {

	private static RelaxMybatisProperties.Toolkit properties;

	public static String[] excludeList = {};

	private static final String[] mustExcludeList = {"class", "serialVersionUID"};

	private static String[] getExcludes() {
		if (properties == null) {
			RelaxMybatisProperties bean = RelaxUtil.getBean(RelaxMybatisProperties.class);
			FieldKeyword.properties = bean.getToolkit();
			excludeList = FieldKeyword.properties.getQueryWrapperExcludeFields();
		}
		return excludeList;
	}


	public static boolean exclude(String name, String... exclude) {
		if (Arrays.asList(mustExcludeList).contains(name)) {
			return true;
		}

		if (exclude.length > 0 && Arrays.asList(exclude).contains(name)) {
			return true;
		}
		if (excludeList.length == 0) {
			return false;
		}
		return Arrays.asList(getExcludes()).contains(name);
	}

}
