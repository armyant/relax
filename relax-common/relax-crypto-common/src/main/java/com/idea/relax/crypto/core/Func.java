package com.idea.relax.crypto.core;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * @className: Func
 * @description:
 * @author: salad
 * @date: 2022/5/28
 **/
class Func {


	/**
	 * 判断对象是否为Empty
	 *
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		} else if (obj instanceof Optional) {
			return !((Optional<?>) obj).isPresent();
		} else if (obj instanceof CharSequence) {
			return ((CharSequence) obj).length() == 0;
		} else if (obj.getClass().isArray()) {
			return Array.getLength(obj) == 0;
		} else if (obj instanceof Collection) {
			return ((Collection<?>) obj).isEmpty();
		} else {
			return obj instanceof Map && ((Map) obj).isEmpty();
		}
	}

	public static boolean isNotEmpty(Object obj) {
		return !isEmpty(obj);
	}

}
