package com.idea.relax.datascope;

/**
 * @className: IDataScopeConditionRegister
 * @description:
 * @author: salad
 * @date: 2023/1/25
 **/
public interface IDataScopeConditionRegister {

    void register(DataScopeEnum dataScopeEnum,Conditionor conditionor);

    Conditionor get(DataScopeEnum dataScopeEnum);

}
