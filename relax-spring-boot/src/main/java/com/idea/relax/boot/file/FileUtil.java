package com.idea.relax.boot.file;

import com.idea.relax.tool.core.DateUtil;
import com.idea.relax.tool.core.ExceptionUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.UUID;

/**
 * @author: 沉香
 * @date: 2023/4/19
 * @description:
 */
public class FileUtil {


	public static String getFileId() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 获取文件后缀
	 *
	 * @param fileName 文件名
	 * @return 文件后缀
	 */
	public static String getFileExt(String fileName) {
		if (!StringUtils.hasLength(fileName)) {
			return "";
		}
		if (!fileName.contains(".")) {
			return ".jpg";
		} else {
			return fileName.substring(fileName.lastIndexOf("."));
		}
	}

	public static long getFileSize(File file) {
		if (!file.isDirectory() && file.isFile()) {
			return file.length();
		}
		return 0L;
	}

	public static boolean createDir(String dir) {
		File file = new File(dir);
		if (!file.isDirectory()) {
			return false;
		}
		if (!file.exists()) {
			return file.mkdirs();
		}
		return false;
	}

	public static boolean createDir(File file) {
		if (!file.isDirectory()) {
			return false;
		}
		if (!file.exists()) {
			return file.mkdirs();
		}
		return false;
	}

	public static File rename(File f, String path) {
		File dest = new File(path);
		f.renameTo(dest);
		return dest;
	}

	public static String renameToUUID(String fileName) {
		Assert.notNull(fileName, "file name is not null!");
		String uuid = UUID.randomUUID().toString().replace("-", "");
		String fileType = fileName.substring(fileName.lastIndexOf("."));
		return uuid + fileType;
	}

	public static String getDir(String parent, String saveDir, String dir, String name) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(parent).append(File.separator).append(saveDir).append(File.separator);
		if (dir != null) {
			stringBuffer.append(dir).append(File.separator);
		}
		stringBuffer.append(DateUtil.format(new Date(), DateUtil.PATTERN_SIMPLE_DATE)).append(File.separator);
		stringBuffer.append(System.nanoTime()).append(File.separator);
		if (name != null) {
			stringBuffer.append(getFileExt(name).replaceFirst(".", ""));
		}
		return stringBuffer.append(File.separator).toString();
	}

	public static void download(String absFilePath, String fileName, HttpServletResponse response) {
		File file = new File(absFilePath);
		if (!file.exists()) {
			throw new RuntimeException("文件未找到");
		}
		try (InputStream inputStream = new FileInputStream(file); OutputStream os = response.getOutputStream()) {
			response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName=" + java.net.URLEncoder.encode(fileName, "utf-8"));
			response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
			response.setCharacterEncoding("utf-8");
			byte[] b = new byte[2048];
			int length;
			while ((length = inputStream.read(b)) > 0) {
				os.write(b, 0, length);
			}
		} catch (Exception e) {
			throw ExceptionUtil.unchecked(e);
		}
	}


}
