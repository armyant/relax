package com.idea.relax.crypto.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: 沉香
 * @date: 2023/2/27
 * @description:
 */
@Data
@ConfigurationProperties(RelaxCryptoProperties.PREFIX)
public class RelaxCryptoProperties {

    /**
     * 前缀
     */
    public static final String PREFIX = "relax.crypto";

	/**
	 * 是否开启报文加密
	 */
	private boolean enabled = false;

	/**
	 * 全局的私钥
	 */
	private String  privateKey = "";



}
