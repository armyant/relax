package com.idea.relax.datascope.aron;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: Condition
 * @description: TODO 类描述
 * @author: salad
 * @date: 2023/5/3
 **/
@Getter
@AllArgsConstructor
public enum Condition {

	ADN("ADN"),

	OR("OR");

	private String value;

}
