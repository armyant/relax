package com.idea.relax.mybatis.autoconfig;

import com.idea.relax.launch.autoconfig.LaunchAutoConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Properties;

/**
 * @className: MybatisPlusLaunchAutoConfigurer
 * @description:
 * @author: salad
 * @date: 2023/4/12
 **/
public class MybatisPlusLaunchAutoConfigurer implements LaunchAutoConfigurer {


	private static final String MAPPER_LOCATIONS = "relax-mybatis-mapper-locations";

	private static final String TYPE_ENUMS_PACKAGE = "relax-mybatis-type-aliases-package";


	@Override
	public void autoconfig(ConfigurableEnvironment environment, SpringApplication application) {
		String name = application.getMainApplicationClass().getPackage().getName();
		Properties properties = System.getProperties();
		properties.setProperty(MAPPER_LOCATIONS, "classpath:**/mapper/*Mapper.xml");
		properties.setProperty(TYPE_ENUMS_PACKAGE, name.concat("**").concat(".enums"));
	}

	@Override
	public int getOrder() {
		return 0;
	}
}
