package com.idea.relax.cache.suppert.func;


import org.springframework.util.StringUtils;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @className: Func
 * @description: 炒鸡好用的瑞士军刀工具类 (>_<)
 * @author: salad
 * @date: 2022/5/28
 **/
public class Func implements IFunction{

    /**
     * 对象组中是否存在 Empty Object
     *
     * @param os 对象组
     * @return boolean
     */
    public boolean hasEmpty(Object... os) {
        for (Object o : os) {
            if (isEmpty(o)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof Optional) {
            return !((Optional<?>) obj).isPresent();
        } else if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        } else if (obj instanceof Collection) {
            return ((Collection<?>) obj).isEmpty();
        } else {
            return obj instanceof Map && ((Map) obj).isEmpty();
        }
    }

    public boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    public List<String> toList(String values, String separator) {
        if (isBlank(values)){
            return Collections.emptyList();
        }
        return Arrays.asList(values.split(separator));
    }

    public List<String> toList(String values) {
        return toList(values, ",");
    }

    public String[] toArray(String values, String separator) {
        if (isBlank(values)){
            return new String[0];
        }
        return values.split(separator);
    }

    public String[] toArray(String values) {
        return toArray(values, ",");
    }


    public String append(CharSequence src, boolean skipEmpty, CharSequence... append) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(src);
        for (CharSequence one : append) {
            if (skipEmpty) {
                if (isNotBlank(one)) {
                    stringBuilder.append(one);
                }
            } else {
                stringBuilder.append(one);
            }
        }
        return stringBuilder.toString();
    }

    public String append(CharSequence src, CharSequence... append) {
        return append(src, true, append);
    }
    public boolean isBlank(final CharSequence cs) {
        return !StringUtils.hasText(cs);
    }

    public boolean isNotBlank(final CharSequence cs) {
        return StringUtils.hasText(cs);
    }

    public boolean eq(final CharSequence v1, final CharSequence v2) {
        return eq(v1, v2, false);
    }

    public boolean eqIgnoreCase(final CharSequence v1, final CharSequence v2) {
        return eq(v1, v2, true);
    }

    public boolean eq(CharSequence str1, CharSequence str2, boolean ignoreCase) {
        if (null == str1) {
            return str2 == null;
        } else if (null == str2) {
            return false;
        } else {
            return ignoreCase ? str1.toString()
                    .equalsIgnoreCase(str2.toString()) : str1.equals(str2);
        }
    }

    public String formatDateTime(LocalDateTime dateTime, String pattern) {
        return DateTimeFormatter.ofPattern(pattern).format(dateTime);
    }

    public String formatDateTime(LocalDateTime dateTime) {
        return formatDateTime(dateTime, "yyyy-MM-dd HH:mm:ss");
    }

    public String formatDate(LocalDate date, String pattern) {
        return DateTimeFormatter.ofPattern(pattern).format(date);
    }

}
