package com.idea.relax.boot.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.idea.relax.tool.core.StringPool;

import java.io.IOException;

/**
 * @author: 沉香
 * @date: 2023/4/2
 * @description:
 */
public interface NullJsonSerializers {


	class ObjectJsonSerializer extends JsonSerializer<Object> {

		@Override
		public void serialize(Object o, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
			gen.writeStartObject();
			gen.writeEndObject();
		}

	}

	class StringJsonSerializer extends JsonSerializer<Object> {

		@Override
		public void serialize(Object o, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
			gen.writeString(StringPool.EMPTY);
		}

	}

	class NumberJsonSerializer extends JsonSerializer<Object> {

		@Override
		public void serialize(Object o, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
			gen.writeNumber(-1);
		}

	}

	class BooleanJsonSerializer extends JsonSerializer<Object> {

		@Override
		public void serialize(Object o, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
			gen.writeObject(Boolean.FALSE);
		}

	}

	class ArrayJsonSerializer extends JsonSerializer<Object> {

		@Override
		public void serialize(Object o, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
			gen.writeStartArray();
			gen.writeEndArray();
		}

	}

}
