package com.idea.relax.datascope.annotation;


import com.idea.relax.datascope.DataScopeEnum;

import java.lang.annotation.*;

/**
 * 数据权限定义
 * @author salad
 *  不同角色  查询字段不一样 查询的值不一样
 *  字段需要提取出来，值需要提取出来
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DataScope {

	/**
	 * 资源编号
	 */
	String code() default "";

	/**
	 * 数据权限对应字段
	 */
	String column() default "";

	/**
	 * 数据权限规则
	 */
	DataScopeEnum type() default DataScopeEnum.ALL;

	/**
	 * 数据权限规则值域
	 */
	String value() default "";

}

