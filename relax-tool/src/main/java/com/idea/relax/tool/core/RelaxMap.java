package com.idea.relax.tool.core;

import org.springframework.util.LinkedCaseInsensitiveMap;


/**
 * @author salad
 */
public class RelaxMap extends LinkedCaseInsensitiveMap<Object> {

    private RelaxMap() {
        super();
    }


    public static RelaxMap create() {
        return new RelaxMap();
    }

    /**
     * 设置列
     *
     * @param attr  属性
     * @param value 值
     * @return 本身
     */
    public RelaxMap put(String attr, Object value) {
        super.put(attr, value);
        return this;
    }

    /**
     * 获得特定类型值
     *
     * @param <T>          值类型
     * @param attr         字段名
     * @param defaultValue 默认值
     * @return 字段值
     */
    public <T> T get(String attr, T defaultValue) {
        final Object result = get(attr);
        return (T) (result != null ? result : defaultValue);
    }


    public static void main(String[] args) {
        RelaxMap rm = RelaxMap.create().put("A", "1").put("B", "2");
        System.out.println(rm);
        System.out.println(rm.get("C",3));
    }

}
