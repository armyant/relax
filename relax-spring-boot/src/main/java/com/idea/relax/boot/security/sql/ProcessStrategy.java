package com.idea.relax.boot.security.sql;

/**
 * @author: 沉香
 * @date: 2023/4/11
 * @description:
 */
public enum ProcessStrategy {

	/**
	 * 抛出异常
	 */
	THROW,

	/**
	 * 抛出异常
	 */
	SILENCE

}
