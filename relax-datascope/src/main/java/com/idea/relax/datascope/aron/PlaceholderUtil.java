package com.idea.relax.datascope.aron;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: 沉香
 * @date: 2023/5/4
 * @description:
 */
public class PlaceholderUtil {


	public static final Pattern LIMIT_COUNT_PATTERN = Pattern.compile("\\{([\\s\\S]+)\\}");

	/**
	 * 按照动态内容的参数出现顺序,将参数放到List中
	 */
	public static List<String> find(String content) {
		Set<String> paramSet = new LinkedHashSet<>();
		Matcher matcher = LIMIT_COUNT_PATTERN.matcher(content);
		while (matcher.find()) {
			paramSet.add(matcher.group(1));
		}
		return new ArrayList<>(paramSet);
	}

}
