package com.idea.relax.boot.config;

import com.idea.relax.tool.core.yml.YamlPropertySourceFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author salad
 */
@Configuration
@PropertySource(value = "classpath:log.yml", factory = YamlPropertySourceFactory.class)
public class LogConfiguration {


}
