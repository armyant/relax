package com.idea.relax.boot.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.idea.relax.boot.jackson.BigNumberModule;
import com.idea.relax.boot.jackson.JavaTimeModule;
import com.idea.relax.boot.jackson.RelaxBeanSerializerModifier;
import com.idea.relax.boot.jackson.RelaxJacksonProperties;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author: 沉香
 * @date: 2023/4/3
 * @description:
 */
@AutoConfiguration
@AllArgsConstructor
@ConditionalOnClass(ObjectMapper.class)
@AutoConfigureBefore(JacksonAutoConfiguration.class)
@EnableConfigurationProperties(RelaxJacksonProperties.class)
public class JacksonConfiguration {

	private final RelaxJacksonProperties properties;

	@Primary
	@Bean
	public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
		builder.simpleDateFormat(properties.getGlobalDateFormat());
		//创建ObjectMapper
		ObjectMapper objectMapper = builder.createXmlMapper(false).build();
		//设置地点为中国
		objectMapper.setLocale(Locale.CHINA);
		//去掉默认的时间戳格式
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		//设置为中国上海时区
		objectMapper.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
		//序列化时，日期的统一格式
		objectMapper.setDateFormat(new SimpleDateFormat(properties.getGlobalDateFormat(), Locale.CHINA));
		//序列化处理
		objectMapper.configure(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(), true);
		//反序列化的JSON串里带有反斜杠时不抛出异常
		objectMapper.configure(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER.mappedFeature(), true);
		objectMapper.findAndRegisterModules();
		//序列化没有属性的空对象时不抛异常
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		//支持解析带注释的Json串（JSON里的注释符会被过滤掉）
		objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
		//忽略不一致字段
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//单引号处理
		objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		//处理没有双引号的json串
		objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		//反序列化时，属性不存在的兼容处理
		objectMapper.getDeserializationConfig().withoutFeatures(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		//日期格式化
		objectMapper.registerModule(JavaTimeModule.create(properties.getGlobalDateTimeFormat(), properties.getGlobalDateFormat(), properties.getGlobalTimeFormat()));
		//大数字序列化
		objectMapper.registerModule(BigNumberModule.INSTANCE);
		//特殊值的json序列化设置
		objectMapper.setSerializerFactory(objectMapper.getSerializerFactory().withSerializerModifier(new RelaxBeanSerializerModifier(properties)));
		objectMapper.findAndRegisterModules();
		return objectMapper;
	}

}
