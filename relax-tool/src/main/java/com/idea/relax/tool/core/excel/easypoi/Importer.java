package com.idea.relax.tool.core.excel.easypoi;

import java.util.List;

/**
 * @className: Importer
 * @description:
 * @author: salad
 * @date: 2022/2/19
 **/
public interface Importer<T> {

    void save(List<T> dataList);

}
