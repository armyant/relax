package com.idea.relax.log.listener;


import com.idea.relax.log.publisher.ILogPublisher;
import com.idea.relax.log.event.ApiLogEvent;
import com.idea.relax.log.service.IRelaxLogService;
import com.idea.relax.log.support.spel.RelaxExpressionParser;
import com.idea.relax.log.service.IApiLogService;
import com.idea.relax.log.thread.RelaxLoggingThreadPoolManager;
import com.idea.relax.log.api.callback.RelaxFailureCallbackHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;

import java.util.Map;


/**
 * @className: ApiLogListener
 * @description: 操作日志事件的监听
 * @author: salad
 * @date: 2022/6/9
 **/
@Slf4j
@AllArgsConstructor
public class ApiLogListener {

    private final IApiLogService apiLogService;

    private final IRelaxLogService relaxLogService;

    private final RelaxExpressionParser expressionParser;

    private final RelaxLoggingThreadPoolManager threadPoolManager;

    private final RelaxFailureCallbackHandler failureCallbackHandler;

    @SuppressWarnings("unchecked")
    @EventListener(ApiLogEvent.class)
    public void listener(ApiLogEvent event) {
        ILogPublisher.Wrapper<Map<String, Object>> wrapper =
                (ILogPublisher.Wrapper<Map<String, Object>>) event.getSource();
        threadPoolManager.getThreadPool().execute(
                new ApiLogWorker(wrapper, expressionParser,
                        apiLogService, relaxLogService, failureCallbackHandler)
        );
    }

}