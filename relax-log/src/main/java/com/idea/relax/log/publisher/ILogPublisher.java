package com.idea.relax.log.publisher;


/**
 * @className: ILogPublisher
 * @description: 日志事件发布者的接口
 * @author: salad
 * @date: 2022/6/3
 **/
public interface ILogPublisher {

    /**
     * 发布事件
     */
    void publish();

    /**
     * 发布事件时传输的数据包装器
     * @param <T> 具体要包装的数据类型的泛型
     */
    class Wrapper<T>{

        private final T value;

        public Wrapper(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

    }

}
