package com.idea.relax.tool.wechat.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author: 沉香
 * @date: 2023/3/7
 * @description:
 */
@Data
@AllArgsConstructor
public class WechatPayNotice {

    private static final String SUCCESS = "SUCCESS";

    private static final String FAIL = "FAIL";

    private String code;

    private String message;

    public static WechatPayNotice ok() {
        return new WechatPayNotice(SUCCESS, "");
    }

    public static WechatPayNotice ok(String message) {
        return new WechatPayNotice(SUCCESS, message);
    }

    public static WechatPayNotice fail(String message) {
        return new WechatPayNotice(FAIL, message);
    }

}
