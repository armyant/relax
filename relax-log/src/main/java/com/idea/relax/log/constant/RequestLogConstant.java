package com.idea.relax.log.constant;

import com.idea.relax.log.props.RequestLogMode;

import java.util.HashSet;
import java.util.Set;

/**
 * @className: RequestLogConstant
 * @description: 请求日志相关的常量
 * @author: salad
 * @date: 2022/12/31
 **/
public final class RequestLogConstant {


    /**
     * 请求日志的切入点表达式
     */
    public static final String RESP_LOG_EXP = "@within(org.springframework.stereotype.Controller)" +
            "|| @within(org.springframework.web.bind.annotation.RestController)";

    /**
     * 请求日志默认的模式
     */
    public static final Set<RequestLogMode> DEFAULT_MODES = new HashSet<>(16);


    static {
        DEFAULT_MODES.add(RequestLogMode.HEADERS);
        DEFAULT_MODES.add(RequestLogMode.REQ_PARAMS);
        DEFAULT_MODES.add(RequestLogMode.REQ_BODY);
        DEFAULT_MODES.add(RequestLogMode.URI);
        DEFAULT_MODES.add(RequestLogMode.RESP_DATA);
    }


}
