package com.idea.relax.redis.config;

/**
 * @className: SerializerType
 * @description:
 * @author: salad
 * @date: 2022/9/6
 **/

public enum SerializerType {

    JDK,

    JSON,

    PROTO_STUFF;

}
