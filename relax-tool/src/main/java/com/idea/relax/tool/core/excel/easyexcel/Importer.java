package com.idea.relax.tool.core.excel.easyexcel;

import java.util.List;

public interface Importer<T> {
    void save(List<T> list);
}
