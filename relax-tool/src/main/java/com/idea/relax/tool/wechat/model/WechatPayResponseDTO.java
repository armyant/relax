package com.idea.relax.tool.wechat.model;

import lombok.Setter;

import java.io.Serializable;
import java.util.List;


/**
 * @author salad
 */
@Setter
public class WechatPayResponseDTO implements Serializable {

    /**
     * amount
     */
    private TransactionAmount amount;

    /**
     * appid
     */
    private String appid;

    /**
     * attach
     */
    private String attach;

    /**
     * bankType
     */
    private String bank_type;

    /**
     * mchid
     */
    private String mchid;

    /**
     * outTradeNo
     */
    private String out_trade_no;

    /**
     * payer
     */
    private TransactionPayer payer;

    /**
     * promotionDetail
     */
    private List<PromotionDetail> promotion_detail;

    /**
     * successTime
     */
    private String success_time;

    /**
     * tradeState
     */
    public enum TradeStateEnum {

        SUCCESS,

        REFUND,

        NOTPAY,

        CLOSED,

        REVOKED,

        USERPAYING,

        PAYERROR,

        ACCEPT

    }

    private TradeStateEnum trade_state;

    /**
     * tradeStateDesc
     */
    private String trade_state_desc;

    /**
     * tradeType
     */
    public enum TradeTypeEnum {
        JSAPI,

        NATIVE,

        APP,

        MICROPAY,

        MWEB,

        FACEPAY
    }

    private TradeTypeEnum trade_type;
    /**
     * transactionId
     */
    private String transaction_id;

    /**
     * 微信支付退款号 说明：微信支付退款号
     */
    private String refund_id;

    /**
     * 微信支付退款状态 成功：SUCCESS
     */
    private String refund_status;

    public String getOutTradeNo() {
        return out_trade_no;
    }

    public TransactionAmount getAmount() {
        return amount;
    }

    public String getAppid() {
        return appid;
    }

    public String getAttach() {
        return attach;
    }

    public String getBankType() {
        return bank_type;
    }

    public String getMchid() {
        return mchid;
    }

    public TransactionPayer getPayer() {
        return payer;
    }

    public String getSuccessTime() {
        return success_time;
    }

    public TradeStateEnum getTradeState() {
        return trade_state;
    }

    public String getTradeStateDesc() {
        return trade_state_desc;
    }

    public TradeTypeEnum getTradeType() {
        return trade_type;
    }

    public String getRefundId() {
        return refund_id;
    }

    public String getRefundStatus() {
        return refund_status;
    }

    public String getTransactionId() {
        return transaction_id;
    }

    public List<PromotionDetail> getPromotionDetail() {
        return promotion_detail;
    }
}
