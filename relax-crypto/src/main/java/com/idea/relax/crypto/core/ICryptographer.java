package com.idea.relax.crypto.core;

import com.idea.relax.crypto.config.CryptoType;

/**
 * @author: 沉香
 * @date: 2023/3/19
 * @description:
 */
public interface ICryptographer {


	/**
	 * 支持的加密类型
	 * @return
	 */
	CryptoType cryptoType();

	/**
	 * 是否支持这个加密类型
	 * @param cryptoType
	 * @return
	 */
	boolean support(CryptoType cryptoType);

	/**
	 * 解密
	 * @param data
	 * @param password
	 * @return
	 */
	byte[] decryptFormBase64(byte[] data, String password);

	/**
	 * 加密
	 * @param data
	 * @param password
	 * @return
	 */
	String encryptToBase64(byte[] data, String password);

}
