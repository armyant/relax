package com.idea.relax.redis.lock.support;

import com.idea.relax.redis.lock.config.RedissonMode;
import com.idea.relax.redis.lock.config.RelaxRedisLockProperties;
import com.idea.relax.redis.support.utils.RelaxUtil;
import org.redisson.config.*;

/**
 * @author: 沉香
 * @date: 2023/2/13
 * @description:
 */
@SuppressWarnings("all")
public class RedissonConfigBuilder {


    public static Config build(RelaxRedisLockProperties properties) {
        RedissonMode redissonMode = properties.getRedissonMode();
        Config redissonConfig = new Config();
        switch (redissonMode) {
            case SENTINEL:
                 return sentinelConfig(redissonConfig,properties);
            case CLUSTER:
                return clusterConfig(redissonConfig,properties);
            case MASTER_SLAVE:
                return masterSlaveConfig(redissonConfig,properties);
            case SINGLE:
                return singleConfig(redissonConfig,properties);
            default:
                return redissonConfig;
        }
    }


    /**
     * 单机redis的配置
     * @param properties
     * @return
     */
    public static Config singleConfig(Config redissonConfig,RelaxRedisLockProperties properties) {
        SingleServerConfig serversConfig = redissonConfig.useSingleServer();
        serversConfig.setAddress(properties.getAddress());
        String password = properties.getPassword();
        if (RelaxUtil.isNotBlank(password)) {
            serversConfig.setPassword(password);
        }
        serversConfig.setDatabase(properties.getDatabase());
        serversConfig.setConnectionPoolSize(properties.getPoolSize());
        serversConfig.setConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setIdleConnectionTimeout(properties.getConnectionTimeout());
        serversConfig.setConnectTimeout(properties.getConnectionTimeout());
        serversConfig.setTimeout(properties.getTimeout());
        return redissonConfig;
    }

    /**
     *
     * @param properties
     * @return
     */
    public static Config masterSlaveConfig(Config redissonConfig,RelaxRedisLockProperties properties) {
        MasterSlaveServersConfig serversConfig = redissonConfig.useMasterSlaveServers();
        serversConfig.setMasterAddress(properties.getMasterAddress());
        serversConfig.addSlaveAddress(properties.getSlaveAddress());
        String password = properties.getPassword();
        if (RelaxUtil.isNotBlank(password)) {
            serversConfig.setPassword(password);
        }

        serversConfig.setDatabase(properties.getDatabase());
        serversConfig.setMasterConnectionPoolSize(properties.getPoolSize());
        serversConfig.setMasterConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setSlaveConnectionPoolSize(properties.getPoolSize());
        serversConfig.setSlaveConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setIdleConnectionTimeout(properties.getConnectionTimeout());
        serversConfig.setConnectTimeout(properties.getConnectionTimeout());
        serversConfig.setTimeout(properties.getTimeout());
        return redissonConfig;
    }

    /**
     *
     * @param properties
     * @return
     */
    public static Config sentinelConfig(Config redissonConfig,RelaxRedisLockProperties properties) {
        SentinelServersConfig serversConfig = redissonConfig.useSentinelServers();
        serversConfig.setMasterName(properties.getMasterName());
        serversConfig.addSentinelAddress(properties.getSentinelAddress());
        String password = properties.getPassword();
        if (RelaxUtil.isNotBlank(password)) {
            serversConfig.setPassword(password);
        }

        serversConfig.setDatabase(properties.getDatabase());
        serversConfig.setMasterConnectionPoolSize(properties.getPoolSize());
        serversConfig.setMasterConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setSlaveConnectionPoolSize(properties.getPoolSize());
        serversConfig.setSlaveConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setIdleConnectionTimeout(properties.getConnectionTimeout());
        serversConfig.setConnectTimeout(properties.getConnectionTimeout());
        serversConfig.setTimeout(properties.getTimeout());
        return redissonConfig;
    }

    /**
     * 集群模式
     * @param properties
     * @return
     */
    public static Config clusterConfig(Config redissonConfig,RelaxRedisLockProperties properties) {
        ClusterServersConfig serversConfig = redissonConfig.useClusterServers();
        serversConfig.addNodeAddress(properties.getNodeAddress());
        String password = properties.getPassword();
        if (RelaxUtil.isNotBlank(password)) {
            serversConfig.setPassword(password);
        }
        serversConfig.setMasterConnectionPoolSize(properties.getPoolSize());
        serversConfig.setMasterConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setSlaveConnectionPoolSize(properties.getPoolSize());
        serversConfig.setSlaveConnectionMinimumIdleSize(properties.getIdleSize());
        serversConfig.setIdleConnectionTimeout(properties.getConnectionTimeout());
        serversConfig.setConnectTimeout(properties.getConnectionTimeout());
        serversConfig.setTimeout(properties.getTimeout());
        return redissonConfig;
    }


}
