package com.idea.relax.log.support.utils;

import lombok.Getter;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @className: ServerInfoProvider
 * @description: 获取服务相关信息
 * @author: salad
 * @date: 2022/6/3
 **/
@Getter
@Configuration(
        proxyBeanMethods = false
)
public class ServerInfoProvider implements SmartInitializingSingleton {

    private final ServerProperties serverProperties;

    private String hostName;

    private String ip;

    private Integer port;

    private final String appName;

    private final String env;


    @Autowired(
            required = false
    )
    public ServerInfoProvider(ServerProperties serverProperties,Environment environment) {
        this.serverProperties = serverProperties;
        this.appName = environment.getProperty("spring.application.name","");
        this.env = StringUtil.join(environment.getActiveProfiles());
    }


    @Override
    public void afterSingletonsInstantiated() {
        this.hostName = INetUtil.getHostName();
        this.ip = INetUtil.getHostIp();
        this.port = serverProperties.getPort();
    }

}
