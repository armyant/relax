package com.idea.relax.mybatis.toolkit;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.Nullable;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * @className: RelaxUtil
 * @description:
 * @author: salad
 * @date: 2022/1/25
 **/
class RelaxUtil implements ApplicationContextAware {

	private static ApplicationContext context;

	@Override
	public void setApplicationContext(@Nullable ApplicationContext context) throws BeansException {
		RelaxUtil.context = context;
	}

	public static <T> T getBean(Class<T> clazz) {
		return clazz == null ? null : context.getBean(clazz);
	}

	public static boolean isNotEmpty(@Nullable Object obj) {
    	return !isEmpty(obj);
	}

    public static boolean isEmpty(@Nullable Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof Optional) {
            return !((Optional) obj).isPresent();
        } else if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        } else if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        } else if (obj instanceof Number) {
            return StringPool.ZERO.equals(obj.toString());
        } else {
            return obj instanceof Map && ((Map) obj).isEmpty();
        }
    }

	/**
	 * 驼峰命名转下划线分割命名
	 */
	public static String humpToUnderline(@Nullable String para) {
		para = firstCharToLower(para);
		StringBuilder sb = new StringBuilder(para);
		int temp = 0;

		for (int i = 0; i < para.length(); ++i) {
			if (Character.isUpperCase(para.charAt(i))) {
				sb.insert(i + temp, "_");
				++temp;
			}
		}
		return sb.toString().toLowerCase();
	}

	public static String firstCharToLower(String str) {
		char firstChar = str.charAt(0);
		if (firstChar >= 'A' && firstChar <= 'Z') {
			char[] arr = str.toCharArray();
			arr[0] = (char) (arr[0] + 32);
			return new String(arr);
		} else {
			return str;
		}
	}

	/**
	 * 将CheckedException转换为UncheckedException.
	 *
	 * @param e Throwable
	 * @return {RuntimeException}
	 */
	public static RuntimeException unchecked(Throwable e) {
		if (e instanceof IllegalAccessException || e instanceof IllegalArgumentException
			|| e instanceof NoSuchMethodException) {
			return new IllegalArgumentException(e);
		} else if (e instanceof InvocationTargetException) {
			return new RuntimeException(((InvocationTargetException) e).getTargetException());
		} else if (e instanceof RuntimeException) {
			return (RuntimeException) e;
		} else {
			return new RuntimeException(e);
		}
	}

	/**
	 * 获取标识符，用于参数清理
	 *
	 * @param param 参数
	 * @return 清理后的标识符
	 */
	@Nullable
	public static String cleanIdentifier(@Nullable String param) {
		if (param == null) {
			return null;
		}
		StringBuilder paramBuilder = new StringBuilder();
		for (int i = 0; i < param.length(); i++) {
			char c = param.charAt(i);
			if (Character.isJavaIdentifierPart(c)) {
				paramBuilder.append(c);
			}
		}
		return paramBuilder.toString();
	}

	public static String[] toArray(String values, String separator) {
		if (isEmpty(values)){
			return new String[0];
		}
		return values.split(separator);
	}

}
