package com.idea.relax.datascope;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据权限类型
 *
 * @author lengleng, Chill
 */
@Getter
@AllArgsConstructor
public enum DataScopeEnum {
	/**
	 * 全部数据
	 */
	ALL(1, "全部"),

	/**
	 * 本人可见
	 */
	OWN(2, "本人可见"),

	/**
	 * 所在机构可见
	 */
	OWN_DEPT(3, "所在机构可见"),

	/**
	 * 自定义
	 */
	CUSTOM(5, "自定义");

	/**
	 * 类型
	 */
	private final int type;
	/**
	 * 描述
	 */
	private final String description;

}
