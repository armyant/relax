
package com.idea.relax.mybatis.page;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.idea.relax.mybatis.toolkit.Condition;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author salad
 */
@Data
public class PageReqVO {

	/**
	 * 当前页
	 */
	@ApiModelProperty("当前页")
    private Long current;

	/**
	 * 页大小
	 */
	@ApiModelProperty("页大小")
    private Long pageSize;

	/**
	 * 排序的字段名，多个按照逗号分割
	 */
	@ApiModelProperty("排序的字段名，多个按照逗号分割")
	private String ascs;

	/**
	 * 排序的字段名，多个按照逗号分割
	 */
	@ApiModelProperty("排序的字段名，多个按照逗号分割")
	private String descs;


	/**
	 * 转换为IPage
	 * @param <T>
	 * @return
	 */
	public <T> IPage<T> toPage(){
		return Condition.<T>getPage(this);
	}

}
