package com.idea.relax.tool.core;

import javax.crypto.Cipher;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jinZhao at 2022/9/1
 */
//@Configuration
//@Component
public class RSACoder {

    //非对称密钥算法
    public static final String KEY_ALGORITHM = "RSA";
    /**
     * 密钥长度，DH算法的默认密钥长度是1024
     * 密钥长度必须是64的倍数，在512到65536位之间
     */
    private static final int KEY_SIZE = 512;
    //公钥
    private static final String PUBLIC_KEY = "RSAPublicKey";
    //私钥
    private static final String PRIVATE_KEY = "RSAPrivateKey";

    /**
     * 初始化密钥对
     *
     * @return Map 甲方密钥的Map
     */
    public static Map<String, Object> initKey() throws Exception {
        //实例化密钥生成器
        final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        //初始化密钥生成器
        keyPairGenerator.initialize(KEY_SIZE);
        //生成密钥对
        final KeyPair keyPair = keyPairGenerator.generateKeyPair();
        //甲方公钥
        final RSAPublicKey publicKey = (RSAPublicKey)keyPair.getPublic();
        //甲方私钥
        final RSAPrivateKey privateKey = (RSAPrivateKey)keyPair.getPrivate();
        //将密钥存储在map中
        final Map<String, Object> keyMap = new HashMap<String, Object>();
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;

    }

    /**
     * 私钥加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPrivateKey(final byte[] data, final byte[] key) throws Exception {

        //取得私钥
        final PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        final KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //生成私钥
        final PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        //数据加密
        final Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        return cipher.doFinal(data);
    }

    /**
     * 公钥加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPublicKey(final byte[] data, final byte[] key) throws Exception {

        //实例化密钥工厂
        final KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //初始化公钥
        //密钥材料转换
        final X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        //产生公钥
        final PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);

        //数据加密
        final Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    /**
     * 私钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密数据
     */
    public static byte[] decryptByPrivateKey(final byte[] data, final byte[] key) throws Exception {
        //取得私钥
        final PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        final KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //生成私钥
        final PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        //数据解密
        final Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(data);
    }

    /**
     * 公钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密数据
     */
    public static byte[] decryptByPublicKey(final byte[] data, final byte[] key) throws Exception {

        //实例化密钥工厂
        final KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //初始化公钥
        //密钥材料转换
        final X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        //产生公钥
        final PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
        //数据解密
        final Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    /**
     * 取得私钥
     *
     * @param keyMap 密钥map
     * @return byte[] 私钥
     */
    public static byte[] getPrivateKey(final Map<String, Object> keyMap) {
        final Key key = (Key)keyMap.get(PRIVATE_KEY);
        return key.getEncoded();
    }

    /**
     * 取得公钥
     *
     * @param keyMap 密钥map
     * @return byte[] 公钥
     */
    public static byte[] getPublicKey(final Map<String, Object> keyMap) throws Exception {
        final Key key = (Key)keyMap.get(PUBLIC_KEY);
        return key.getEncoded();
    }

//    /**
//     * @param args
//     * @throws Exception
//     */
//    public static void main(final String[] args) throws Exception {
//
//        final byte[] afterDecode = RSACoder.decryptByPrivateKey(Base64.getDecoder()
//                .decode("ugJv6FfLbSLNKoDtGhqc3USsHMVOt0dpk9woTBCKAKbLIYDH4Yu4sWMUPvqh/C3KMxwIHNhkSJKG5maTs1pWyg=="),
//            Base64.getDecoder().decode(
//                "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAuhACdOFZpvOjyKRYc0s7mNLlpzuK4cf4zqWyW7Fdll4bmPHvgqR5gTrJNya35LEHwimB61w4XcxiPnrxMUIVuQIDAQABAkBrQZ1Y6Sr4LczFdc+zLrQ9RZ2zSISn4NHRtfzaFKWBbuTMtG3Bop/faqT0kotSFHS9Nyoa77SUy+bUhI1+byHxAiEA3aJrYK1tXUlMJeOH2069WVy2aIGJgr8IZ/dW4sgNBc0CIQDW6ZhQvCCDx5eMDjb9Pjik9Y3UVCybQdUvYWqjPOSjnQIgF1g/RSyfQ18wFSGVMsVlbl80m2dhi5MBmKnVzgdST9UCIQC/uz8sJ+pKJvLI/JWKcMxQPtH9r+rryFvNHXfpkhXY8QIgc4NbKWzyP0BJ+8MmPKP3seW2fliHhk8sHUeN05wTOSo="));
//        System.out.println(new String(afterDecode));
//
//        //初始化密钥
//        //生成密钥对
//        //        final Map<String, Object> keyMap = RSACoder.initKey();
//        //        final String publicKey = Base64.getEncoder().encodeToString(RSACoder.getPublicKey(keyMap));
//        //        final String privateKey = Base64.getEncoder().encodeToString(RSACoder.getPrivateKey(keyMap));
//        //        System.out.println("公钥：\r\n" + publicKey);
//        //        System.out.println("私钥：\r\n" + privateKey);
//        //        final String str = "12fds231";
//        //        System.out.println("原文:\r\n" + str);
//        //        final byte[] code1 = RSACoder.encryptByPublicKey(str.getBytes(), Base64.getDecoder().decode(publicKey));
//        //        final String encodeStr = Base64.getEncoder().encodeToString(code1);
//        //        System.out.println("使用公钥加密后的数据：\r\n" + encodeStr);
//        //        //乙方进行数据的解密
//        //        final byte[] afterDecode =
//        //            RSACoder.decryptByPrivateKey(Base64.getDecoder().decode(encodeStr), Base64.getDecoder().decode(privateKey));
//        //        System.out.println("解密后的数据:\r\n" + new String(afterDecode));
//    }

}
