package com.idea.relax.exception.api;

import com.idea.relax.exception.core.manager.ErrorCodeManager;

/**
 * @author: 沉香
 * @date: 2023/2/28
 * @description: 异常基础类
 */
public abstract class BaseException extends RuntimeException {

	/**
	 * 异常等级
	 */
	private final IErrorLevel errorLevel;

	/**
	 * 异常类型
	 */
	private final IErrorType errorType;

	/**
	 * 异常码实例
	 */
	private final IErrorCode errorCodeEnum;

	/**
	 * 异常码
	 */
	private final String errorCode;


	public BaseException(final IErrorCode errorCodeEnum, final Object... args) {
		super(StringFormatter.format(errorCodeEnum.getMessage(), args));
		this.errorCodeEnum = errorCodeEnum;
		this.errorLevel = null;
		this.errorType = null;
		this.errorCode = ErrorCodeManager.getErrorCode(errorCodeEnum);
	}

	public BaseException(final IErrorLevel errorLevel, final IErrorType errorType, final IErrorCode errorCodeEnum, final Object... args) {
		//获取国际化的异常信息
		super(StringFormatter.format(ErrorCodeManager.getMessageSourceErrorCode(errorCodeEnum), args));
		this.errorCodeEnum = errorCodeEnum;
		this.errorLevel = errorLevel;
		this.errorType = errorType;
		this.errorCode = ErrorCodeManager.getErrorCode(errorCodeEnum);
	}

	/**
	 * 获取异常等级
	 *
	 * @return 异常等级
	 */
	public String getLevel() {
		return this.errorLevel.getLevel();
	}

	/**
	 * 获取异常类型
	 *
	 * @return 异常类型
	 */
	public String getType() {
		return this.errorType.getType();
	}

	/**
	 * 获取用户提示信息
	 *
	 * @return 用户提示信息
	 */
	public String getUserTip() {
		return errorCodeEnum.getUserTip();
	}

	/**
	 * 获取HTTP状态码
	 *
	 * @return HTTP状态码
	 */
	public Integer getCode() {
		return errorCodeEnum.getCode();
	}

	/**
	 * 获取异常码枚举类
	 *
	 * @return 异常码枚举类
	 */
	public IErrorCode getErrorCodeEnum() {
		return errorCodeEnum;
	}

	/**
	 * 获取异常等级枚举类
	 *
	 * @return 异常等级枚举类
	 */
	public IErrorLevel getErrorLevel() {
		return errorLevel;
	}

	/**
	 * 获取异常类型枚举类
	 *
	 * @return 异常类型枚举类
	 */
	public IErrorType getErrorType() {
		return errorType;
	}

	/**
	 * 获取异常码
	 *
	 * @return 异常码
	 */
	public String getErrorCode() {
		return errorCode;
	}


}
