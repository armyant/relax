package com.idea.relax.tool.api;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @author salad
 */
public class R<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 成功
	 */
	public static final int SUCCESS = RCode.SUCCESS.code;

	/**
	 * 失败
	 */
	public static final int FAIL = RCode.INTERNAL_SERVER_ERROR.code;

	private int code;

	private String msg;

	private T data;

	public static <T> R<T> ok() {
		return restResult(null, RCode.SUCCESS);
	}

	public static <T> R<T> ok(T data) {
		return restResult(data, RCode.SUCCESS);
	}

	public static <T> R<T> ok(T data, String msg) {
		return restResult(data, RCode.SUCCESS.code, msg);
	}

	public static <T> R<T> fail() {
		return restResult(null, RCode.FAILURE);
	}

	public static <T> R<T> fail(String msg) {
		return restResult(null, RCode.FAILURE.code, msg);
	}

	public static <T> R<T> fail(T data) {
		return restResult(data, RCode.FAILURE);
	}

	public static <T> R<T> fail(T data, String msg) {
		return restResult(data, RCode.FAILURE.code, msg);
	}

	public static <T> R<T> fail(int code, String msg) {
		return restResult(null, code, msg);
	}

	public static <T> R<T> status(boolean flag) {
		return flag ? ok() : fail();
	}

	private static <T> R<T> restResult(T data, RCode resultCode) {
		R<T> apiResult = new R<>();
		apiResult.setCode(resultCode.code);
		apiResult.setData(data);
		apiResult.setMsg(resultCode.message);
		return apiResult;
	}

	private static <T> R<T> restResult(T data, int code, String message) {
		R<T> apiResult = new R<>();
		apiResult.setCode(code);
		apiResult.setData(data);
		apiResult.setMsg(message);
		return apiResult;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
