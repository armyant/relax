package com.idea.relax.boot.constant;

/**
 * @author: 沉香
 * @date: 2023/4/7
 * @description:
 */
public final class DateConstant {


	public static final String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";

	public static final String PATTERN_DATE = "yyyy-MM-dd";

	public static final String PATTERN_SLASH_DATE = "yyyy/MM/dd";

	public static final String PATTERN_SLASH_DATETIME = "yyyy/MM/dd HH:mm:ss";

	public static final String PATTERN_TIME = "HH:mm:ss";


}
